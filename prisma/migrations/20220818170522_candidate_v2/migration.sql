-- AlterTable
ALTER TABLE `candidates` ADD COLUMN `hasDiplomas` BOOLEAN NULL,
    ADD COLUMN `language_level` ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NULL,
    ADD COLUMN `years_exp` ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NULL;

-- AlterTable
ALTER TABLE `offers` ADD COLUMN `is_diplomas_required` BOOLEAN NULL,
    ADD COLUMN `min_exp_years` ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NULL,
    ADD COLUMN `min_language_level` ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NULL;
