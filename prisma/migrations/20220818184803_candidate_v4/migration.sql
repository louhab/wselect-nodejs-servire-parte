-- AlterTable
ALTER TABLE `offers` MODIFY `min_exp_years` ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NULL DEFAULT '10',
    MODIFY `min_language_level` ENUM('1', '2', '3', '4', '5', '6', '7', '8', '9', '10') NULL DEFAULT '10';
