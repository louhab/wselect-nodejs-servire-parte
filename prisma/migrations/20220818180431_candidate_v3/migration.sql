/*
  Warnings:

  - You are about to drop the column `campaign_id` on the `campaigns` table. All the data in the column will be lost.
  - You are about to drop the column `hasDiplomas` on the `candidates` table. All the data in the column will be lost.
  - You are about to drop the column `isAdmis` on the `candidates` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `campaigns` DROP COLUMN `campaign_id`;

-- AlterTable
ALTER TABLE `candidates` DROP COLUMN `hasDiplomas`,
    DROP COLUMN `isAdmis`,

-- AlterTable
ALTER TABLE `offers` MODIFY `title` VARCHAR(191) NULL;

-- CreateTable
CREATE TABLE `offers_on_campaigns` (
    `offer_id` INTEGER NOT NULL,
    `campaign_id` INTEGER NOT NULL,

    PRIMARY KEY (`offer_id`, `campaign_id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
