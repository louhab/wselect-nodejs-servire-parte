/*
  Warnings:

  - You are about to drop the column `campaign_id` on the `candidates` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `candidates` DROP COLUMN `campaign_id`;
