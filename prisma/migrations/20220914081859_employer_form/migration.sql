-- CreateTable
CREATE TABLE `employer_form` (
    `id` INTEGER NOT NULL AUTO_INCREMENT,
    `uuid` VARCHAR(191) NOT NULL,
    `title` VARCHAR(191) NULL,
    `account_number` VARCHAR(191) NULL,
    `company_name` VARCHAR(191) NULL,
    `company_address` VARCHAR(191) NULL,
    `company_city` VARCHAR(191) NULL,
    `company_province` VARCHAR(191) NULL,
    `company_country` VARCHAR(191) NULL,
    `company_zip` VARCHAR(191) NULL,
    `company_address_bis` VARCHAR(191) NULL,
    `company_city_bis` VARCHAR(191) NULL,
    `company_province_bis` VARCHAR(191) NULL,
    `company_country_bis` VARCHAR(191) NULL,
    `company_zip_bis` VARCHAR(191) NULL,
    `website` VARCHAR(191) NULL,
    `start_date` DATE NULL,
    `is_individual` BOOLEAN NULL,
    `is_partnership` BOOLEAN NULL,
    `is_company` BOOLEAN NULL,
    `is_cooperative` BOOLEAN NULL,
    `is_nonprofit` BOOLEAN NULL,
    `is_registered_charity` BOOLEAN NULL,
    `first_name` VARCHAR(191) NULL,
    `second_first_name` VARCHAR(191) NULL,
    `last_name` VARCHAR(191) NULL,
    `position_title` VARCHAR(191) NULL,
    `first_phone` VARCHAR(191) NULL,
    `first_position` VARCHAR(191) NULL,
    `second_phone` VARCHAR(191) NULL,
    `second_position` VARCHAR(191) NULL,
    `fax_number` VARCHAR(191) NULL,
    `email` VARCHAR(191) NOT NULL,
    `address` VARCHAR(191) NULL,
    `city` VARCHAR(191) NULL,
    `province` VARCHAR(191) NULL,
    `country` VARCHAR(191) NULL,
    `zip` VARCHAR(191) NULL,
    `first_name_bis` VARCHAR(191) NULL,
    `second_first_name_bis` VARCHAR(191) NULL,
    `last_name_bis` VARCHAR(191) NULL,
    `position_title_bis` VARCHAR(191) NULL,
    `first_phone_bis` VARCHAR(191) NULL,
    `first_position_bis` VARCHAR(191) NULL,
    `second_phone_bis` VARCHAR(191) NULL,
    `second_position_bis` VARCHAR(191) NULL,
    `fax_number_bis` VARCHAR(191) NULL,
    `email_bis` VARCHAR(191) NULL,
    `address_bis` VARCHAR(191) NULL,
    `city_bis` VARCHAR(191) NULL,
    `province_bis` VARCHAR(191) NULL,
    `country_bis` VARCHAR(191) NULL,
    `zip_bis` VARCHAR(191) NULL,
    `sec_three_1` VARCHAR(191) NULL,
    `sec_three_2` BOOLEAN NULL,
    `sec_three_3` BOOLEAN NULL,
    `sec_three_4` VARCHAR(191) NULL,
    `sec_three_5` VARCHAR(191) NULL,
    `sec_three_6` VARCHAR(191) NULL,
    `sec_three_7` BOOLEAN NULL,
    `sec_three_8` VARCHAR(191) NULL,
    `sec_three_9` BOOLEAN NULL,
    `sec_three_10` VARCHAR(191) NULL,
    `trade_name` VARCHAR(191) NULL,
    `workspace_description` MEDIUMTEXT NULL,
    `workspace_address` VARCHAR(191) NULL,
    `workspace_city` VARCHAR(191) NULL,
    `workspace_province` VARCHAR(191) NULL,
    `workspace_zip` VARCHAR(191) NULL,
    `sec_five_1` VARCHAR(191) NULL,
    `sec_five_2` VARCHAR(191) NULL,
    `sec_five_3` BOOLEAN NULL,
    `sec_five_4` VARCHAR(191) NULL,
    `sec_five_5` VARCHAR(191) NULL,
    `sec_five_6` BOOLEAN NULL,
    `sec_five_7` VARCHAR(191) NULL,
    `sec_five_8` VARCHAR(191) NULL,
    `sec_five_9` BOOLEAN NULL,
    `sec_five_10` VARCHAR(191) NULL,
    `sec_five_11` BOOLEAN NULL,
    `sec_five_12` VARCHAR(191) NULL,
    `sec_five_13` BOOLEAN NULL,
    `sec_five_14` BOOLEAN NULL,
    `sec_five_15` BOOLEAN NULL,
    `sec_five_16` BOOLEAN NULL,
    `sec_five_17` BOOLEAN NULL,
    `sec_five_18` VARCHAR(191) NULL,
    `sec_five_19` VARCHAR(191) NULL,
    `created_at` DATETIME(3) NOT NULL DEFAULT CURRENT_TIMESTAMP(3),
    `updated_at` DATETIME(3) NOT NULL,
    `deleted_at` DATETIME(0) NULL,

    UNIQUE INDEX `employer_form_uuid_key`(`uuid`),
    UNIQUE INDEX `employer_form_email_key`(`email`),
    PRIMARY KEY (`id`)
) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;
