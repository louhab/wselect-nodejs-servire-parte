"use strict";
import prisma from "../helpers/prisma.helper.js";
import seedUser from "./seeders/seed-user.js";
import seedDiplome from "./seeders/seed-diplome.js";
import seedDocs from "./seeders/seed-docs.js";
async function main() {
	seedUser();
	seedDiplome();
	seedDocs();
}

main()
	.catch((e) => {
		console.log(e);
		process.exit(1);
	})
	.finally(() => {
		prisma.$disconnect();
	});
