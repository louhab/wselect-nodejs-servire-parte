import { PrismaClient } from "@prisma/client";
import bcrypt from "bcrypt";

const prisma = new PrismaClient();

async function seedUser() {
	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash("qwertz", salt);
	let seeds;
	try {
		seeds = await prisma.user.create({
			data: {
				fullname: "Admin Boom",
				email: "admin@boom-digital.ma",
				password: hashedPassword,
				phone: "+2126666666666",
				role: "Admin",
			},
		});
	} catch (error) {}

	if (seeds) {
		console.log("🌱 User seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}
	await prisma.$disconnect();
}

export default seedUser;
