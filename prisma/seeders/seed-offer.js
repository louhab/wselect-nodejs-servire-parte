import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

async function seedOffer() {
	let seeds;
	try {
		seeds = await prisma.Offer.create({
			data: {
				title: "New offre has comme",
				diplomasName: "bac+5",
				isDiplomasRequired: true,
				minExpYears: "one",
                minLanguageLevel: "one",
                employerId : 1,
			},
		});
	} catch (error) {
        console.log(error);
    }

	if (seeds) {
		console.log("🌱 Offer seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}
	await prisma.$disconnect();
}

export default seedOffer;
