import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedCandidate() {
	const data = 
	{ 
        firstName: "Candidate",
        lastName: "Candidate last name",
        phone:"+212 657 444 566",
        address: "Address of the Candidate",
        email: "emailoftheCandidate@test.com",
        password: "$2b$10$OsPYrWhpdk9v1V25hXzebuAEsQ0RsZk9jwquCPRTFooe2BzU9LlZa",
        diplomasName:"Diplome Name",
        cvPath : '/upload/cv.pdf',
     }
	
	let seeds;
	try {
		seeds = await prisma.Candidate.createMany({
			data
	   });
	
	} catch (error) {
		console.log(error);
	}

	if (seeds) {
		console.log("🌱 Candidate seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}
	await prisma.$disconnect();
}

export default seedCandidate;
