import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedUsers() {
    for(let i=0; i<40; i++) {
        const data = {
            fullname:'Boom , user'+i,
            email:'user'+i+'@example.com',
            // password is (1111)
            password: '$2a$10$CwTycUXWue0Thq9StjUM0u3yMNCJdqXdJWy1uFHwpa2dZoMjrgkOW',
            phone:'212676879810',
            role: "Admin"
        }
      await prisma.User.createMany({
		data,
	});
    }
	console.log("🌱 User seeded successfully!");
	await prisma.$disconnect();
}

export default seedUsers;
