import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedEmploymentType() {
	const data = [{ label: "label 1" }, { label: "label 2" }];

	const seeds = await prisma.employmentType.createMany({
		data,
	});

	if (seeds) {
		console.log("🌱 Employment Types seeded successfully!");
	}

	await prisma.$disconnect();
}

export default seedEmploymentType;
