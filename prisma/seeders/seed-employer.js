import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedEmployer() {
	
    for(let i=0; i<40; i++) {
        const data = {
            companyName:'Boom Digitale',
            contactName:'Nabil',
            contactPhone : '+212 7 03 15 57 57',
            address: 'Marina Tour Crystal 1 10ème étage Boulevard des Almohades 20000 Casablanca, Maroc',
            email : 'nabil@boom-digitale.com'+i,
            password : '$2b$10$Y4.nJmZeyOMAUoY0X6CS3OX.e.pk6.62t0zLtmwmLQQXuspG8pKoO',
            website: 'https://boom-digital.ma/',
            remarks: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas massa erat, fringilla in mollis in, malesuada at urna. Quisque augue mi, tristique ac elit at, ornare efficitur tortor. Proin rutrum neque eu suscipit sagittis. Donec id convallis ante. Vivamus sollicitudin nibh vel sem iaculis condimentum. Aenean mauris nulla, feugiat non odio sagittis, finibus consequat metus. Etiam aliquam vulputate purus, vitae tempus mi accumsan vel. Vestibulum dapibus mauris quis neque posuere tempus. In pretium consequat orci, nec porttitor tellus.',
            contract: '/upload/cv.pdf',
    
        }
    let seeds = await prisma.Employer.createMany({
		data,
	});
    }


	if (seeds) {
		console.log("🌱 Employer seeded successfully!");
	}

	await prisma.$disconnect();
}

export default seedEmployer;
