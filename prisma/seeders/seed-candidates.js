import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedCandidates() {
  
    for(let i=0; i<40; i++) {
        const data =   { 
            firstName: "Candidate"+i,
            lastName: "Candidate last name"+i,
            phone:"+212 657 444 566"+i,
            address: "Address of the Candidate"+i,
            email: "emailoftheCandidate@test.com"+i,
            password: "$2b$10$OsPYrWhpdk9v1V25hXzebuAEsQ0RsZk9jwquCPRTFooe2BzU9LlZa",
            diplomasName:"Diplome Name"+i,
            cvPath : '/upload/cv.pdf',
         }
      await prisma.Candidate.createMany({
		data,
	});
    }
	console.log("🌱 Candidates seeded successfully!");
	await prisma.$disconnect();
}

export default seedCandidates;
