import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient();

const rawJobFields = [
	{
		label_fr: "Gestion de l'administration des Affaires",
		category: "Administration des Affaires",
		categoryId: 1,
	},
	{
		label_fr: "Ressources Humaines",
		category: "Administration des Affaires",
		categoryId: 1,
	},
	{
		label_fr: "Service à la clientèle",
		category: "Administration des Affaires",
		categoryId: 1,
	},
	{
		label_fr: "Agriculteur",
		category: "Agriculture",
		categoryId: 2,
	},
	{
		label_fr: "Conseiller agricole",
		category: "Agriculture",
		categoryId: 2,
	},
	{
		label_fr: "Ingénieur agronome",
		category: "Agriculture",
		categoryId: 2,
	},
	{
		label_fr: "Architecte",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Architecte d'intérieur",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Agent de développement local",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Génie civil",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Dessinateur- projeteur",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Géomètre",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Topographe",
		category: "Architecture et Génie civil",
		categoryId: 3,
	},
	{
		label_fr: "Ingénierie en construction",
		category: "Construction et BTP",
		categoryId: 4,
	},
	{
		label_fr: "Briquetage",
		category: "Construction et BTP",
		categoryId: 4,
	},
	{
		label_fr: "Maçonnerie",
		category: "Construction et BTP",
		categoryId: 4,
	},
	{
		label_fr: "Electricité",
		category: "Construction et BTP",
		categoryId: 4,
	},
	{
		label_fr: "Plomberie",
		category: "Construction et BTP",
		categoryId: 4,
	},
	{
		label_fr: "Peinture en BTP",
		category: "Construction et BTP",
		categoryId: 4,
	},
	{
		label_fr: "Art appliqué",
		category: "Design - Art",
		categoryId: 5,
	},
	{
		label_fr: "Audio-visuel",
		category: "Design - Art",
		categoryId: 5,
	},
	{
		label_fr: "Infographie",
		category: "Design - Art",
		categoryId: 5,
	},
	{
		label_fr: "Design",
		category: "Design - Art",
		categoryId: 5,
	},
	{
		label_fr: "Design industriel",
		category: "Design - Art",
		categoryId: 5,
	},
	{
		label_fr: "Ingénierie et Génie biomédical",
		category: "Domaine de santé",
		categoryId: 6,
	},
	{
		label_fr: "Pharmacie",
		category: "Domaine de santé",
		categoryId: 6,
	},
	{
		label_fr: "Dentaire",
		category: "Domaine de santé",
		categoryId: 6,
	},
	{
		label_fr: "Infirmerie",
		category: "Domaine de santé",
		categoryId: 6,
	},
	{
		label_fr: "Kinésithérapie",
		category: "Domaine de santé",
		categoryId: 6,
	},
	{
		label_fr: "Métiers de santé",
		category: "Domaine de santé",
		categoryId: 6,
	},
	{
		label_fr: "Droit",
		category: "Droit et Justice de protection du public",
		categoryId: 7,
	},
	{
		label_fr: "Protection du public",
		category: "Droit et Justice de protection du public",
		categoryId: 7,
	},
	{
		label_fr:
			"Conseiller /ère en sécurité, enquêtes et services judiciaires",
		category: "Droit et Justice de protection du public",
		categoryId: 7,
	},
	{
		label_fr: "Attaché Judiciaire",
		category: "Droit et Justice de protection du public",
		categoryId: 7,
	},
	{
		label_fr: "Cadre juridique",
		category: "Droit et Justice de protection du public",
		categoryId: 7,
	},
	{
		label_fr: "Police",
		category: "Droit et Justice de protection du public",
		categoryId: 7,
	},
	{
		label_fr: "Enseignant",
		category: "Enseignement et Education",
		categoryId: 8,
	},
	{
		label_fr: "Coach",
		category: "Enseignement et Education",
		categoryId: 8,
	},
	{
		label_fr: "Educateur/ éducatrice",
		category: "Enseignement et Education",
		categoryId: 8,
	},
	{
		label_fr: "Topographe",
		category: "Enseignement et Education",
		categoryId: 8,
	},
	{
		label_fr: "Technicien en électricité",
		category: "Electricité",
		categoryId: 9,
	},
	{
		label_fr: "Installation électrique",
		category: "Electricité",
		categoryId: 9,
	},
	{
		label_fr: "Ingénieur en  électronique",
		category: "Electronique",
		categoryId: 10,
	},
	{
		label_fr: "Technicien en électronique",
		category: "Electronique",
		categoryId: 10,
	},
	{
		label_fr: "Energie renouvelable",
		category: "Environnement",
		categoryId: 11,
	},
	{
		label_fr: "Biologiste en environnement",
		category: "Environnement",
		categoryId: 11,
	},
	{
		label_fr: "Animateur nature",
		category: "Environnement",
		categoryId: 11,
	},
	{
		label_fr: "Conseiller en environnement",
		category: "Environnement",
		categoryId: 11,
	},
	{
		label_fr: "Chargé d'hygiène et de sécurité",
		category: "Environnement",
		categoryId: 11,
	},
	{
		label_fr: "Comptabilité",
		category: "Finance Banque et Assurance",
		categoryId: 12,
	},
	{
		label_fr: "Trésorerie",
		category: "Finance Banque et Assurance",
		categoryId: 12,
	},
	{
		label_fr: "Fiscalité",
		category: "Finance Banque et Assurance",
		categoryId: 12,
	},
	{
		label_fr: "Analyse financière",
		category: "Finance Banque et Assurance",
		categoryId: 12,
	},
	{
		label_fr: "Directeur d’hôtel",
		category: "Hôtellerie",
		categoryId: 13,
	},
	{
		label_fr: "Agent d’Accueil",
		category: "Hôtellerie",
		categoryId: 13,
	},
	{
		label_fr: "Voiturier",
		category: "Hôtellerie",
		categoryId: 13,
	},
	{
		label_fr: "Responsable de salle",
		category: "Hôtellerie",
		categoryId: 13,
	},
	{
		label_fr: "Femme de chambre",
		category: "Hôtellerie",
		categoryId: 13,
	},
	{
		label_fr: "Informatique",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Système d'information",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Développement web",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Développement mobile",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Intelligence artificielle",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Ingénierie en cyber sécurité",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Webmaster",
		category: "IT Système d'information - Informatique",
		categoryId: 14,
	},
	{
		label_fr: "Acheteur",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Technicien logistique",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Approvisionneur",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Cariste",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Magasinier",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Manutentionnaire",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Logisticien",
		category: "Logistique  et approvisionnement",
		categoryId: 15,
	},
	{
		label_fr: "Commerce",
		category: "Management - International Business",
		categoryId: 16,
	},
	{
		label_fr: "Commerce international",
		category: "Management - International Business",
		categoryId: 16,
	},
	{
		label_fr: "Communication",
		category: "Management - International Business",
		categoryId: 16,
	},
	{
		label_fr: "Gestion",
		category: "Management - International Business",
		categoryId: 16,
	},
	{
		label_fr: "Marketing",
		category: "Management - International Business",
		categoryId: 16,
	},
	{
		label_fr: "Management de projet",
		category: "Management - International Business",
		categoryId: 16,
	},
	{
		label_fr: "Mécanique d’industrie",
		category: "Mécanique",
		categoryId: 17,
	},
	{
		label_fr: "Mécanique de voiture",
		category: "Mécanique",
		categoryId: 17,
	},
	{
		label_fr: "Mécanicien Véhicules de transport",
		category: "Mécanique",
		categoryId: 17,
	},
	{
		label_fr: "Débosseleur / Carrossier",
		category: "Mécanique",
		categoryId: 17,
	},
	{
		label_fr: "Soudeur",
		category: "Mécanique",
		categoryId: 17,
	},
	{
		label_fr: "Ingénieur minier",
		category: "Mine",
		categoryId: 18,
	},
	{
		label_fr: "Inspecteur des mines",
		category: "Mine",
		categoryId: 18,
	},
	{
		label_fr: "Foreur",
		category: "Mine",
		categoryId: 18,
	},
	{
		label_fr: "Mineur",
		category: "Mine",
		categoryId: 18,
	},
	{
		label_fr: "Chef de cuisine",
		category: "Restauration",
		categoryId: 19,
	},
	{
		label_fr: "Cuisinier (ère)",
		category: "Restauration",
		categoryId: 19,
	},
	{
		label_fr: "Commis",
		category: "Restauration",
		categoryId: 19,
	},
	{
		label_fr: "Serveur",
		category: "Restauration",
		categoryId: 19,
	},
	{
		label_fr: "Sciences",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sciences humaines",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sciences politiques",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sciences vétérinaires",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sciences dentaires",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sciences biomédicales et pharmaceutiques",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sciences psychologiques",
		category: "Sciences",
		categoryId: 20,
	},
	{
		label_fr: "Sociologie",
		category: "Sociologie",
		categoryId: 21,
	},
	{
		label_fr: "Géopolitique",
		category: "Sociologie",
		categoryId: 21,
	},
	{
		label_fr: "Conseiller en mode",
		category: "Stylisme et Mode",
		categoryId: 22,
	},
	{
		label_fr: "Designer en textile",
		category: "Stylisme et Mode",
		categoryId: 22,
	},
	{
		label_fr: "Coordinateur de mode",
		category: "Stylisme et Mode",
		categoryId: 22,
	},
	{
		label_fr: "Styliste",
		category: "Stylisme et Mode",
		categoryId: 22,
	},
];

function getJobFields(data) {
	const array = [];
	for (const el of data) {
		array.push({ label: el.label_fr });
	}
	return array;
}

async function seedJobFields() {
	const jobFields = getJobFields(rawJobFields);
	const record = await prisma.jobField.createMany({
		data: jobFields,
	});

	if (record) {
		console.log("🌱 Job Fields seeded successfully!");
	}

	await prisma.$disconnect();
}

export default seedJobFields;
