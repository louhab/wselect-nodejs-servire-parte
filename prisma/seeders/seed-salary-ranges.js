import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedSalaryRange() {
	const data = [{ label: "label 1" }, { label: "label 2" }];

	const seeds = await prisma.salaryRange.createMany({
		data,
	});

	if (seeds) {
		console.log("🌱 salary Ranges seeded successfully!");
	}

	await prisma.$disconnect();
}

export default seedSalaryRange;
