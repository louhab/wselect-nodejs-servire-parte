import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedCandidatetesOnOffers() {
    function getRandomNumberBetween(min, max) {
        const randomDecimal = Math.random();
        const randomInRange = min + randomDecimal * (max - min);
        const randomWholeNumber = Math.floor(randomInRange);
        return randomWholeNumber;
      }
    for(let i=0; i<40; i++) {
    //     const data = {
    //        offerId:getRandomNumberBetween(1,41) ,
        
    //        candidateId : getRandomNumberBetween(1,41) 
    //     }
    //   await prisma.CandidatesOnOffers.createMany({
	// 	data,
	// });
    const deleteResult = await prisma.CandidatesOnOffers.deleteMany();
    
    console.log('Records deleted:', deleteResult.count);
    // prisma.CandidatesOnOffers.deleteMany();
    }
	console.log("🌱 Candidates on Offers seeded successfully!");
	await prisma.$disconnect();
}

export default seedCandidatetesOnOffers;
