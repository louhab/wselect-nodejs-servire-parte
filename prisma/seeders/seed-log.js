import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedLog() {
	const data = [
		{ action: "Log test" },
	];
	let seeds;
	try {
		seeds = await prisma.Log.createMany({
			data
	   });
	
	} catch (error) {
		console.log(error);
	}

	if (seeds) {
		console.log("🌱 Log seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}
	await prisma.$disconnect();
}

export default seedLog;
