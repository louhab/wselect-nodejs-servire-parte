import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedCountry() {
	const data = [{ label: "Canada" }, { label: "Maroc" }];
	const seeds = await prisma.country.createMany({
		data,
	});

	if (seeds) {
		console.log("🌱 Countries seeded successfully!");
	}

	await prisma.$disconnect();
}

export default seedCountry;
