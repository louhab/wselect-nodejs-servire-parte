import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedAbonnement() {
    for(let i=0; i<40; i++) {
        const data = {
            title:'Boom Digitale'+i,
            details:'Nabil'+i
        }
      await prisma.Abonnements.createMany({
		data,
	});
    }
	console.log("🌱 Abonnements seeded successfully!");
	await prisma.$disconnect();
}

export default seedAbonnement;
