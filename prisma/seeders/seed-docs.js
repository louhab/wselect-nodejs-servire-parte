import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
export async function seedDocs() {

let seeds;
try {

    let seeds;
    let  Docs = [
            {
                name: "1 Wselect offre de service",
                path: "1_wselect_offre_de_service.pdf",
            },
            { name: "2 Wselect About Us", path: "2_wselect_about_us.pdf" },
            {
                name: "3 Wselect Recrutement-Expert",
                path: "3_wselect _recrutement_expert.pdf",
            },
            {
                name: "4 Wselect notre processus de recrutement",
                path: "4_wselect_notre_processus_de_recrutement.pdf",
            },
            {
                name: "5 Wselect Doc Programme TET",
                path: "5_wselect_doc_programme_TET.pdf",
            },
            {
                name: "6 Wselect Devis profil bas salaire",
                path: "6_wselect_devis_profil_bas_salaire.pdf",
            },
            {
                name: "7 Wselect Devis profil haut salaire",
                path: "7_wselect_devis_profil_haut_salaire.pdf",
            },
            { name: "8 Contrat Wselect", path: "8_contrat_wselect.pdf" },
        ];
        seeds = await prisma.Doc.createMany({
                data:Docs,
	     });
if (seeds) {
		console.log("🌱 Docs seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}
	 prisma.$disconnect();



    // seeds = await prisma.Offer.create({
    //     data: {
    //         title: "New offre has comme",
    //         diplomasName: "bac+5",
    //         isDiplomasRequired: true,
    //         minExpYears: "+one",
    //         minLanguageLevel: "one",
    //         employerId : 1,
    //         descriptio : "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas massa erat, fringilla in mollis in, malesuada at urna. Quisque augue mi, tristique ac elit at, ornare efficitur tortor. Proin rutrum neque eu suscipit sagittis. Donec id convallis ante. Vivamus sollicitudin nibh vel sem iaculis condimentum. Aenean mauris nulla, feugiat non odio sagittis, finibus consequat metus. Etiam aliquam vulputate purus, vitae tempus mi accumsan vel. Vestibulum dapibus mauris quis neque posuere tempus. In pretium consequat orci, nec porttitor tellus.",
    //     },
    // });
} catch (error) {
    console.log(error);
}

if (seeds) {
    console.log("🌱 Offer seeded successfully!");
} else {
    console.log("There is a conflict ❌");
}
await prisma.$disconnect();
}
export default seedDocs
