import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedDiplome() {
	const data = [
        { 
            name: "bac e+ 2",
            isActive : true
    
         },
         { 
            name: "bac e+ 3",
            isActive : true
    
         },
         { 
            name: "bac e+ 5",
            isActive : true
    
         },
         { 
            name: "bac e+ 6",
            isActive : true
    
         },
    ]
	
	
	let seeds;
	try {
		seeds = await prisma.Diplome.createMany({
			data
	   });
	
	} catch (error) {
		console.log(error);
	}

	if (seeds) {
		console.log("🌱 Diplome seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}
	await prisma.$disconnect();
}

export default seedDiplome;
