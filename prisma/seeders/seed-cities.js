import { PrismaClient } from "@prisma/client";
const prisma = new PrismaClient();
async function seedCitys() {
	const data = [
		{ label: "Barrie, Ontario" },
		{ label: "Calgary, Alberta" },
		{ label: "Edmonton, Alberta" },
		{ label: "Halifax, Nova Scotia" },
		{ label: "Hamilton, Ontario" },
		{ label: "Kelowna, British Columbia" },
		{ label: "Kitchener, Ontario" },
		{ label: "London, Ontario" },
		{ label: "Montreal, Quebec" },
		{ label: "Oshawa, Ontario" },
		{ label: "Ottawa, Ontario" },
		{ label: "Quebec City, Quebec" },
		{ label: "Regina, Saskatchewan" },
		{ label: "Saskatoon, Saskatchewan" },
		{ label: "St. John's, Newfoundland and Labrador" },
		{ label: "Toronto, Ontario" },
		{ label: "Vancouver, British Columbia" },
		{ label: "Victoria, British Columbia" },
		{ label: "Windsor, Ontario" },
		{ label: "Winnipeg, Manitoba" },
	];
	let seeds;
	try {
		seeds = await prisma.city.createMany({
			data
	   });
	
	} catch (error) {
		console.log(error);
	}

	if (seeds) {
		console.log("🌱 city seeded successfully!");
	} else {
		console.log("There is a conflict ❌");
	}

	await prisma.$disconnect();
}

export default seedCitys;
