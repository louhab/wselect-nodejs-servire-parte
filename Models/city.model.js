"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a City from an ID
 * @param {Number} Id
 * @returns {Object} City data
 */
const findCityById = async (id) => {
	const city = prisma.city.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return city;
};

/**
 * Get All Citys
 * @returns {Object}
 */
const findAllCitys = async (skip = null, take = null, filters = {}) => {
	const citys = await prisma.city.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return citys;
};

/**
 * Add a new City
 * @param {Object}
 * @returns {Object}
 */
const addCity = async (data) => {
	const city = await prisma.city.create({
		data,
	});
	return city;
};

/**
 * Update a City
 * @param {Object}
 * @returns {Number}
 */
const editCity = async (id, data) => {
	const city = await prisma.city.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return city;
};

/**
 * Delete a City (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCity = async (id) => {
	const city = await prisma.city.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return city;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export { findCityById, findAllCitys, addCity, editCity, deleteCity };
