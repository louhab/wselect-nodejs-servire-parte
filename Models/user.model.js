"use strict";
import prisma from "../helpers/prisma.helper.js";
import bcrypt from "bcrypt";

/**
 * Get a User from an ID
 * @param {Number} Id
 * @returns {Object} User data
 */
const findUserById = async (id) => {
	const user = prisma.user.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			fullname: true,
			email: true,
			password: true,
			phone: true,
			role: true,
			createdAt: true,
		},
	});
	return user;
};

/**
 * Get All Users
 * @returns {Object}
 */
const findAllUsers = async (skip,take) => {
	return {
		allusers :  await prisma.user.findMany({
			where: { 
			OR:[{deletedAt: null}],
			NOT: [{role: 'Admin'}]
		}
	}),
		users : await prisma.user.findMany({
			skip :skip,
			take:take,
			where: { 
				OR:[{deletedAt: null}],
				NOT: [{role: 'Admin'}]
			},
			select: {
				id: true,
				fullname: true,
				email: true,
				password: true,
				phone: true,
				role: true,
				createdAt: true,
			},
		}),
	} 
};

/**
 * Add a new User
 * @param {Object}
 * @returns {Object}
 */
const addUser = async (data) => {
	const salt = await bcrypt.genSalt(10);
	const hashedPassword = await bcrypt.hash(data.password, salt);
	const user = await prisma.user.create({
		data: {
            ...data,
            password: hashedPassword 
        },
		select: {
			id: true,
			fullname: true,
			email: true,
			role: true,
			phone: true,
			createdAt: true,
			password:true,
		},
	});
	return user;
};

/**
 * Update a User
 * @param {Object}
 * @returns {Number}
 */
const editUser = async (id, data) => {
	const user = await prisma.user.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return user;
};

/**
 * Delete a User (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteUser = async (id) => {
	const user = await prisma.user.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return user;
};

export { findUserById, findAllUsers, addUser, editUser, deleteUser };
