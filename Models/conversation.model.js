"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Conversation from an ID
 * @param {Number} Id
 * @returns {Object} Conversation data
 */
const findConversationById = async (type, id) => {
  const conversations = await prisma.conversation.findMany({
    // 	where:
    // 		{deletedAt: null},
    // 			select: {
    // 				id: true,
    // 				name: true,
    // 				isActive: true,
    // 			},

    // where: { id: +conversationId },
    where: { [`${type}Id`]: +id },
    include: { messages: { orderBy: { createdAt: "asc" } }}
  });

  return conversations;
};

/**
 * Get All Conversations
 * @returns {Object}
 */

// skip = null, take = null, filters = {}
const findAllConversations = async () => {
  const conversations = await prisma.conversation.findMany({
    // 	where:
    // 		{deletedAt: null},
    // 			select: {
    // 				id: true,
    // 				name: true,
    // 				isActive: true,
    // 			},

    // where: { id: +conversationId },
    include: { messages: { orderBy: { createdAt: "asc" } } }
  });
  return conversations;
};

/**
 * Add a new Conversation
 * @param {Object}
 * @returns {Object}
 */
const addConversation = async data => {
  const conversation = await prisma.conversation.create({
    data
  });
  return conversation;
};

const addMessage = async data => {
  const newMessage = await prisma.message.create({
    data
  });
  return newMessage;
};

/**
 * Delete a Conversation (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteConversation = async id => {
  const conversation = await prisma.conversation.delete({
    where: {
      id,
      // AND: {
      //   deletedAt: null
      // }
    },
    // data: {
    //   deletedAt: new Date()
    // }
  });
  return conversation;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
  findConversationById,
  findAllConversations,
  addConversation,
  deleteConversation,
  addMessage
};
