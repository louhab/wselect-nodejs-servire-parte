"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a CarrerLevel from an ID
 * @param {Number} Id
 * @returns {Object} CarrerLevel data
 */
const findCarrerLevelById = async (id) => {
	const carrerLevel = prisma.carrerLevel.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return carrerLevel;
};

/**
 * Get All CarrerLevels
 * @returns {Object}
 */
const findAllCarrerLevels = async (skip = null, take = null, filters = {}) => {
	const carrerLevels = await prisma.carrerLevel.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return carrerLevels;
};

/**
 * Add a new CarrerLevel
 * @param {Object}
 * @returns {Object}
 */
const addCarrerLevel = async (data) => {
	const carrerLevel = await prisma.carrerLevel.create({
		data,
	});
	return carrerLevel;
};

/**
 * Update a CarrerLevel
 * @param {Object}
 * @returns {Number}
 */
const editCarrerLevel = async (id, data) => {
	const carrerLevel = await prisma.carrerLevel.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return carrerLevel;
};

/**
 * Delete a CarrerLevel (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCarrerLevel = async (id) => {
	const carrerLevel = await prisma.carrerLevel.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return carrerLevel;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findCarrerLevelById,
	findAllCarrerLevels,
	addCarrerLevel,
	editCarrerLevel,
	deleteCarrerLevel,
};
