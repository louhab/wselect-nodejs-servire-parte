"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a EmployerForm from an uuid
 * @param {String} uuid
 * @returns {Object} EmployerForm data
 */
const findEmployerFormById = async (employerId) => {
	const employerForm = prisma.employerForm.findFirst({
		where: {
			employerId,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			uuid: true,
			accountNumber: true,
			companyName: true,
			companyAddress: true,
			companyCity: true,
			companyProvince: true,
			companyCountry: true,
			companyZip: true,
			companyAddressBis: true,
			companyCityBis: true,
			companyProvinceBis: true,
			companyCountryBis: true,
			companyZipBis: true,
			website: true,
			startDate: true,
			isIndividual: true,
			isPartnership: true,
			isCompany: true,
			isCooperative: true,
			isNonprofit: true,
			isRegisteredCharity: true,
			firstName: true,
			secondFirstName: true,
			lastName: true,
			positionTitle: true,
			firstPhone: true,
			firstPosition: true,
			secondPhone: true,
			secondPosition: true,
			faxNumber: true,
			address: true,
			city: true,
			province: true,
			country: true,
			zip: true,
			firstNameBis: true,
			secondFirstNameBis: true,
			lastNameBis: true,
			positionTitleBis: true,
			firstPhoneBis: true,
			firstPositionBis: true,
			secondPhoneBis: true,
			secondPositionBis: true,
			faxNumberBis: true,
			emailBis: true,
			addressBis: true,
			cityBis: true,
			provinceBis: true,
			countryBis: true,
			zipBis: true,
			secThree1: true,
			secThree2: true,
			secThree3: true,
			secThree4: true,
			secThree5: true,
			secThree6: true,
			secThree7: true,
			secThree8: true,
			secThree9: true,
			secThree10: true,
			tradeName: true,
			workspaceDescription: true,
			workspaceAddress: true,
			workspaceCity: true,
			workspaceProvince: true,
			workspaceZip: true,
			secFive1: true,
			secFive2: true,
			secFive3: true,
			secFive4: true,
			secFive5: true,
			secFive6: true,
			secFive7: true,
			secFive8: true,
			secFive9: true,
			secFive10: true,
			secFive11: true,
			secFive12: true,
			secFive13: true,
			secFive14: true,
			secFive15: true,
			secFive16: true,
			secFive17: true,
			secFive18: true,
			secFive19: true,
			createdAt: true,
			updatedAt: true,
		},
	});
	return employerForm;
};

/**
 * Get All EmployerForms
 * @returns {Object}
 */
const findAllEmployerForms = async (skip = null, take = null, filters = {}) => {
	const employers = await prisma.employerForm.findMany({
		where: filters,
		select: {
			id: true,
			uuid: true,
			accountNumber: true,
			companyName: true,
			companyAddress: true,
			companyCity: true,
			companyProvince: true,
			companyCountry: true,
			companyZip: true,
			companyAddressBis: true,
			companyCityBis: true,
			companyProvinceBis: true,
			companyCountryBis: true,
			companyZipBis: true,
			website: true,
			startDate: true,
			isIndividual: true,
			isPartnership: true,
			isCompany: true,
			isCooperative: true,
			isNonprofit: true,
			isRegisteredCharity: true,
			firstName: true,
			secondFirstName: true,
			lastName: true,
			positionTitle: true,
			firstPhone: true,
			firstPosition: true,
			secondPhone: true,
			secondPosition: true,
			faxNumber: true,
			address: true,
			city: true,
			province: true,
			country: true,
			zip: true,
			firstNameBis: true,
			secondFirstNameBis: true,
			lastNameBis: true,
			positionTitleBis: true,
			firstPhoneBis: true,
			firstPositionBis: true,
			secondPhoneBis: true,
			secondPositionBis: true,
			faxNumberBis: true,
			emailBis: true,
			addressBis: true,
			cityBis: true,
			provinceBis: true,
			countryBis: true,
			zipBis: true,
			secThree1: true,
			secThree2: true,
			secThree3: true,
			secThree4: true,
			secThree5: true,
			secThree6: true,
			secThree7: true,
			secThree8: true,
			secThree9: true,
			secThree10: true,

			tradeName: true,
			workspaceDescription: true,
			workspaceAddress: true,
			workspaceCity: true,
			workspaceProvince: true,
			workspaceZip: true,

			secFive1: true,
			secFive2: true,
			secFive3: true,
			secFive4: true,
			secFive5: true,

			secFive6: true,
			secFive7: true,

			secFive8: true,

			secFive9: true,
			secFive10: true,

			secFive11: true,
			// TODO upload ?
			secFive12: true,

			secFive13: true,
			secFive14: true,
			secFive15: true,
			secFive16: true,
			secFive17: true,

			secFive18: true,
			secFive19: true,
			createdAt: true,
			updatedAt: true,
		},
	});
	return employers;
};

/**
 * Add a new EmployerForm
 * @param {Object}
 * @returns {Object}
 */
const addEmployerForm = async (data) => {
	const employerForm = await prisma.employerForm.create({
		data,
	});
	return employerForm;
};

/**
 * Add a new EmployerForm
 * @param {Object}
 * @returns {Object}
 */
const countEmployerForm = async (email) => {
	const employerForm = await prisma.employerForm.count({
		where: {
			email,
		},
	});
	return Boolean(employerForm);
};

/**
 *
 * @param {String} email
 * @param {Object} data
 * @returns number
 */
const editEmployerForm = async (employerId, data) => {
	const employerForm = await prisma.employerForm.updateMany({
		where: {
			employerId,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return employerForm;
};

/**
 * Delete a EmployerForm (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteEmployerForm = async (id) => {
	const employerForm = await prisma.employerForm.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return employerForm;
};

export {
	findEmployerFormById,
	findAllEmployerForms,
	addEmployerForm,
	editEmployerForm,
	deleteEmployerForm,
	countEmployerForm,
};
