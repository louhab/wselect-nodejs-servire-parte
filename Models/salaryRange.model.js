"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a SalaryRange from an ID
 * @param {Number} Id
 * @returns {Object} SalaryRange data
 */
const findSalaryRangeById = async (id) => {
	const salaryRange = prisma.salaryRange.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return salaryRange;
};

/**
 * Get All SalaryRanges
 * @returns {Object}
 */
const findAllSalaryRanges = async (skip = null, take = null, filters = {}) => {
	const salaryRanges = await prisma.salaryRange.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return salaryRanges;
};

/**
 * Add a new SalaryRange
 * @param {Object}
 * @returns {Object}
 */
const addSalaryRange = async (data) => {
	const salaryRange = await prisma.salaryRange.create({
		data,
	});
	return salaryRange;
};

/**
 * Update a SalaryRange
 * @param {Object}
 * @returns {Number}
 */
const editSalaryRange = async (id, data) => {
	const salaryRange = await prisma.salaryRange.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return salaryRange;
};

/**
 * Delete a SalaryRange (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteSalaryRange = async (id) => {
	const salaryRange = await prisma.salaryRange.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return salaryRange;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findSalaryRangeById,
	findAllSalaryRanges,
	addSalaryRange,
	editSalaryRange,
	deleteSalaryRange,
};
