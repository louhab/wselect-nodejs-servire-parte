"use strict";
import prisma from "../helpers/prisma.helper.js";
const status = {
	In_progress: "En_Coure",
	Validited: "validée",
	Rejected: "rejetée",
}


/**
 * Add a new Employer Contract 
 * @param {Object}
 * @returns {Object}
 */
const addEmployerContract = async (data) => {
	data.status = status.In_progress
	const employerContract = await prisma.EmployerContrat.create({data});
	return employerContract;
};


export {
    addEmployerContract
};
