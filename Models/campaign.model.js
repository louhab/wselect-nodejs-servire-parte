"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Campaign from an ID
 * @param {Number} Id
 * @returns {Object} Campaign data
 */
const findCampaignById = async (id) => {
	const campaign = await prisma.campaign.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			ref: true,
			offerId: true,
			offer: {
				select: {
					title: true,
					isDiplomasRequired: true,
					minExpYears: true,
					minLanguageLevel: true,
				},
			},
			createdAt: true,
		},
	});
	return campaign;
};

/**
 * Get a Campaign from an ID
 * @param {Number} Id
 * @returns {Object} Campaign data
 */
const findCampaignByRef = async (uid) => {
	const campaign = await prisma.campaign.findFirst({
		where: {
			ref: uid,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			ref: true,
			createdAt: true,
		},
	});
	return campaign ;
	if (campaign === null) return null;
	const offersOnCampaigns = await prisma.offersOnCampaigns.findFirst({
		where: {
			offerId,
			campaignId: campaign?.id,
		},
		select: {
			offer: {
				select: {
					title: true,
					diplomasName: true,
				},
			},
		},
	});

	return offersOnCampaigns;
};

/**
 * Get All Campaigns
 * @returns {Object}
 */
const findAllCampaigns = async (skip = null, take = null, filters = {}) => {
	const campaigns = await prisma.campaign.findMany({
		// skip,
		// take,
		where: filters,
		select: {
			id: true,
			ref: true,
			offerId: true,
			offer: {
				select: {
					title: true,
					isDiplomasRequired: true,
					minExpYears: true,
					minLanguageLevel: true,
				},
			},
			createdAt: true,
		},
	});
	return campaigns;
};

/**
 * Add a new Campaign
 * @param {Object}
 * @returns {Object}
 */
const addCampaign = async (data) => {
	const campaign = await prisma.campaign.create({
		data,
	});
	return campaign;
};

/**
 * Update a Campaign
 * @param {Object}
 * @returns {Number}
 */
const editCampaign = async (id, data) => {
	const campaign = await prisma.campaign.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return campaign;
};

/**
 * Delete a Campaign (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCampaign = async (id) => {
	const campaign = await prisma.campaign.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return campaign;
};

export {
	findCampaignById,
	findAllCampaigns,
	addCampaign,
	editCampaign,
	deleteCampaign,
	findCampaignByRef,
};
