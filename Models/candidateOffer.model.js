"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a CandidateOffer from an ID
 * @param {Number} Id
 * @returns {Object} CandidateOffer data
 */
const findCandidateOfferById = async (id) => {
	const candidateOffer = prisma.candidatesOnOffers.findFirst({
		where: {
			offerId: id,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			// id: true,
			offerId: true,
			candidateId: true,
			// createdAt: true,
		},
	});
	return candidateOffer;
};

/**
 * Get All CandidateOffers
 * @returns {Object}
 */
const findAllCandidateOffers = async () => {
	const candidateOffers = await prisma.candidatesOnOffers.findMany({});
	return candidateOffers;
};

/**
 * Add a new CandidateOffer
 * @param {Object}
 * @returns {Object}
 */
const addCandidateOffer = async (data) => {
	const candidateOffer = await prisma.candidatesOnOffers.create({
		data,
	});

	return candidateOffer;
};

/**
 * Update a CandidateOffer
 * @param {Object}
 * @returns {Number}
 */
const editCandidateOffer = async (id, data) => {
	const candidateOffer = await prisma.candidatesOnOffers.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return candidateOffer;
};

/**
 * Delete a CandidateOffer (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCandidateOffer = async (id) => {
	const candidateOffer = await prisma.candidatesOnOffers.delete({
		where: {
            // offerId_candidateId: {
            //     candidateId: id, //replace with appropriate variable
            //     offerId: 1, //replace with appropriate variable
            // },
	}});
	return candidateOffer;
};

/**
 * Assign candidateOffer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findCandidateOfferById,
	findAllCandidateOffers,
	addCandidateOffer,
	editCandidateOffer,
	deleteCandidateOffer,
};
