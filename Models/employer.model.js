"use strict";
import prisma from "../helpers/prisma.helper.js";
/**
 * Get a Employer from an Uuid
 * @param {String} Uuid
 * @returns {Object} Employer data
 */
const findEmployerByUuid = async (uuid) => {
	const employer =await prisma.employer.findFirst({
					where: {
							uuid: uuid,
							AND: {
								deletedAt: null,
							},
					},
					select : {
						id:true,
						companyName:true,
						contactName:true,
						contactPhone:true,
						password:true,
						address:true,
						email:true,
						website:true,
						remarks:true,
						contract:true,
						status:true,
						isActive:true,
						comment:true,
						Offer:{
							select:{
								id:true,
                                employerId:true,
								canidate:true
							}
						},
					}
	});
	return employer ;
};

/**
 * Get a Employer from an ID
 * @param {Number} Id
 * @returns {Object} Employer data
 */
const findEmployerById = async (key, val) => {
	const employer = await prisma.employer.findFirst({
					where: {
							[key]: val,
							AND: {
								deletedAt: null,
							},
					},
					select : {
						id:true,
						companyName:true,
						contactName:true,
						contactPhone:true,
						password:true,
						address:true,
						email:true,
						website:true,
						remarks:true,
						contract:true,
						status:true,
						isActive:true,
						comment:true,
						EmployerContrat : true,
						Offer:{
							select:{
								id:true,
                                employerId:true,
								canidate:true
							},
							where : {
								deletedAt:null
							}
						},
					}
	});
	return employer 
};

/**
 * Add a new EmployerForm
 * @param {Object}
 * @returns {Object}
 */
const countEmployer = async (email) => {
	const employerForm = await prisma.employer.count({
		where: {
			email,
			AND: {
				deletedAt: null,
			},
		},
	});

	return Boolean(employerForm);
};

/**
 * Get All Employers
 * @returns {Object}
 */
const findAllEmployers = async (skip,take) => {
	const employersCount = 	await prisma.employer.count({where : {deletedAt:null}});
	const allemployers = await prisma.employer.findMany({where:{ deletedAt: null }});
	const employers =  await prisma.employer.findMany({   skip :skip,take:take,
		where:{ deletedAt: null },
		orderBy: {
			createdAt: "asc",	
		  },
		select : {
			id:true,
			companyName:true,
			contactName:true,
			contactPhone:true,
			address:true,
			email:true,
			website:true,
			remarks:true,
			contract:true,
			status:true,
			isActive:true,
			createdAt :true,
			employerRef:true,
			Offer:{
				select:{
					id:true,
					employerId:true,
					canidate:true
				}
			},
		}
	})
	return {
		allemployers,
		employers,
		employersCount
	}

};

/**
 * Add a new Employer
 * @param {Object}
 * @returns {Object}
 */
const addEmployer = async (data) => {
	const transaction = await prisma.$transaction(async () => {

		const employer = await prisma.employer.create({
			data,
		});
		// sert à quoi employer Form ?

		await prisma.employerForm.create({
			data: { employerId: employer?.id },
		});

		return { employer };
	});

	return transaction?.employer;
};

/**
 * Update a Employer
 * @param {Object}
 * @returns {Number}
 */
const editEmployer = async (id, data) => {
	const employer = await prisma.employer.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return employer;
};

/**
 * Delete a Employer (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteEmployer = async (id) => {
	const employer = await prisma.employer.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return employer;
};

export {
	findEmployerById,
	findAllEmployers,
	addEmployer,
	editEmployer,
	deleteEmployer,
	countEmployer,
	findEmployerByUuid,
};
