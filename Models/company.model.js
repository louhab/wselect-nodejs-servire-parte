"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Company from an ID
 * @param {Number} Id
 * @returns {Object} Company data
 */
const findCompanyById = async (id) => {
	const company = prisma.companie.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			name: true,
			isActive: true,
			companySize: true,
			foundedIn: true,
			phone: true,
			email: true,
			website: true,
			discription: true,
			countryId: true,
			cityId: true,
			city: {},
			Country:{},
		
		},
	});
	return company;
};

/**
 * Get All Companys
 * @returns {Object}
 */
const findAllCompanys = async (skip = null, take = null, filters = {}) => {
	const companys = await prisma.companie.findMany({
		where: { deletedAt: null },
		select :{
			id: true,
			name: true,
			isActive: true,
			companySize: true,
			foundedIn: true,
			phone: true,
			countryId: true,
			email: true,
			Country: { },
			city : {}

		}
	});
	return companys;
};

/**
 * Add a new Company
 * @param {Object}
 * @returns {Object}
 */
const addCompany = async (data) => {
	const company = await prisma.companie.create({
		data,
	});
	return company;
};

/**
 * Update a Company
 * @param {Object}
 * @returns {Number}
 */
const editCompany = async (id, data) => {
	const company = await prisma.companie.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return company;
};

/**
 * Delete a Company (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCompany = async (id) => {
	const company = await prisma.companie.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return company;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findCompanyById,
	findAllCompanys,
	addCompany,
	editCompany,
	deleteCompany,
};
