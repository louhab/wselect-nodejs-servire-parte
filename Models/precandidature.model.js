"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a PreCandidate from an ID
 * @param {Number} Id
 * @returns {Object} PreCandidate data
 */
const findPreCandidateById = async (id) => {
	const pricandidate = prisma.PreCandidate.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
	});
	return pricandidate;
};

/**
 * Get All PreCandidate
 * @returns {Object}
 */
const findAllPreCandidate = async (skip, take) => {
    const precandidate = await prisma.PreCandidate.findMany({ where : {}})
    return precandidate;
};

/**
* Add a new PreCandidate
* @param {Object}
* @returns {Object}
*/
const addPreCandidate = async (data) => {
	const precandidate = await prisma.PreCandidate.create({
		data : {
		firstName: data.firstName,
		lastName: data.lastName,
		gender : data.gender,
		phone : data.phone,
		email : data.email,
		domaineDactiviteId : data.domaineDactiviteId || null,
		password : data.password,
		cvPath : data.cvPath,
		diplomeId: Number(data.diplomeId) || null,
		hasDiplomas : data.hasDiploma ,
		diplomasName : data.diplomasName || null,
		employerId : data.employerId || null,
		offerId : Number(data.offerId) || null,
		yearsExp : data.yearsExp || null,
		languageLevel : data.languageLevel || null,	
		}
	});
	return precandidate;

};

/**
 * Update a Offer
 * @param {Object}
 * @returns {Number}
 */
const editPreCandidate = async (id, data) => {
	const precandidate = await prisma.PreCandidate.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return precandidate;
};

/**
 * Delete a Offer (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deletePreCandidate = async (id) => {
	const precandidate = await prisma.PreCandidate.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return precandidate;
};

/**
 * Get Boolean if Pre candidate by mail exist
 * @param {Object}
 * @returns {Object}
 */
const countPreCandidate = async (email) => {
	const countPreCandidate = await prisma.PreCandidate.count({
		where: {
			email,
		},
	});
	const precandidate = await prisma.PreCandidate.findFirst({
		where: {
			email,
		}
	})

	return {
		precandidate: precandidate,
		existe : Boolean(countPreCandidate)
	};
};

export {
	findPreCandidateById,
	findAllPreCandidate,
	addPreCandidate,
	editPreCandidate,
	deletePreCandidate,
	countPreCandidate,
};



