"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Comment from an ID
 * @param {Number} Id
 * @returns {Object} Comment data
 */
const findCommentById = async (id) => {
	const comment = await prisma.comments.findMany({
		where: {
			employerId: id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			comment: true,
			user: {
				select: {
					fullname: true,
				},
			},
			createdAt: true,
		},
	});
	return comment;
};

/**
 * Get All Comments
 * @returns {Object}
 */
const findAllComments = async (id) => {
	const comments = await prisma.comments.findMany({
		where: { candidateId: id, deletedAt: null },
		select: {
			id: true,
			comment: true,
			employer:{},
			user: {
				select: {
					id:true,
					fullname: true,
					role:true
				},
			},
			createdAt: true,
		},
	});
	return comments;
};

/**
 * Add a new Comment
 * @param {Object}
 * @returns {Object}
 */
const addComment = async (data) => {
	const comment = await prisma.comments.create({
		data,
		select: {
			id: true,
			comment: true,
			user: {
				select: {
					fullname: true,
				},
			},
			createdAt: true,
		},
	});
	return comment;
};

/**
 * Update a Comment
 * @param {Object}
 * @returns {Number}
 */
const editComment = async (id, data) => {
	const comment = await prisma.comments.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return comment;
};

/**
 * Delete a Comment (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteComment = async (id) => {
	const comment = await prisma.comments.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return comment;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findCommentById,
	findAllComments,
	addComment,
	editComment,
	deleteComment,
};
