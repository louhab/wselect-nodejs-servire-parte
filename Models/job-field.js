"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Domaine from an ID
 * @param {Number} Id
 * @returns {Object} Domaine data
 */
const findJobFieldById = async (id) => {
	const record = await prisma.jobField.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return record;
};

/**
 * Get All JobFields
 * @returns {Object}
 */
const findAllJobFields = async (skip = null, take = null, filters = {}) => {
	const records = await prisma.jobField.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return records;
};

/**
 * Add a new JobField
 * @param {Object}
 * @returns {Object}
 */
const addJobField = async (data) => {
	const record = await prisma.jobField.create({ data });
	return record;
};

/**
 * Update a JobField
 * @param {Object}
 * @returns {Number}
 */
const editJobField = async (id, data) => {
	const record = await prisma.jobField.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return record;
};

/**
 * Delete a JobField (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteJobField = async (id) => {
	const record = await prisma.jobField.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return record;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findJobFieldById,
	findAllJobFields,
	addJobField,
	editJobField,
	deleteJobField,
};
