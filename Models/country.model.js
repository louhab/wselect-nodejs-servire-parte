"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Country from an ID
 * @param {Number} Id
 * @returns {Object} Country data
 */
const findCountryById = async (id) => {
	const country = prisma.country.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return country;
};

/**
 * Get All Countrys
 * @returns {Object}
 */
const findAllCountrys = async (skip = null, take = null, filters = {}) => {
	const countrys = await prisma.country.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return countrys;
};

/**
 * Add a new Country
 * @param {Object}
 * @returns {Object}
 */
const addCountry = async (data) => {
	const country = await prisma.country.create({
		data,
	});
	return country;
};

/**
 * Update a Country
 * @param {Object}
 * @returns {Number}
 */
const editCountry = async (id, data) => {
	const country = await prisma.country.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return country;
};

/**
 * Delete a Country (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCountry = async (id) => {
	const country = await prisma.country.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return country;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findCountryById,
	findAllCountrys,
	addCountry,
	editCountry,
	deleteCountry,
};
