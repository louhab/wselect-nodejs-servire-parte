"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Offer from an Uuid
 * @param {String} Uuid
 * @returns {Object} Offer data
 */

const findOnByUuid = async (uuid) =>{
	const offer = prisma.offer.findFirst({
		where: {
			uuid,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			title: true,
			diplomasName: true,
			description:true,
			isDiplomasRequired: true,
			minExpYears: true,
			minLanguageLevel: true,
			description: true,
			createdAt: true,
			status: true,
			canidate : {
				select : {
					candidate: true
				}
			},
			employer:{},

		},
	});
	return offer;
}
/**
 * Get a Offer from an ID
 * @param {Number} Id
 * @returns {Object} Offer data
 */
const findOfferById = async (id) => {
	const offer = prisma.offer.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			title: true,
			diplomasName: true,
			description:true,
			isDiplomasRequired: true,
			minExpYears: true,
			minLanguageLevel: true,
			description: true,
			createdAt: true,
			status: true,
			PreCandidate : {},
			employer:{},
		},
	});
	return offer;
};

/**
 * Get All Offers
 * @returns {Object}
 */
const findAllOffers = async (skip, take) => {
	const offers = await prisma.offer.findMany({
		skip:skip,
		take:take,
		where:
		{ deletedAt: null },
		select: {
			id: true,
			uuid : true,
			title: true,
			diplomasName: true,
			isDiplomasRequired: true,
			minExpYears: true,
			minLanguageLevel: true,
			createdAt: true,
			status: true,
			// canidate:{},
			employer:{},
			// TO FIX ME LATER
			// PreCandidate : {
			// 	select : {
			// 		firstName : true,
			// 		lastName : true
			// 	}
			// },
		},
	});
	const offerCount = 	await prisma.offer.count({
		where:
		{ deletedAt: null },
	})
	const allOffers = 	await prisma.offer.findMany({
		where:
		{ deletedAt: null },
		select: {
			id: true,
			uuid : true,
			title: true,
			diplomasName: true,
			isDiplomasRequired: true,
			minExpYears: true,
			minLanguageLevel: true,
			createdAt: true,
			status: true,
			employer:{},
			// PreCandidate : {
			// 	select : {
			// 		firstName : true,
			// 		lastName : true
			// 	}
			// },
		},
	})
	  return {
		offers,
		offerCount,
		allOffers,
	  };
};

/**
* Add a new Offer
* @param {Object}
* @returns {Object}
*/
const addOffer = async (data) => {
	const offer = await prisma.offer.create({
		data,
	});
	return offer;
};

/**
 * Update a Offer
 * @param {Object}
 * @returns {Number}
 */
const editOffer = async (id, data) => {
	const offer = await prisma.offer.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return offer;
};

/**
 * Delete a Offer (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteOffer = async (id) => {
	const offer = await prisma.offer.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return offer;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */
// const assignToCampaigns = async (data) => {
// 	const offer = await prisma.offer.create({
// 		data: {
// 			title: data.title,
// 			diplomasName: data.diplomasName,
// 			isDiplomasRequired: data.isDiplomasRequired,
// 			minExpYears: data.minExpYears,
// 			minLanguageLevel: data.minLanguageLevel,
// 			campaign: {
// 				create: [
// 					{
// 						campaign: {
// 							connect: {
// 								id: data.campaignId,
// 							},
// 						},
// 					},
// 				],
// 			},
// 		},
// 	});
// 	return offer;
// };

export {
	findOfferById,
	findAllOffers,
	addOffer,
	editOffer,
	deleteOffer,
	findOnByUuid,
};
