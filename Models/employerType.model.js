"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a EmployerType from an ID
 * @param {Number} Id
 * @returns {Object} EmployerType data
 */
const findEmployerTypeById = async (id) => {
	const employerType = prisma.employmentType.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return employerType;
};

/**
 * Get All EmployerTypes
 * @returns {Object}
 */
const findAllEmployerTypes = async (skip = null, take = null, filters = {}) => {
	const employerTypes = await prisma.employmentType.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			label: true,
			isActive: true,
		},
	});
	return employerTypes;
};

/**
 * Add a new EmployerType
 * @param {Object}
 * @returns {Object}
 */
const addEmployerType = async (data) => {
	const employerType = await prisma.employmentType.create({
		data,
	});
	return employerType;
};

/**
 * Update a EmployerType
 * @param {Object}
 * @returns {Number}
 */
const editEmployerType = async (id, data) => {
	const employerType = await prisma.employmentType.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return employerType;
};

/**
 * Delete a EmployerType (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteEmployerType = async (id) => {
	const employerType = await prisma.employmentType.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return employerType;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findEmployerTypeById,
	findAllEmployerTypes,
	addEmployerType,
	editEmployerType,
	deleteEmployerType,
};
