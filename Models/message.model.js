"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Message from an ID
 * @param {Number} Id
 * @returns {Object} Message data
 */
const findMessageById = async (id) => {
	const message = prisma.message.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			name: true,
			isActive: true,
		},
	});
	return message;
};

/**
 * Get All Messages
 * @returns {Object}
 */
const findAllMessages = async (skip = null, take = null, filters = {}) => {
	const messages = await prisma.message.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			name: true,
			isActive: true,
		},
	});
	return messages;
};

/**
 * Add a new Message
 * @param {Object}
 * @returns {Object}
 */
const addMessage = async (data) => {
	const message = await prisma.message.create({
		data,
	});
	return message;
};

/**
 * Delete a Message (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteMessage = async (id) => {
	const message = await prisma.message.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return message;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findMessageById,
	findAllMessages,
	addMessage,
	deleteMessage,
};
