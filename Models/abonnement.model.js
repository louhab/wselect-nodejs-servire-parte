"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Abonnement from an ID
 * @param {Number} Id
 * @returns {Object} Abonnement data
 */
const findAbonnementById = async (key, val) => {
	const abonnement = prisma.abonnements.findFirst({
		where: {
			[key]: val,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			title: true,
			pricePerMonth: true,
			currency: true,
		  
			countryAndCityId: true,
			countryAndCity: {
				select: {
					city: true,
					country: true,
				},
			},
			details: true,

			createdAt: true,
		},
	});
	return abonnement;
};

/**
 * Add a new AbonnementForm
 * @param {Object}
 * @returns {Object}
 */
const countAbonnement = async (id) => {
	const abonnementForm = await prisma.abonnements.count({
		where: {
			id,
		},
	});
	return Boolean(abonnementForm);
};

/**
 * Get All Abonnements
 * @returns {Object}
 */
const findAllAbonnements = async (skip,take) => {
	return {
		    allAbonnements: await prisma.abonnements.findMany({}),
			abonnements: await prisma.abonnements.findMany({
			skip:skip,
			take:take,
			where: 
			{deletedAt: null},
			select: {
				id: true,
				title: true,
				pricePerMonth: true,
				currency: true,
				details: true,
	
				createdAt: true,
			},
		}),

	}

};

/**
 * Add a new Abonnement
 * @param {Object}
 * @returns {Object}
 */
const addAbonnement = async (data) => {
	const abonnement = await prisma.abonnements.create({
		data,
	});
	return abonnement;
};

/**
 * Update a Abonnement
 * @param {Object}
 * @returns {Number}
 */
const editAbonnement = async (id, data) => {
	const abonnement = await prisma.abonnements.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data,
	});
	return abonnement;
};

/**
 * Delete a Abonnement (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteAbonnement = async (id) => {
	const abonnement = await prisma.abonnements.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return abonnement;
};

export {
	findAbonnementById,
	findAllAbonnements,
	addAbonnement,
	editAbonnement,
	deleteAbonnement,
	countAbonnement,
};
