"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Candidate from an ID
 * @param {Number} Id
 * @returns {Object} Candidate data
 */
const findCandidateById = async (key, val) => {
	const candidate = prisma.candidate.findFirst({
		where: {
			[key]: val,
			AND: {
				deletedAt: null,
			},
		},
		select: {
			id: true,
			phone: true,
			firstName: true,
			lastName: true,
			password: true,
			email: true,
			cvPath: true,
			status: true,
			gender: true,
			yearsExp: true,
			languageLevel: true,
			diplomasName: true,
			address: true,
			diplomeId: true,
			diplome: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			jobFieldId: true,
			jobField: {
				select: {
					id: true,
					label: true,
					isActive: true,
				},
			},
			offer: {
				select: {
					offer: {
						select: {
							id:true,
							title:true,
						}
					}
				}
			},
		},
	});
	return candidate;
};

/**
 * Get All Candidates
 * @returns {Object}
 */
const findAllCandidates = async (skip, take) => {
	const candidatesCount = await prisma.PreCandidate.count({where: 
		{deletedAt:null}
	}) 
	//+ await prisma.candidate.count() To check later 
	
	const allcondidates = {
		candidates   : 	await prisma.candidate.findMany({where: {deletedAt : null} }),
		precadidates : 	await prisma.PreCandidate.findMany({ where : 
			{deletedAt : null},
			select : {
				firstName:true,
				lastName:true,
				gender:true,
				phone:true,
				email:true,
				diplome : {},
				employer : {},
				offer : {},
				yearsExp :{},
				languageLevel : true,
			}
		})
	}  
	const candidates = 
	{   
		// candidates :
		candidates : await prisma.candidate.findMany({
			skip :skip,
			 take : take,
			 where:  { deletedAt: null },
			 select: {
				 id: true,
				 phone: true,
				 firstName: true,
				 lastName: true,
				 email: true,
				 cvPath: true,
				 status: true,
				 gender: true,
				 diplomeId: true,
				 status:true,
				 //createdAt:true,
				 address: true,
				 offer: {
					 select: {
						 offer: {
							 select: {
								 id:true,
								 title:true,
								 employer:true,
							 }
						 }
					 }
				 },
				 diplome: {
					 select: {
						 id: true,
						 name: true,
						 isActive: true,
					 },
				 },
				 jobFieldId: true,
				 jobField: {
					 select: {
						 id: true,
						 label: true,
						 isActive: true,
					 },
				 },
			 },}),
			 
		// precandidates :
	    precadidates: await prisma.PreCandidate.findMany({ 
			skip :skip,
			take : take,
			where:  { deletedAt: null },
			select : {
				firstName:true,
				lastName:true,
				gender:true,
				phone:true,
				email:true,
				diplome : {},
				employer : {},
				offer : {},
				yearsExp :{},
				languageLevel : true,
			}
		})
	}
	return {
		candidatesCount,
		allcondidates,
		candidates
	}
	

};

/**
 * Add a new Candidate
 * @param {Object}
 * @returns {Object}
 */
const addCandidate = async (data) => {
	const transaction = await prisma.$transaction(async () => {
		const candidate = await prisma.candidate.create({
			data,
		});
		return candidate;
	});

	return transaction;
};

/**
 * Add a new EmployerForm
 * @param {Object}
 * @returns {Object}
 */
const countCandidate = async (email) => {
	const employerForm = await prisma.candidate.count({
		where: {
			email,
		},
	});
	return Boolean(employerForm);
};

const countCandidatePhone = async (phone) => {
	const employerForm = await prisma.candidate.count({
		where: {
			phone,
		},
	});
	return Boolean(employerForm);
};

/**
 * Update a Candidate
 * @param {Object}
 * @returns {Number}
 */
const editCandidate = async (id, data) => {
	const candidate = await prisma.candidate.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return candidate;
};

/**
 * Delete a Candidate (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteCandidate = async (id) => {
	const candidate = await prisma.candidate.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return candidate;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */
const assignToOffers = async (data) => {
	const candidate = await prisma.candidate.create({
		data: {
			firstName: data.firstName,
			lastName: data.lastName,
			phone: data.phone,
			diplomasName: data.diplomasName,
			hasDiplomas: data.hasDiplomas,
			yearsExp: data.yearsExp,
			languageLevel: data.languageLevel,
			cvPath: data?.cvPath || null,
			email: data.email,
			status: data.status,
			gender: data.gender,
			diplomeId: data?.diplomeId,
			domaineDactiviteId: data.domaineDactiviteId,
			offer: {
				create: [
					{
						offer: {
							connect: {
								id: Number(data.offerId),
							},
						},
					},
				],
			},
			company:{}
		},
	});
	return candidate;
};

export {
	findCandidateById,
	findAllCandidates,
	addCandidate,
	editCandidate,
	deleteCandidate,
	assignToOffers,
	countCandidate,
	countCandidatePhone,
};
