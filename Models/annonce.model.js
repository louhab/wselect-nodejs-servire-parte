"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Annonce from an ID
 * @param {Number} Id
 * @returns {Object} Annonce data
 */
const findAnnonceById = async (id) => {
	const annonce = prisma.annonces.findMany({
		where: {
			employerId: id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			name: true,
			isActive: true,
			discription: true,

			carrerLevelId: true,
			carrerLevel: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			employmentTypeId: true,
			// employmentType: {
			// 	select: {
			// 		id: true,
			// 		name: true,
			// 		isActive: true,
			// 	},
			// },
			salaryRangeId: true,
			salaryRange: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},

			yearsExp: true,

			domaineDactiviteId: true,
			domaineDactivite: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			companieId: true,
			companie: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			employerId: true,
			employers: {
				select: {
					id: true,
					contactName: true,
					// isActive: true,
				},
			},
			countryId: true,
			Country: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			cityId: true,
			city: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			createdAt: true,

		},
	});
	return annonce;
};

/**
 * Get a Annonce from an ID
 * @param {Number} Id
 * @returns {Object} Annonce data
 */
 const findAnnonceViewById = async (id) => {
	const annonce = prisma.annonces.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		
		select: {
			id: true,
			name: true,
			isActive: true,
			discription: true,

			carrerLevelId: true,
			carrerLevel: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			employmentTypeId: true,
			// employmentType: {
			// 	select: {
			// 		id: true,
			// 		name: true,
			// 		isActive: true,
			// 	},
			// },
			salaryRangeId: true,
			salaryRange: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},

			yearsExp: true,

			domaineDactiviteId: true,
			domaineDactivite: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			companieId: true,
			companie: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			employerId: true,
			employers: {
				select: {
					id: true,
					contactName: true,
					// isActive: true,
				},
			},
			countryId: true,
			Country: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			cityId: true,
			city: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			createdAt: true,

		},
	});
	return annonce;
};

/**
 * Get All Annonces
 * @returns {Object}
 */
// skip = null, take = null, filters = {}
const findAllAnnonces = async (info) => {

	const filters = info?.filters ? JSON?.parse(info?.filters) : { deletedAt: null, }
	const annonces = await prisma.annonces.findMany({
		// skip: Number(info?.skip),
		// take: Number(info?.take),
		where: {
			deletedAt: null,
			AND: [filters]
		},
		select: {
			id: true,
			name: true,
			isActive: true,
			discription: true,

			carrerLevelId: true,
			carrerLevel: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			employmentTypeId: true,
			// employmentType: {
			// 	select: {
			// 		id: true,
			// 		name: true,
			// 		isActive: true,
			// 	},
			// },
			salaryRangeId: true,
			salaryRange: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},

			yearsExp: true,

			domaineDactiviteId: true,
			domaineDactivite: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			companieId: true,
			companie: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			employerId: true,
			employers: {
				select: {
					id: true,
					contactName: true,
					// isActive: true,
				},
			},
			countryId: true,
			Country: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			cityId: true,
			city: {
				select: {
					id: true,
					name: true,
					isActive: true,
				},
			},
			createdAt: true,

		},
	});
	return annonces;
};

/**
 * Add a new Annonce
 * @param {Object}
 * @returns {Object}
 */
const addAnnonce = async (data) => {
	const annonce = await prisma.annonces.create({
		data,
	});
	return annonce;
};

/**
 * Update a Annonce
 * @param {Object}
 * @returns {Number}
 */
const editAnnonce = async (id, data) => {
	const annonce = await prisma.annonces.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return annonce;
};

/**
 * Delete a Annonce (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteAnnonce = async (id) => {
	const annonce = await prisma.annonces.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return annonce;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findAnnonceById,
	findAnnonceViewById,
	findAllAnnonces,
	addAnnonce,
	editAnnonce,
	deleteAnnonce,
};
