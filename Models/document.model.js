"use strict";
import prisma from "../helpers/prisma.helper.js";





/**
 * Get a Document from an ID
 * @param {Number} Id
 * @returns {Object} Document data
 */
const findDocumentById = async (id) => {
	const document = prisma.Doc.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
	});
	return document;
};

/**
 * Get All Documents
 * @returns {Object}
 */
const findAllDocuments = async (skip = null, take = null, filters = {}) => {
	const documents = await prisma.Doc.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			name: true,
			path: true,
			employerId: true,
		}
	});
	return documents;
};
/**
 * Add a new Document
 * @param {Object}
 * @returns {Object}
 */
const addDocument = async (data) => {
    const document = await prisma.Doc.create({
        data,
    });
    return document;

};

/**
 * Update a Document
 * @param {Object}
 * @returns {Number}
 */
const editDocument = async (id, data) => {
	const document = await prisma.Doc.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return document;
};

/**
 * Delete a Document (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteDocument = async (id) => {
	const document = await prisma.Doc.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return document;
};

export {
	findDocumentById,
	findAllDocuments,
	editDocument,
	addDocument,
	deleteDocument,
};
