"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Ws101Form from an ID
 * @param {Number} Id
 * @returns {Object} Ws101Form data
 */
const findWs101FormById = async (candidateId) => {
	const record = prisma.ws101Form.findMany({
		where: {
			candidateId,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			candidate: {
				select: {
					id: true,
					firstName: true,
					lastName: true,
					email: true,
					phone: true,
				},
			},

			city: true,
			maritalStatus: true,
			birthDate: true,
			educationLevel: true,
			studyField: true,
			graduationYear: true,
			diplome: true,

			havingFrenchTest: true,
			frenchTestLabel: true,
			frenchTestScore: true,
			frenchTestYear: true,
			frenchLevel: true,

			havingEnglishTest: true,
			englishTestLabel: true,
			englishTestScore: true,
			englishTestYear: true,
			englishLevel: true,

			havingJob: true,
			jobField: true,
			employerName: true,
			experienceYears: true,
			havingCNSS: true,
			declaredDays: true,

			havingOtherJob: true,
			otherJobField: true,
			otherJobEmployerName: true,
			otherJobExperienceYears: true,
			havingCNSSOtherJob: true,
			otherJobDeclaredDays: true,

			createdAt: true,
			updatedAt: true,
			deletedAt: true,
		},
	});
	return record;
};

/**
 * Add a new Ws101Form
 * @param {Object}
 * @returns {Object}
 */
const addWs101Form = async (data) => {
	const record = await prisma.ws101Form.create({
		data,
	});
	return record;
};

/**
 * Update a Ws101Form
 * @param {Object}
 * @returns {Number}
 */
const editWs101Form = async (candidateId, data) => {
	const record = await prisma.ws101Form.updateMany({
		where: {
			candidateId,
			deletedAt: null,
		},
		data,
	});
	return record;
};

/**
 * Delete a Ws101Form (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteWs101Form = async (candidateId) => {
	const record = await prisma.ws101Form.updateMany({
		where: {
			candidateId,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return record;
};

export { findWs101FormById, addWs101Form, editWs101Form, deleteWs101Form };
