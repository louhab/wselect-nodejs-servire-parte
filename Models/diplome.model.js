"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a Diplome from an ID
 * @param {Number} Id
 * @returns {Object} Diplome data
 */
const findDiplomeById = async (id) => {
	const diplome = prisma.diplome.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},

		select: {
			id: true,
			name: true,
			isActive: true,
		},
	});
	return diplome;
};

/**
 * Get All Diplomes
 * @returns {Object}
 */
const findAllDiplomes = async (skip = null, take = null, filters = {}) => {
	const diplomes = await prisma.diplome.findMany({
		where: { deletedAt: null },
		select: {
			id: true,
			name: true,
			isActive: true,
		},
	});
	return diplomes;
};

/**
 * Add a new Diplome
 * @param {Object}
 * @returns {Object}
 */
const addDiplome = async (data) => {
	const diplome = await prisma.diplome.create({
		data,
	});
	return diplome;
};

/**
 * Update a Diplome
 * @param {Object}
 * @returns {Number}
 */
const editDiplome = async (id, data) => {
	const diplome = await prisma.diplome.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return diplome;
};

/**
 * Delete a Diplome (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteDiplome = async (id) => {
	const diplome = await prisma.diplome.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return diplome;
};

/**
 * Assign offer to campaign
 * @param {Object}
 * @returns {Number}
 */

export {
	findDiplomeById,
	findAllDiplomes,
	addDiplome,
	editDiplome,
	deleteDiplome,
};
