"use strict";
import prisma from "../helpers/prisma.helper.js";

/**
 * Get a log from an ID
 * @param {Number} Id
 * @returns {Object} log data
 */
const findLogByid = async (id) => {
	const log = prisma.Log.findFirst({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
	});
	return log;
};

/**
 * Get All logs
 * @returns {Object}
 */
const findAllLogs = async () => {
	const logs = await prisma.Log.findMany({
		where: { deletedAt: null },
	});
	return logs;
};
/**
 * Add a new Log
 * @param {Object}
 * @returns {Object}
 */
const addLog = async (data) => {
    const log = await prisma.Log.create({
        data,
    });
    return log;

};

/**
 * Update a Log
 * @param {Object}
 * @returns {Number}
 */
const editLog = async (id, data) => {
	const log = await prisma.Log.updateMany({
		where: {
			id,
			deletedAt: null,
		},
		data,
	});
	return log;
};

/**
 * Delete a Log (soft delete)
 * @param {Object}
 * @returns {Number}
 */
const deleteLog = async (id) => {
	const log = await prisma.Log.updateMany({
		where: {
			id,
			AND: {
				deletedAt: null,
			},
		},
		data: {
			deletedAt: new Date(),
		},
	});
	return log;
};

export {
	findLogByid,
	findAllLogs,
	editLog,
	addLog,
	deleteLog,
};
