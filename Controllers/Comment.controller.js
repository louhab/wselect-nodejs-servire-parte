"use strict";
import BaseController from "./Base.controller.js";

class CommentController extends BaseController {}

export default new CommentController();
