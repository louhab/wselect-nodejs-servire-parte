"use strict";
import BaseController from "./Base.controller.js";

class CandidateController extends BaseController {
	register = () => async (req, res, next) => {
		res.status(201).json({
			success: true,
			results: {
				user: req?.user,
				accessToken: req.accessToken,
			},
		});
	};

	login = () => async (req, res, next) => {
		res.status(200).json({
			success: true,
			results: {
				user: req?.user,
				accessToken: req?.accessToken,
			},
		});
	};

	token = () => async (req, res, next) => {
		res.status(200).json({
			success: true,
			results: {
				user: req?.payload,
				// accessToken: req?.accessToken,
			},
		});
	};
}

export default new CandidateController();
