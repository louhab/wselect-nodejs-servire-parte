"use strict";
import BaseController from "./Base.controller.js";

class EmployerTypeController extends BaseController {}

export default new EmployerTypeController();
