"use strict";
import BaseController from "./Base.controller.js";

class AuthController extends BaseController {
	token = () => async (req, res) => {
		res.status(200).json({
			success: true,
			results: req.data,
		});
	};
}

export default new AuthController();
