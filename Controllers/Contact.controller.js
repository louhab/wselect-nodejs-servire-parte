"use strict";

class ContactController {
	send = () => async (req, res) => {
		res.status(200).json({
			success: true,
		});
	};
}

export default new ContactController();
