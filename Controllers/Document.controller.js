"use strict";
import BaseController from "./Base.controller.js";

class DocumentController extends BaseController {}

export default new DocumentController();