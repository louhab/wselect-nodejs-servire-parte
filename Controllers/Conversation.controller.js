"use strict";
import BaseController from "./Base.controller.js";

class ConversationController extends BaseController {}

export default new ConversationController();
