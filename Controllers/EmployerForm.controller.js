"use strict";
import BaseController from "./Base.controller.js";

class EmployerFormController extends BaseController {}

export default new EmployerFormController();
