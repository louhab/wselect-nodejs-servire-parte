"use strict";
import BaseController from "./Base.controller.js";

class EmployerController extends BaseController {}

export default new EmployerController();
