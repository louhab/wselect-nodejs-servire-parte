"use strict";
import BaseController from "./Base.controller.js";

class PreCandidateController extends BaseController {}

export default new PreCandidateController();
