"use strict";
import BaseController from "./Base.controller.js";

class CandidateOfferController extends BaseController {}

export default new CandidateOfferController();
