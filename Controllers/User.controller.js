"use strict";
import BaseController from "./Base.controller.js";

class UserController extends BaseController {
	register = () => async (req, res, next) => {
		res.status(201).json({
			success: true,
			results: {
				user: req?.user,
				accessToken: req.accessToken,
			},
		});
	};

	login = () => async (req, res, next) => {
		res.status(200).json({
			success: true,
			results: {
				user: req?.user,
				accessToken: req?.accessToken,
			},	
		});
	};

	token = () => async (req, res, next) => {
		res.status(200).json({
			success: true,
			results: {
				user: {
					...req.payload,
					...req.data
				}
			},
		});
	};
}


export default new UserController();
