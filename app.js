"use strict";
import express from "express";
const app = express();
const router = express.Router();
import createError from "http-errors";
import morgan from "morgan";
import cors from "cors"
import bodyParser from "body-parser"
app.use(bodyParser.text({ type: '/' }));
import EmployerRoute from "./Routes/employer.route.js";
import ContactRoute from "./Routes/contact.route.js";
import OfferRoute from "./Routes/offer.route.js";
import CandidateRoute from "./Routes/candidate.route.js";
import CampaignRoute from "./Routes/campaign.route.js";
import EmployerFormRoute from "./Routes/employer-form.route.js";
import AuthRoute from "./Routes/auth.route.js";
import UserRoute from "./Routes/user.route.js";
import AbonnementRoute from "./Routes/abonnement.route.js";
import JobFieldRoute from "./Routes/job-field.js";
import DiplomeRoute from "./Routes/diplome.route.js";
import DocumentRoute from "./Routes/document.route.js"
import SalaryRangeRoute from "./Routes/salaryRange.route.js";
import EmployerTypeRoute from "./Routes/employerType.route.js";
import CarrerLevelRoute from "./Routes/carrerLevel.route.js";
import CityRoute from "./Routes/city.route.js";
import CountryRoute from "./Routes/country.route.js";
import AnnoneceRoute from "./Routes/annouce.route.js";
import CompanyRoute from "./Routes/company.route.js";
import ConversationRoute from "./Routes/conversation.route.js";
import CandidateOfferRoute from "./Routes/candidateOffer.route.js";
import CommentsRoute from "./Routes/comment.route.js";
import Ws101FormRoute from "./Routes/ws101.route.js";
import UploadedFile from "express-fileupload";
import LogRoute from "./Routes/logs.route.js";
import PreCandidateRoute  from './Routes/precandidate.route.js'
import employerContractRoute from "./Routes/employerContract.route.js";
import { verifyAccessToken,CheckIfTheUserIsExist } from "./helpers/jwt.helper.js";


app.use(express.json());

app.use(
	express.urlencoded({
		extended: true,
	})
	
);
const corsOptions = { 
  AccessControlAllowOrigin: '*',  
  origin: '*',  
  methods: 'GET,HEAD,PUT,PATCH,POST,DELETE' 
}
app.use(cors(corsOptions))

app.use("/static", express.static("./public"));
app.use(UploadedFile());

if (app.get("env") === "development") {
	app.use(morgan("dev"));
	console.log("Morgan is enabled ..!");
}

router.get("/", async (req, res) => {
	res.send("Hello Globers :)");
});

app.use("/api/v1/precandidates", PreCandidateRoute);
app.use("/api/v1/auth", AuthRoute);
app.use("/api/v1/contact", ContactRoute);
app.use("/api/v1/employer-contrat", employerContractRoute);
app.use("/api/v1/users", UserRoute);
app.use("/api/v1/employers",EmployerRoute);
app.use("/api/v1/candidates",CheckIfTheUserIsExist ,CandidateRoute);



app.use(verifyAccessToken);
app.use(CheckIfTheUserIsExist);
app.use("/api/v1",router);
app.use("/api/v1/offers", OfferRoute);
app.use("/api/v1/campaigns", CampaignRoute);
app.use("/api/v1/employer-forms", EmployerFormRoute);
app.use("/api/v1/abonnements", AbonnementRoute);
app.use("/api/v1/job-fields", JobFieldRoute);
app.use("/api/v1/diplomes", DiplomeRoute);
app.use("/api/v1/documents",DocumentRoute)
app.use("/api/v1/logs",LogRoute)


app.use("/api/v1/salary-ranges", SalaryRangeRoute);
app.use("/api/v1/employer-types", EmployerTypeRoute);
app.use("/api/v1/carrer-levels", CarrerLevelRoute);
app.use("/api/v1/cites", CityRoute);
app.use("/api/v1/countries", CountryRoute);
app.use("/api/v1/annonces", AnnoneceRoute);
app.use("/api/v1/companies", CompanyRoute);
app.use("/api/v1/conversations", ConversationRoute);
app.use("/api/v1/candidate-apply", CandidateOfferRoute);
app.use("/api/v1/comments", CommentsRoute);
app.use("/api/v1/ws101form", Ws101FormRoute);


app.use(async (req, res, next) => {
	next(createError.NotFound("Route not found!"));
});

app.use(async (err, req, res, next) => {
	console.log(err);
	res.status(err.status || 500);
	res.send({
		success: false,
		error: {
			status: err.status || 500,
			message: err.message,
		},
	});
});




export default app;

