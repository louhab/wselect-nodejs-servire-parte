"use strict";
import JWT from "jsonwebtoken";
import creatError from "http-errors";
import prisma from "../helpers/prisma.helper.js";
import {findUserById} from '../Models/user.model.js';
const signAccessToken = (userId, userType) =>
	new Promise((resolve, reject) => {
		const id = String(userId);
		const payload = {
			id: userId,
			type: userType,
		};
		const secret = process.env.ACCESS_TOKEN_SECRET_KEY;
		const options = {
			issuer: "Bommers",
			audience: id,
		};
		JWT.sign(payload, secret, options, (err, token) => {
			if (err) {
				console.log(err.message);
				return reject(creatError.InternalServerError());
			}
			resolve(token);
		});
	});

const verifyAccessToken = async (req, res, next) => {
	if (!req.headers["authorization"]) return next(creatError.Unauthorized());
	const [Bearer, token] = req.headers["authorization"].split(" ");
	JWT.verify(token, process.env.ACCESS_TOKEN_SECRET_KEY, (err, payload) => {
		if (err) {
			const message =
				err.name === "JsonWebTokenError" ? null : err.message;
			return next(creatError.Forbidden(message));
		}
		req.payload = payload;
		next();
	});
};

const CheckIfTheUserIsExist = async (req, res,next) => {
	if (!req.headers["authorization"]) return next(creatError.Unauthorized());
	var token = req.headers["authorization"].split(" ")[1]
	let userId = '';
	JWT.verify(token, process.env.ACCESS_TOKEN_SECRET_KEY, (err, payload) => {
	req.payload = payload;
	userId = req.payload.id;
})
	const user = await findUserById(userId);
	if (!user) {
			return res.status(401).json({ message: 'Not authorized' });
	}
	req.user = user;
	next();			
}

const checkIfUserHasRole = async (req, res,next) => {
	const userOfRequest = await findUserById(Number(req.params.id));
	let userId = '';
	if (!req.headers["authorization"]) return next(creatError.Unauthorized());
	var token = req.headers["authorization"].split(" ")[1]
	JWT.verify(token, process.env.ACCESS_TOKEN_SECRET_KEY, (err, payload) => {
		req.payload = payload;
		userId = req.payload.id;
	})
	const userOfToken = await findUserById(userId);
	if (userOfRequest.email != userOfToken.email && userOfToken.role != 'Admin' ) {
	return res.status(401).json({ message: 'Not authorized' });
	}
	req.user = userOfToken
	next();
			
}
export { signAccessToken, verifyAccessToken,CheckIfTheUserIsExist,checkIfUserHasRole};
