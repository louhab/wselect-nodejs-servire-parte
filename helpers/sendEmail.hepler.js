"use strict";
import nodemailer from "nodemailer";

/**
 * EMAIL_HOST="smtp.googlemail.com"
 * EMAIL_PORT=465
 * EMAIL_USER="boom.for.dev@gmail.com"
 * EMAIL_PASSWORD="lcubgjlcbkmunfqt"
 * EMAIL_FROM_ADDRESS="boom.for.dev@gmail.com"
 */

export const transporter = nodemailer.createTransport({
	service: "gmail",
	host: "smtp.googlemail.com",
	port: 465,
	auth: {
		user: "boom.for.dev@gmail.com",
		pass: "lcubgjlcbkmunfqt",
	},
});

function sendEmail(mailSettings, mailTransporter) {
	return new Promise((res, rej) => {
		mailTransporter.sendMail(mailSettings, (err, info) => {
			if (err) rej(err);
			res(info);
		});
	});
}

const sendEmailValidation = async (email, name, password) => {
	const mailOptions = {
		from: "noreply@wselect.ca",
		to: email,
		subject: "WORLD SELECT",
		html: `<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300&display=swap" rel="stylesheet">
		<div style="font-size: 16px; font-family: 'Inter', sans-serif;">
		<h3>Félicitations, ${name}</h3>
				<div>
				Vous êtes maintenant membre de WSELECT.
					<br />
				Vous pouvez désormais publier vos offres et consulter les profils autant que vous le souhaitez.<br />
				Vous pouvez maintenant vous connecter à votre compte sur WSELECT en utilisant votre adresse e-mail et le mot de passe ci-dessous. Vous pouvez bien entendu modifier le mot de passe à votre guise.
					<br />
				Votre mot de passe est: <i><strong>${password}</strong></i>
					<br />
				Si vous avez encore des questions, vous pouvez nous joindre à tout moment via info@wselect.com
					<br />
				Votre équipe de World select.
				</div>
		</div>
		`,
	};
	try {
		await sendEmail(mailOptions, transporter);
	} catch (err) {
		console.log(err);
	}
};

const sendFormURL = async (email, uuid) => {
	const mailOptions = {
		from: "noreply@wselect.ca",
		to: email,
		subject: "WSELECT",
		html: `<link rel="preconnect" href="https://fonts.googleapis.com">
		<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
		<link href="https://fonts.googleapis.com/css2?family=Inter:wght@200;300&display=swap" rel="stylesheet">
		<div style="font-size: 16px; font-family: 'Inter', sans-serif;">
		  <div> Merci d’avoir rejoint WORLD SELECT ! <br /> Vous pouvez désormais procéder au remplissage du formulaire à tout moment en vous connectant à travers le lien qui suit. 
		  <br /> Afin d’assurer la sauvegarde des informations déjà renseignées par vos soins, et sans pour autant remplir le formulaire dès le début, il est judicieux de toujours se connecter à travers ce lien : 
		  <br />
		  https://WSELECT.ca/employer-form/${uuid}
		  </div>
		</div>
		`,
	};
	try {
		await sendEmail(mailOptions, transporter);
	} catch (err) {
		console.log(err);
	}
};

const sendContactForm = async (email, name, message) => {
	const mailOptions = {
		from: email,
		to: "boom.crm2@gmail.com", // change it after
		subject: `From (contact form): ${name}`,
		html: message,
	};

	try {
		await sendEmail(mailOptions, transporter);
	} catch (err) {
		console.log(err);
	}
};

export { sendEmailValidation, sendContactForm, sendFormURL };
