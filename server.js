"use strict";

import http from "http";
import https from "https";
import app from "./app.js";
import dotenv from "dotenv";

dotenv.config();

const PORT = 1337;
const HTPPS_PORT = 4241;

// const options = {
// 	key: fs.readFileSync(process.env.SSL_KEY_PATH, "utf8"),
// 	cert: fs.readFileSync(process.env.SSL_CERT_PATH, "utf8"),
// };

// HTTP server
http.createServer(app).listen(PORT, () => {
	console.log(`HTTP Server is runing on port ${PORT}`);
});

// HTTPS server
https.createServer({}, app).listen(HTPPS_PORT, () => {
	console.log(`HTTPS Server is runing on port ${HTPPS_PORT}`);
});
