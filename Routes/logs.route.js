import express from "express";
const router = express.Router();
import LogController from "../Controllers/Log.controller.js";

 import {
    createLogMiddleware,
	patchLogMiddleware,
	findLogByIdMiddleware,
	findAllLogsMiddleware,
	deleteLogMiddleware,
}
from '../Middlewares/Log.middleware.js'

/**
 * Get All
 */
router.get("/", findAllLogsMiddleware, LogController.findAll());
/**
 * Get `log` by Id
 */
router.get("/:id", findLogByIdMiddleware, LogController.findOne());

/**
 * Create a `Log`
 */
router.post(
	"/",
	createLogMiddleware,
	LogController.create()
);


/**
 * Update a `Log`
 */
router.put("/:id", patchLogMiddleware, LogController.patch());

/**
 * Delete a `Log` from the
 */
router.delete("/:id", deleteLogMiddleware, LogController.delete());

export default router;