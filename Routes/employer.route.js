import express from "express";
const router = express.Router();

import EmployerController from "../Controllers/Employer.controller.js";
import {
	createEmployerMiddleware,
	deleteEmployerMiddleware,
	findAllEmployersMiddleware,
	findEmployerByIdMiddleware,
	patchEmployerMiddleware,
	signInEmployerMiddleware,
	patchEmployerPasswordMiddleware,
	patchEmployerContractMiddleware,
	findEmployerByUuidMiddleware,
} from "../Middlewares/employer.middleware.js";
import { CheckIfTheUserIsExist } from "../helpers/jwt.helper.js";

import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/",findAllEmployersMiddleware, EmployerController.findAll());

/**
 * Get `Employer` by Id
 */
router.get("/:id",CheckIfTheUserIsExist ,findEmployerByIdMiddleware, EmployerController.findOne());
/**
 * Get `Employer` by Uuid
 */
router.get("/uuid/:uuid",CheckIfTheUserIsExist ,findEmployerByUuidMiddleware, EmployerController.findOne());

/**
 * Create a `Employer`
 */
router.post("/", createEmployerMiddleware, EmployerController.create());

/**
 * Create a `Candidate`
 */
router.post("/sign-in", signInEmployerMiddleware, EmployerController.create());

/**
 * Update a `Employer`
 */
router.put(
	"/contract/:id",
	formMiddleWare,
	patchEmployerContractMiddleware,
	EmployerController.patch()
);

/**
 * Update a `Employer`
 */
router.put("/:id", patchEmployerMiddleware, EmployerController.patch());

/**
 * Delete a `Employer`
 */
router.delete("/:id", deleteEmployerMiddleware, EmployerController.delete());

/**
 * Update a `Candidate password`
 */
router.put(
	"/updatePassword/:id",
	patchEmployerPasswordMiddleware,
	EmployerController.patch()
);

export default router;
