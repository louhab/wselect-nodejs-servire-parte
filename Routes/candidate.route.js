import express from "express";
const router = express.Router();

import CandidateController from "../Controllers/Candidate.controller.js";
import {
	createCandidateMiddleware,
	deleteCandidateMiddleware,
	findAllCandidatesMiddleware,
	findCandidateByIdMiddleware,
	patchCandidateMiddleware,
	signUpCandidateMiddleware,
	signInCandidateMiddleware,
	patchCandidatePasswordMiddleware,
	patchCandidateCvMiddleware,
} from "../Middlewares/candidate.middleware.js";

import {
	loginMiddleware,
	registerMiddleware,
} from "../Middlewares/auth.middleware.js";

import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/", findAllCandidatesMiddleware, CandidateController.findAll());

/**
 * Get `Candidate` by Id
 */
router.get("/:id", findCandidateByIdMiddleware, CandidateController.findOne());

/**
 * Create a `Candidate`
 */
router.post(
	"/",
	formMiddleWare,
	createCandidateMiddleware,
	CandidateController.create()
);

/**
 * Create a `Candidate`
 */
router.post(
	"/sign-up",
	signUpCandidateMiddleware,
	CandidateController.register()
);

/**
 * Create a `Candidate`
 */
router.post(
	"/sign-in",
	signInCandidateMiddleware,
	CandidateController.create()
);

/**
 * Update a `Candidate`
 */
router.put("/:id", patchCandidateMiddleware, CandidateController.patch());

/**
 * Update a `Candidate password`
 */
router.put(
	"/updatePassword/:id",
	patchCandidatePasswordMiddleware,
	CandidateController.patch()
);

router.put(
	"/updateCv/:id",
	formMiddleWare,
	patchCandidateCvMiddleware,
	CandidateController.patch()
);
/**
 * Delete a `Candidate`
 */
router.delete("/:id", deleteCandidateMiddleware, CandidateController.delete());

export default router;
