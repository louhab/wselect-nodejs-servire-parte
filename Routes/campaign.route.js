import express from "express";
const router = express.Router();

import CampaignController from "../Controllers/Campaign.controller.js";
import {
	createCampaignMiddleware,
	deleteCampaignMiddleware,
	findAllCampaignsMiddleware,
	findCampaignByIdMiddleware,
	patchCampaignMiddleware,
	findCampaignByUidMiddleware,
} from "../Middlewares/campaign.middleware.js";

/**
 * Get All
 */
router.get("/", findAllCampaignsMiddleware, CampaignController.findAll());

/**
 * Get `Campaign` by Id
 */
router.get("/:id", findCampaignByIdMiddleware, CampaignController.findOne());

/**
 * Get `BC` by Id
 */
router.get(
	"/uid/:uid",
	findCampaignByUidMiddleware,
	CampaignController.findOne()
);

/**
 * Create a `Campaign`
 */
router.post("/", createCampaignMiddleware, CampaignController.create());

/**
 * Update a `Campaign`
 */
router.put("/:id", patchCampaignMiddleware, CampaignController.patch());

/**
 * Delete a `Campaign`
 */
router.delete("/:id", deleteCampaignMiddleware, CampaignController.delete());

export default router;
