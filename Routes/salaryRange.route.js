import express from "express";
const router = express.Router();

import SalaryRangeController from "../Controllers/SalaryRange.controller.js";
import {
	createSalaryRangeMiddleware,
	deleteSalaryRangeMiddleware,
	findAllSalaryRangesMiddleware,
	findSalaryRangeByIdMiddleware,
	patchSalaryRangeMiddleware,
} from "../Middlewares/salaryRange.middleware.js";

/**
 * Get All
 */
router.get("/", findAllSalaryRangesMiddleware, SalaryRangeController.findAll());

/**
 * Get `SalaryRange` by Id
 */
router.get("/:id", findSalaryRangeByIdMiddleware, SalaryRangeController.findOne());

/**
 * Create a `SalaryRange`
 */
router.post(
	"/",
	createSalaryRangeMiddleware,
	SalaryRangeController.create()
);

/**
 * Create a `SalaryRange`
 */

/**
 * Update a `SalaryRange`
 */
router.put("/:id", patchSalaryRangeMiddleware, SalaryRangeController.patch());

/**
 * Delete a `SalaryRange`
 */
router.delete("/:id", deleteSalaryRangeMiddleware, SalaryRangeController.delete());

export default router;
