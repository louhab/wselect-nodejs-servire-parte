import express from "express";
const router = express.Router();

import AuthController from "../Controllers/Auth.controller.js";
import { validateTokenMiddleware } from "../Middlewares/auth.middleware.js";
import { verifyAccessToken } from "../helpers/jwt.helper.js";

/**
 *
 */
router.get(
	"/token",
	verifyAccessToken,
	validateTokenMiddleware,
	AuthController.token()
);

export default router;
