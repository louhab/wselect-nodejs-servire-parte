import express from "express";
const employerContractRoute = express.Router();
import EmployerContractController from "../Controllers/EmployerContract.controller.js";
import {
	createEmployerContractMiddleware,
} from "../Middlewares/employer.middleware.js";

/**
 * Create a `Employer Contract`
 */
 employerContractRoute.post("/", createEmployerContractMiddleware ,EmployerContractController.create());

export default employerContractRoute;

