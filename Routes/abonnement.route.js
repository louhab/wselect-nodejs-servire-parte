import express from "express";
const router = express.Router();

import AbonnementController from "../Controllers/Abonnement.controller.js";
import {
	createAbonnementMiddleware,
	deleteAbonnementMiddleware,
	findAllAbonnementsMiddleware,
	findAbonnementByIdMiddleware,
	patchAbonnementMiddleware,
} from "../Middlewares/abonnement.middleware.js";

/**
 * Get All
 */
router.get("/", findAllAbonnementsMiddleware, AbonnementController.findAll());

/**
 * Get `Abonnement` by Id
 */
router.get("/:id", findAbonnementByIdMiddleware, AbonnementController.findOne());

/**
 * Create a `Abonnement`
 */
router.post("/", createAbonnementMiddleware, AbonnementController.create());


/**
 * Update a `Abonnement`
 */
router.put("/:id", patchAbonnementMiddleware, AbonnementController.patch());

/**
 * Delete a `Abonnement`
 */
router.delete("/:id", deleteAbonnementMiddleware, AbonnementController.delete());

export default router;
