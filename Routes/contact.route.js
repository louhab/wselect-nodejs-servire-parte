import express from "express";
const router = express.Router();

import ContactController from "../Controllers/Contact.controller.js";
import { sendContactFormMiddleware } from "../Middlewares/contact.middleware.js";

/**
 * Send a message from contact form
 */
router.post("/", sendContactFormMiddleware, ContactController.send());

export default router;
