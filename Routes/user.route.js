import express from "express";
const router = express.Router();

import UserController from "../Controllers/User.controller.js";
import {
	createUserMiddleware,
	deleteUserMiddleware,
	findAllUsersMiddleware,
	findUserByIdMiddleware,
	patchUserMiddleware,
	validateTokenMiddleware
} from "../Middlewares/user.middleware.js";

import {
	loginMiddleware,
	registerMiddleware,
} from "../Middlewares/auth.middleware.js";
import {checkIfUserHasRole} from "../helpers/jwt.helper.js"
import { verifyAccessToken } from "../helpers/jwt.helper.js";

/**
 * Get All `User`
 */
router.get("/", findAllUsersMiddleware, UserController.findAll());

/**
 * Create a `User`
 */
router.post("/", createUserMiddleware, UserController.create());


/**
 * Update a `User`
 */
router.put("/:id",patchUserMiddleware ,UserController.patch());

/**
 * Delete a `User`
 */
router.delete("/:id", deleteUserMiddleware, UserController.delete());
/**
 * Register a `User`
 */
router.post("/register", registerMiddleware, UserController.register());
/**
 * Login a `User`
 */
router.post("/login", loginMiddleware, UserController.login());
/**
 * Token of the `User`
 */
router.get("/token",verifyAccessToken ,validateTokenMiddleware,UserController.token());

/**
 * Get `User` by Id
 */
router.get("/:id", checkIfUserHasRole,findUserByIdMiddleware, UserController.findOne());

export default router;
