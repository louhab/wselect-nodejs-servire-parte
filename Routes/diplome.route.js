import express from "express";
const router = express.Router();

import DiplomeController from "../Controllers/Diplome.controller.js";
import {
	createDiplomeMiddleware,
	deleteDiplomeMiddleware,
	findAllDiplomesMiddleware,
	findDiplomeByIdMiddleware,
	patchDiplomeMiddleware,
} from "../Middlewares/diplome.middleware.js";

/**
 * Get All
 */
router.get("/", findAllDiplomesMiddleware, DiplomeController.findAll());

/**
 * Get `Diplome` by Id
 */
router.get("/:id", findDiplomeByIdMiddleware, DiplomeController.findOne());

/**
 * Create a `Diplome`
 */
router.post(
	"/",
	createDiplomeMiddleware,
	DiplomeController.create()
);

/**
 * Create a `Diplome`
 */

/**
 * Update a `Diplome`
 */
router.put("/:id", patchDiplomeMiddleware, DiplomeController.patch());

/**
 * Delete a `Diplome`
 */
router.delete("/:id", deleteDiplomeMiddleware, DiplomeController.delete());

export default router;
