import express from "express";
const router = express.Router();

import CarrerLevelController from "../Controllers/CarrerLevel.controller.js";
import {
	createCarrerLevelMiddleware,
	deleteCarrerLevelMiddleware,
	findAllCarrerLevelsMiddleware,
	findCarrerLevelByIdMiddleware,
	patchCarrerLevelMiddleware,
} from "../Middlewares/carrerLevel.middleware.js";

/**
 * Get All
 */
router.get("/", findAllCarrerLevelsMiddleware, CarrerLevelController.findAll());

/**
 * Get `CarrerLevel` by Id
 */
router.get("/:id", findCarrerLevelByIdMiddleware, CarrerLevelController.findOne());

/**
 * Create a `CarrerLevel`
 */
router.post(
	"/",
	createCarrerLevelMiddleware,
	CarrerLevelController.create()
);

/**
 * Create a `CarrerLevel`
 */

/**
 * Update a `CarrerLevel`
 */
router.put("/:id", patchCarrerLevelMiddleware, CarrerLevelController.patch());

/**
 * Delete a `CarrerLevel`
 */
router.delete("/:id", deleteCarrerLevelMiddleware, CarrerLevelController.delete());

export default router;
