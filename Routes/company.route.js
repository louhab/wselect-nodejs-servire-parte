import express from "express";
const router = express.Router();

import CompanyController from "../Controllers/Company.controller.js";
import {
	createCompanyMiddleware,
	deleteCompanyMiddleware,
	findAllCompanysMiddleware,
	findCompanyByIdMiddleware,
	patchCompanyMiddleware,
} from "../Middlewares/company.middleware.js";
import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/", findAllCompanysMiddleware, CompanyController.findAll());

/**
 * Get `Company` by Id
 */
router.get("/:id", findCompanyByIdMiddleware, CompanyController.findOne());

/**
 * Create a `Company`
 */
router.post(
	"/",
	createCompanyMiddleware,
	CompanyController.create()
);

/**
 * Create a `Company`
 */

/**
 * Update a `Company`
 */
router.put("/:id", patchCompanyMiddleware, CompanyController.patch());

/**
 * Delete a `Company`
 */
router.delete("/:id", deleteCompanyMiddleware, CompanyController.delete());

export default router;
