import express from "express";
const router = express.Router();

import Ws101Controller from "../Controllers/Ws101.controller.js";
import {
	createWs101FormMiddleware,
	findWs101FormByIdMiddleware,
	patchWs101FormMiddleware,
	deleteWs101FormMiddleware,
} from "../Middlewares/ws101.middleware.js";

/**
 * Get `Ws101Form` by candidate_id
 */
router.get(
	"/:candidateId",
	findWs101FormByIdMiddleware,
	Ws101Controller.findOne()
);

/**
 * Create a `Annonce`
 */
router.post("/", createWs101FormMiddleware, Ws101Controller.create());

/**
 * Update a `Annonce`
 */
router.put("/:candidateId", patchWs101FormMiddleware, Ws101Controller.patch());

/**
 * Delete a `Annonce`
 */
router.delete(
	"/:candidateId",
	deleteWs101FormMiddleware,
	Ws101Controller.delete()
);

export default router;
