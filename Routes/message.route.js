import express from "express";
const router = express.Router();

import MessageController from "../Controllers/Message.controller.js";
import {
	createMessageMiddleware,
	deleteMessageMiddleware,
	findAllMessagesMiddleware,
	findMessageByIdMiddleware,
} from "../Middlewares/message.middleware.js";

/**
 * Get All
 */
router.get("/", findAllMessagesMiddleware, MessageController.findAll());

/**
 * Get `Message` by Id
 */
router.get("/:id", findMessageByIdMiddleware, MessageController.findOne());

/**
 * Create a `Message`
 */
router.post(
	"/",
	createMessageMiddleware,
	MessageController.create()
);

/**
 * Create a `Message`
 */

/**
 * Delete a `Message`
 */
router.delete("/:id", deleteMessageMiddleware, MessageController.delete());

export default router;
