import express from "express";
const router = express.Router();

import AnnonceController from "../Controllers/Annonce.controller.js";
import {
	createAnnonceMiddleware,
	deleteAnnonceMiddleware,
	findAllAnnoncesMiddleware,
	findAnnonceByIdMiddleware,
	patchAnnonceMiddleware,
	findAnnonceViewByIdMiddleware,
} from "../Middlewares/annonce.middleware.js";

/**
 * Get All
 */
router.get("/", findAllAnnoncesMiddleware, AnnonceController.findAll());

/**
 * Get `Annonce` by Id
 */
router.get("/:id", findAnnonceByIdMiddleware, AnnonceController.findAll());

router.get("/view/:id", findAnnonceViewByIdMiddleware, AnnonceController.findAll());

/**
 * Create a `Annonce`
 */
router.post(
	"/",
	createAnnonceMiddleware,
	AnnonceController.create()
);

/**
 * Create a `Annonce`
 */

/**
 * Update a `Annonce`
 */
router.put("/:id", patchAnnonceMiddleware, AnnonceController.patch());

/**
 * Delete a `Annonce`
 */
router.delete("/:id", deleteAnnonceMiddleware, AnnonceController.delete());

export default router;
