import express from "express";
const router = express.Router();

import EmployerTypeController from "../Controllers/EmployerType.controller.js";
import {
	createEmployerTypeMiddleware,
	deleteEmployerTypeMiddleware,
	findAllEmployerTypesMiddleware,
	findEmployerTypeByIdMiddleware,
	patchEmployerTypeMiddleware,
} from "../Middlewares/employerType.middleware.js";
import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/", findAllEmployerTypesMiddleware, EmployerTypeController.findAll());

/**
 * Get `EmployerType` by Id
 */
router.get("/:id", findEmployerTypeByIdMiddleware, EmployerTypeController.findOne());

/**
 * Create a `EmployerType`
 */
router.post(
	"/",
	createEmployerTypeMiddleware,
	EmployerTypeController.create()
);

/**
 * Create a `EmployerType`
 */

/**
 * Update a `EmployerType`
 */
router.put("/:id", patchEmployerTypeMiddleware, EmployerTypeController.patch());

/**
 * Delete a `EmployerType`
 */
router.delete("/:id", deleteEmployerTypeMiddleware, EmployerTypeController.delete());

export default router;
