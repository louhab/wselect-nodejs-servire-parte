import express from "express";
const router = express.Router();
import DocumentController from "../Controllers/Document.controller.js";

import {
    createDocumentMiddleware,
	patchDocumentMiddleware,
	findDocumentByIdMiddleware,
	findAllDocumentsMiddleware,
	deleteDocumentMiddleware,
}
from '../Middlewares/document.middleware.js'

/**
 * Get All
 */
router.get("/", findAllDocumentsMiddleware, DocumentController.findAll());
/**
 * Get `Doc` by Id
 */
router.get("/:id", findDocumentByIdMiddleware, DocumentController.findOne());

/**
 * Create a `Document`
 */
router.post(
	"/",
	createDocumentMiddleware,
	DocumentController.create()
);


/**
 * Update a `Doc`
 */
router.put("/:id", patchDocumentMiddleware, DocumentController.patch());

/**
 * Delete a `Doc` from the
 */
router.delete("/:id", deleteDocumentMiddleware, DocumentController.delete());

export default router;