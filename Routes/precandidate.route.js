import express from "express";
const router = express.Router();

import PreCandidateController from "../Controllers/precandidate.controller.js";
import {
    findAllPreCandidates,
    patchPreCandidateMiddleware,
    creatPreCandidateMiddleware,
} from "../Middlewares/precandidate.middleware.js";

/**
 * Get All pre candidates :
 */
router.get("/", findAllPreCandidates, PreCandidateController.findAll());


/**
 * Create a `Pre Candidate`
 */
router.post(
	"/",
	creatPreCandidateMiddleware,
	PreCandidateController.create()
);


/**
 * Update a `pre candidate`
 */
router.put("/:id", patchPreCandidateMiddleware, PreCandidateController.patch());


export default router;
