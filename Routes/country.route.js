import express from "express";
const router = express.Router();

import CountryController from "../Controllers/Country.controller.js";
import {
	createCountryMiddleware,
	deleteCountryMiddleware,
	findAllCountrysMiddleware,
	findCountryByIdMiddleware,
	patchCountryMiddleware,
} from "../Middlewares/country.middleware.js";
import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/", findAllCountrysMiddleware, CountryController.findAll());

/**
 * Get `Country` by Id
 */
router.get("/:id", findCountryByIdMiddleware, CountryController.findOne());

/**
 * Create a `Country`
 */
router.post(
	"/",
	createCountryMiddleware,
	CountryController.create()
);

/**
 * Create a `Country`
 */

/**
 * Update a `Country`
 */
router.put("/:id", patchCountryMiddleware, CountryController.patch());

/**
 * Delete a `Country`
 */
router.delete("/:id", deleteCountryMiddleware, CountryController.delete());

export default router;
