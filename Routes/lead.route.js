import express from "express";
const router = express.Router();

import LeadController from "../Controllers/Lead.controller.js";
import {
  createLeadMiddleware,
  deleteLeadMiddleware,
  findAllLeadsMiddleware,
  findLeadByIdMiddleware,
  patchLeadMiddleware,
} from "../Middlewares/lead.middleware.js";

/**
 * Get All
 */
router.get("/", findAllLeadsMiddleware, LeadController.findAll());

/**
 * Get `Lead` by Id
 */
router.get("/:id", findLeadByIdMiddleware, LeadController.findOne());

/**
 * Create a `Lead`
 */
router.post("/", createLeadMiddleware, LeadController.create());

/**
 * Update a `Lead`
 */
router.put("/:id", patchLeadMiddleware, LeadController.patch());

/**
 * Delete a `Lead`
 */
router.delete("/:id", deleteLeadMiddleware, LeadController.delete());

export default router;
