import express from "express";
const router = express.Router();

import StatusController from "../Controllers/Status.controller.js";
import {
	createStatusMiddleware,
	deleteStatusMiddleware,
	findAllStatusMiddleware,
	findStatusByIdMiddleware,
	patchStatusMiddleware,
} from "../Middlewares/status.middleware.js";

/**
 * Get All `Status`
 */
router.get("/", findAllStatusMiddleware, StatusController.findAll());

/**
 * Get `Status` by Id
 */
router.get("/:id", findStatusByIdMiddleware, StatusController.findOne());

/**
 * Create a `Status`
 */
router.post("/", createStatusMiddleware, StatusController.create());

/**
 * Update a `Status`
 */
router.put("/:id", patchStatusMiddleware, StatusController.patch());

/**
 * Delete a `Status`
 */
router.delete("/:id", deleteStatusMiddleware, StatusController.delete());

module.exports = router;
