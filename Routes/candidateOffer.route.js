import express from "express";
const router = express.Router();

import CandidateOfferController from "../Controllers/CandidateOffer.controller.js";
import {
	createCandidateOfferMiddleware,
	deleteCandidateOfferMiddleware,
	findAllCandidateOffersMiddleware,
	findCandidateOfferByIdMiddleware,
	patchCandidateOfferMiddleware,
} from "../Middlewares/candidateOffer.middleware.js";

/**
 * Get All
 */
router.get("/", findAllCandidateOffersMiddleware, CandidateOfferController.findAll());

/**
 * Get All offer alppayer
 */
 router.get("/:type/:id", findAllCandidateOffersMiddleware, CandidateOfferController.findAll());



/**
 * Get `CandidateOffer` by Id
 */
router.get("/:id", findCandidateOfferByIdMiddleware, CandidateOfferController.findOne());

/**
 * Create a `CandidateOffer`
 */
router.post("/", createCandidateOfferMiddleware, CandidateOfferController.create());

/**
 * Update a `CandidateOffer`
 */
router.put("/:id", patchCandidateOfferMiddleware, CandidateOfferController.patch());

/**
 * Delete a `CandidateOffer`
 */
router.delete("/:id", deleteCandidateOfferMiddleware, CandidateOfferController.delete());

export default router;
