import express from "express";
const router = express.Router();

import ConversationController from "../Controllers/Conversation.controller.js";
import {
	createConversationMiddleware,
	deleteConversationMiddleware,
	findAllConversationsMiddleware,
	findConversationByIdMiddleware,
	createMessageMiddleware,
} from "../Middlewares/conversation.middleware.js";

/**
 * Get All
 */
router.get("/", findAllConversationsMiddleware, ConversationController.findAll());

/**
 * Get `Conversation` by Id
 */
router.get("/:type/:id", findConversationByIdMiddleware, ConversationController.findOne());

/**
 * Get `Conversation` by Id
 */
 router.post("/message", createMessageMiddleware, ConversationController.create());


/**
 * Create a `Conversation`
 */
router.post(
	"/",
	createConversationMiddleware,
	ConversationController.create()
);

/**
 * Delete a `Conversation`
 */
router.delete("/:id", deleteConversationMiddleware, ConversationController.delete());

export default router;
