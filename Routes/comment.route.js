import express from "express";
const router = express.Router();

import CommentController from "../Controllers/Comment.controller.js";
import {
	createCommentMiddleware,
	deleteCommentMiddleware,
	findAllCommentsMiddleware,
	findCommentByIdMiddleware,
	patchCommentMiddleware,
} from "../Middlewares/comment.middleware.js";
import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/", findAllCommentsMiddleware, CommentController.findAll());

/**
 * Get `Comment` by Id
 */
router.get("/:id", findCommentByIdMiddleware, CommentController.findOne());

/**
 * Create a `Comment`
 */
router.post("/", createCommentMiddleware, CommentController.create());

/**
 * Update a `Comment`
 */
router.put("/:id", patchCommentMiddleware, CommentController.patch());

/**
 * Delete a `Comment`
 */
router.delete("/:id", deleteCommentMiddleware, CommentController.delete());

export default router;
