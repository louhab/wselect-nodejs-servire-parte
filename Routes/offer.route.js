import express from "express";
const router = express.Router();

import OfferController from "../Controllers/Offer.controller.js";
import {
	createOfferMiddleware,
	deleteOfferMiddleware,
	findAllOffersMiddleware,
	findOfferByIdMiddleware,
	patchOfferMiddleware,
	findOfferByUuidMiddleware,
} from "../Middlewares/offer.middleware.js";

/**
 * Get All
 */
router.get("/", findAllOffersMiddleware, OfferController.findAll());

/**
 * Get `Offer` by Id
 */
router.get("/:id", findOfferByIdMiddleware, OfferController.findOne());
/**
* Get `Offer` by Uuid
*/
router.get("/uuid/:uuid",findOfferByUuidMiddleware,OfferController.findOne());

/**
 * Create a `Offer`
 */
router.post("/", createOfferMiddleware, OfferController.create());

/**
 * Update a `Offer`
 */
router.put("/:id", patchOfferMiddleware, OfferController.patch());

/**
 * Delete a `Offer`
 */
router.delete("/:id", deleteOfferMiddleware, OfferController.delete());

export default router;
