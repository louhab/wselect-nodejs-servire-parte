import express from "express";
const router = express.Router();

import JobFieldController from "../Controllers/JobField.controller.js";
import {
	createJobFieldMiddleware,
	deleteJobFieldMiddleware,
	findAllJobFieldsMiddleware,
	findJobFieldByIdMiddleware,
	patchJobFieldMiddleware,
} from "../Middlewares/job-field.middleware.js";

/**
 * Get All
 */
router.get("/", findAllJobFieldsMiddleware, JobFieldController.findAll());

/**
 * Get `JobField` by Id
 */
router.get("/:id", findJobFieldByIdMiddleware, JobFieldController.findOne());

/**
 * Create a `JobField`
 */
router.post("/", createJobFieldMiddleware, JobFieldController.create());

/**
 * Update a `JobField`
 */
router.put("/:id", patchJobFieldMiddleware, JobFieldController.patch());

/**
 * Delete a `JobField`
 */
router.delete("/:id", deleteJobFieldMiddleware, JobFieldController.delete());

export default router;
