import express from "express";
const router = express.Router();

import CityController from "../Controllers/City.controller.js";
import {
	createCityMiddleware,
	deleteCityMiddleware,
	findAllCitysMiddleware,
	findCityByIdMiddleware,
	patchCityMiddleware,
} from "../Middlewares/city.middleware.js";
import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
router.get("/", findAllCitysMiddleware, CityController.findAll());

/**
 * Get `City` by Id
 */
router.get("/:id", findCityByIdMiddleware, CityController.findOne());

/**
 * Create a `City`
 */
router.post(
	"/",
	createCityMiddleware,
	CityController.create()
);

/**
 * Create a `City`
 */

/**
 * Update a `City`
 */
router.put("/:id", patchCityMiddleware, CityController.patch());

/**
 * Delete a `City`
 */
router.delete("/:id", deleteCityMiddleware, CityController.delete());

export default router;
