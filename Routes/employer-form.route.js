import express from "express";
const router = express.Router();

import EmployerFormController from "../Controllers/EmployerForm.controller.js";
import {
	createEmployerFormMiddleware,
	patchEmployerFormMiddleware,
	findEmployerFormByIdMiddleware,
	deleteEmployerFormMiddleware,
	findEmployerFormByEmployerIdMiddleware,
	patchEmployerFormByIdMiddleware,
} from "../Middlewares/employer-form.middleware.js";
import formMiddleWare from "../Middlewares/Config/Form.middlware.js";

/**
 * Get All
 */
// router.get(
// 	"/",
// 	findAllEmployerFormsMiddleware,
// 	EmployerFormController.findAll()
// );

/**
 * Get `EmployerForm` by Id
 */
router.get(
	"/:id",
	findEmployerFormByEmployerIdMiddleware,
	EmployerFormController.findOne()
);

/**
 * Get `EmployerForm` by Id
 */
router.get(
	"/",
	findEmployerFormByIdMiddleware,
	EmployerFormController.findOne()
);

/**
 * Create a `EmployerForm`
 */
router.post("/", createEmployerFormMiddleware, EmployerFormController.create());

/**
 * Update a `EmployerForm`
 */
router.put(
	"/:id",
	// formMiddleWare,
	patchEmployerFormByIdMiddleware,
	EmployerFormController.patch()
);

/**
 * Update a `EmployerForm`
 */
router.put(
	"/",
	// formMiddleWare,
	patchEmployerFormMiddleware,
	EmployerFormController.patch()
);

/**
 * Delete a `EmployerForm`
 */
router.delete(
	"/:id",
	deleteEmployerFormMiddleware,
	EmployerFormController.delete()
);

export default router;
