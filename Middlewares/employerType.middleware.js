"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findEmployerTypeById,
	findAllEmployerTypes,
	addEmployerType,
	editEmployerType,
	deleteEmployerType,
} from "../Models/employerType.model.js";

/**
 * Validate Add EmployerType
 */
const employerTypeSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate EmployerType Id
 */

const employerTypeIdSchema = Joi.object({
	id: Joi.number(),
});


const patchEmployerTypeSchema
= Joi.object({
	name: Joi.string().max(255),
	isActive: Joi.boolean(),
});
/**
 * Add a EmployerType
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createEmployerTypeMiddleware = async (req, res, next) => {
	try {
		const result = await employerTypeSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedEmployerType = await addEmployerType(result);
		req.data = addedEmployerType;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `EmployerType`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchEmployerTypeMiddleware = async (req, res, next) => {
	try {
		const result = await patchEmployerTypeSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const employerType = await employerTypeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedEmployerType = await editEmployerType(Number(employerType.id), result);

		if (editedEmployerType.count) {
			req.data = await findEmployerTypeById(Number(employerType.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `EmployerType`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findEmployerTypeByIdMiddleware = async (req, res, next) => {
	try {
		const result = await employerTypeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const employerType = await findEmployerTypeById(id);
		if (!employerType) throw createError.NotFound("No data found!");
		req.data = employerType;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `EmployerType`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllEmployerTypesMiddleware = async (req, res, next) => {
	try {
		const allEmployerTypes = await findAllEmployerTypes();
		if (allEmployerTypes.length === 0) return res.sendStatus(204).end();
		else if (!allEmployerTypes) throw createError.NotFound("No data found!");
		req.data = allEmployerTypes;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `EmployerType`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteEmployerTypeMiddleware = async (req, res, next) => {
	try {
		const result = await employerTypeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const employerType = await deleteEmployerType(id);
		if (!employerType.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `EmployerType`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createEmployerTypeFn = async (result) => {
	try {
		const addedEmployerType = await addEmployerType({});

		return addedEmployerType;
	} catch (error) {
		console.log(error);
	}
};

export {
	createEmployerTypeMiddleware,
	patchEmployerTypeMiddleware,
	findEmployerTypeByIdMiddleware,
	findAllEmployerTypesMiddleware,
	deleteEmployerTypeMiddleware,
	createEmployerTypeFn,
};
