"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findDiplomeById,
	findAllDiplomes,
	addDiplome,
	editDiplome,
	deleteDiplome,
} from "../Models/diplome.model.js";

/**
 * Validate Add Diplome
 */
const diplomeSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate Diplome Id
 */

const diplomeIdSchema = Joi.object({
	id: Joi.number(),
});


/**
 * Add a Diplome
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createDiplomeMiddleware = async (req, res, next) => {
	try {
		const result = await diplomeSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedDiplome = await addDiplome(result);
		req.data = addedDiplome;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Diplome`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchDiplomeMiddleware = async (req, res, next) => {
	try {
		const result = await patchDiplomeSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const diplome = await diplomeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedDiplome = await editDiplome(Number(diplome.id), result);

		if (editedDiplome.count) {
			req.data = await findDiplomeById(Number(diplome.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Diplome`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findDiplomeByIdMiddleware = async (req, res, next) => {
	try {
		const result = await diplomeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const diplome = await findDiplomeById(id);
		if (!diplome) throw createError.NotFound("No data found!");
		req.data = diplome;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Diplome`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllDiplomesMiddleware = async (req, res, next) => {
	try {
		const allDiplomes = await findAllDiplomes();
		if (allDiplomes.length === 0) return res.sendStatus(204).end();
		else if (!allDiplomes) throw createError.NotFound("No data found!");
		req.data = allDiplomes;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Diplome`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteDiplomeMiddleware = async (req, res, next) => {
	try {
		const result = await diplomeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const diplome = await deleteDiplome(id);
		if (!diplome.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Diplome`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createDiplomeFn = async (result) => {
	try {
		const addedDiplome = await addDiplome({});

		return addedDiplome;
	} catch (error) {
		console.log(error);
	}
};

export {
	createDiplomeMiddleware,
	patchDiplomeMiddleware,
	findDiplomeByIdMiddleware,
	findAllDiplomesMiddleware,
	deleteDiplomeMiddleware,
	createDiplomeFn,
};
