"use strict";
import createError from "http-errors";
import {
	findLogByid,
	findAllLogs,
	editLog,
	addLog,
	deleteLog,
} from "../Models/Log.model.js";
import Joi from "joi";



/**
 * Add a addedDocument
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Validate Add Diplome
 */
const logSchema = Joi.object({
	action: Joi.string().max(255),

});
const logIdSchema = Joi.object({
	id: Joi.number(),
})
const createLogMiddleware = async (req, res, next) => {
	try {

		const result = await logSchema.validateAsync(req.body, {
			abortEarly: false,
		});
		const addedLog = await addLog({
			action: result.action,
		});
		req.data = addedLog;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Log`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchLogMiddleware = async (req, res, next) => {
	try {
		const editedLog = await editLog(Number(document.id), result);

		if (editedLog.count) {
			req.data = await findLogById(Number(document.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};
/**
 * Get one `Log` 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findLogByIdMiddleware = async (req, res, next) => {
	try {
		const result = await logIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const log = await findLogById(id);
		if (!log) throw createError.NotFound("No data found!");
		req.data = log;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `logs` 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllLogsMiddleware = async (req, res, next) => {
	try {
		const allLogs = await findAllLogs();
		if (allLogs.length === 0) return res.sendStatus(204).end();
		else if (!allLogs) throw createError.NotFound("No data found!");
		req.data = allLogs;
		next();
	} catch (error) {
		next(error);
	}
};


/**
 * Delete a `log` 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteLogMiddleware = async (req, res, next) => {
	try {
		const result = await logIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const log = await deleteLog(id);
		if (!log.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

export {
	createLogMiddleware,
	patchLogMiddleware,
	findLogByIdMiddleware,
	findAllLogsMiddleware,
	deleteLogMiddleware,
};