"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findJobFieldById,
	findAllJobFields,
	addJobField,
	editJobField,
	deleteJobField,
} from "../Models/job-field.js";

/**
 * Validate Add JobField
 */
const jobFieldSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate JobField Id
 */

const jobFieldIdSchema = Joi.object({
	id: Joi.number(),
});

const patchJobFieldSchema = Joi.object({
	name: Joi.string().max(255),
	isActive: Joi.boolean(),
});
/**
 * Add a JobField
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createJobFieldMiddleware = async (req, res, next) => {
	try {
		const result = await jobFieldSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedJobField = await addJobField(result);
		req.data = addedJobField;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `JobField`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchJobFieldMiddleware = async (req, res, next) => {
	try {
		const result = await patchJobFieldSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const jobField = await jobFieldIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedJobField = await editJobField(Number(jobField.id), result);

		if (editedJobField.count) {
			req.data = await findJobFieldById(Number(jobField.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `JobField`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findJobFieldByIdMiddleware = async (req, res, next) => {
	try {
		const result = await jobFieldIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const jobField = await findJobFieldById(id);
		if (!jobField) throw createError.NotFound("No data found!");
		req.data = jobField;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `JobField`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllJobFieldsMiddleware = async (req, res, next) => {
	try {
		const allJobFields = await findAllJobFields();
		if (allJobFields.length === 0) return res.sendStatus(204).end();
		else if (!allJobFields) throw createError.NotFound("No data found!");
		req.data = allJobFields;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `JobField`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteJobFieldMiddleware = async (req, res, next) => {
	try {
		const result = await jobFieldIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const jobField = await deleteJobField(id);
		if (!jobField.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `JobField`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createJobFieldFn = async (result) => {
	try {
		const addedJobField = await addJobField({});

		return addedJobField;
	} catch (error) {
		console.log(error);
	}
};

export {
	createJobFieldMiddleware,
	patchJobFieldMiddleware,
	findJobFieldByIdMiddleware,
	findAllJobFieldsMiddleware,
	deleteJobFieldMiddleware,
	createJobFieldFn,
};
