"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findCompanyById,
	findAllCompanys,
	addCompany,
	editCompany,
	deleteCompany,
} from "../Models/company.model.js";

/**
 * Validate Add Company
 */
const companySchema = Joi.object({
	name: Joi.string().max(255),
	companySize: Joi.string().max(255),
	foundedIn: Joi.string().max(255),
	phone: Joi.string().max(255),
	email: Joi.string().max(255),
	website: Joi.string().max(255),
	discription: Joi.string(),
	isActive: Joi.boolean(),
	domaineDactiviteId: Joi.number(),
	countryId: Joi.number(),
	cityId: Joi.number(),
});

/**
 * Validate Company Id
 */

const companyIdSchema = Joi.object({
	id: Joi.number(),
});


const patchCompanySchema
	= Joi.object({
		name: Joi.string().max(255),
		isActive: Joi.boolean(),

		companySize: Joi.string().max(255),
		foundedIn: Joi.string().max(255),
		phone: Joi.string().max(255),
		email: Joi.string().max(255),
		website: Joi.string().max(255),
		discription: Joi.string(),
		isActive: Joi.boolean(),
		domaineDactiviteId: Joi.number(),
		countryId: Joi.number(),
		cityId: Joi.number(),
	});
/**
 * Add a Company
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCompanyMiddleware = async (req, res, next) => {
	try {
		const result = await companySchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedCompany = await addCompany(result);
		req.data = addedCompany;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Company`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCompanyMiddleware = async (req, res, next) => {
	try {
		const result = await patchCompanySchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const company = await companyIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCompany = await editCompany(Number(company.id), result);

		if (editedCompany.count) {
			req.data = await findCompanyById(Number(company.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Company`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCompanyByIdMiddleware = async (req, res, next) => {
	try {
		const result = await companyIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const company = await findCompanyById(id);
		if (!company) throw createError.NotFound("No data found!");
		req.data = company;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Company`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCompanysMiddleware = async (req, res, next) => {
	try {
		const allCompanys = await findAllCompanys();
		if (allCompanys.length === 0) return res.sendStatus(204).end();
		else if (!allCompanys) throw createError.NotFound("No data found!");
		req.data = allCompanys;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Company`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCompanyMiddleware = async (req, res, next) => {
	try {
		const result = await companyIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const company = await deleteCompany(id);
		if (!company.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Company`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCompanyFn = async (result) => {
	try {
		const addedCompany = await addCompany({});

		return addedCompany;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCompanyMiddleware,
	patchCompanyMiddleware,
	findCompanyByIdMiddleware,
	findAllCompanysMiddleware,
	deleteCompanyMiddleware,
	createCompanyFn,
};
