"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findWs101FormById,
	addWs101Form,
	editWs101Form,
	deleteWs101Form,
} from "../Models/ws101.model.js";

const languageLevel = ["very_good", "good", "average", "weak"];

/**
 * Validate Add Ws101Form
 */
const ws101FormSchema = Joi.object({
	firstName: Joi.string().allow(null, ""),
	lastName: Joi.string().allow(null, ""),
	phone: Joi.string().allow(null, ""),
	email: Joi.string().allow(null, ""),
	//
	// 3
	birthDate: Joi.string().allow(null, ""),
	city: Joi.string().allow(null, ""),
	maritalStatus: Joi.string().allow(null, ""),

	// secoend part => 4
	educationLevel: Joi.string().allow(null, ""),
	studyField: Joi.string().allow(null, ""),
	graduationYear: Joi.string().allow(null, ""),
	diplome: Joi.string().allow(null, ""),

	// third part => 10
	havingFrenchTest: Joi.boolean().allow(null, ""),
	frenchTestLabel: Joi.string().allow(null, ""),
	frenchTestScore: Joi.string().allow(null, ""),
	frenchTestYear: Joi.string().allow(null, ""),
	frenchLevel: Joi.any().valid(...languageLevel),
	havingEnglishTest: Joi.boolean().allow(null, ""),
	englishTestLabel: Joi.string().allow(null, ""),
	englishTestScore: Joi.string().allow(null, ""),
	englishTestYear: Joi.string().allow(null, ""),
	englishLevel: Joi.any().valid(...languageLevel),

	// last part => 12
	havingJob: Joi.boolean().allow(null, ""),
	jobField: Joi.string().allow(null, ""),
	employerName: Joi.string().allow(null, ""),
	experienceYears: Joi.string().allow(null, ""),

	havingCNSS: Joi.boolean().allow(null, ""),
	declaredDays: Joi.string().allow(null, ""),

	havingOtherJob: Joi.boolean().allow(null, ""),
	otherJobField: Joi.string().allow(null, ""),
	otherJobEmployerName: Joi.string().allow(null, ""),
	otherJobExperienceYears: Joi.string().allow(null, ""),

	havingCNSSOtherJob: Joi.boolean().allow(null, ""),
	otherJobDeclaredDays: Joi.string().allow(null, ""),
});

/**
 * Validate candidate Id
 */

const candidateIdSchema = Joi.object({
	candidateId: Joi.number(),
});

/**
 * Add a Ws101Form
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createWs101FormMiddleware = async (req, res, next) => {
	try {
		const result = await ws101FormSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedWs101Form = addWs101Form(result);

		req.data = addedWs101Form;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a Ws101Form
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchWs101FormMiddleware = async (req, res, next) => {
	try {
		const result = await ws101FormSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const candidateId = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		console.log("After Validation ::", result);

		const id = Number(candidateId.candidateId);

		delete result?.firstName;
		delete result?.lastName;
		delete result?.phone;
		delete result?.email;

		// clear all null records
		Object.keys(result).forEach((key) => {
			if (result[key] == null || result[key] == "") {
				delete result[key];
			}
		});

		console.log("before changing ::", result);

		const editedWs101Form = await editWs101Form(id, result);

		if (editedWs101Form.count) {
			req.data = await findWs101FormById(id);
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Ws101Form`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findWs101FormByIdMiddleware = async (req, res, next) => {
	try {
		const result = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.candidateId);
		const record = findWs101FormById(id);
		if (!record) throw createError.NotFound("No data found!");
		req.data = record;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Delete a `Ws101Form`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteWs101FormMiddleware = async (req, res, next) => {
	try {
		const result = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.candidateId);
		const ws101Form = deleteWs101Form(id);
		if (!ws101Form.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

// /**
//  * Add a `Ws101Form`
//  * @param {*} req
//  * @param {*} res
//  * @param {*} next
//  */
// const createWs101FormFn = async (result) => {
// 	try {
// 		const addedWs101Form = await addWs101Form({
// 			result,
// 		});

// 		return addedWs101Form;
// 	} catch (error) {
// 		console.log(error);
// 	}
// };

export {
	createWs101FormMiddleware,
	patchWs101FormMiddleware,
	findWs101FormByIdMiddleware,
	deleteWs101FormMiddleware,
	// createWs101FormFn,
};
