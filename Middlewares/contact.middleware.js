"use strict";
import Joi from "joi";
import createError from "http-errors";
import { sendContactForm } from "../helpers/sendEmail.hepler.js";

/**
 * Validate Contact form Schema
 */
const contactFormSchema = Joi.object({
	name: Joi.string().max(255).required(),
	email: Joi.string().max(255).required(),
	message: Joi.string().required(),
});

/**
 * Send A contact form
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const sendContactFormMiddleware = async (req, res, next) => {
	try {
		const result = await contactFormSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		sendContactForm(result.email, result.name, result.message);

		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

export { sendContactFormMiddleware };
