"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findCarrerLevelById,
	findAllCarrerLevels,
	addCarrerLevel,
	editCarrerLevel,
	deleteCarrerLevel,
} from "../Models/carrerLevel.model.js";

/**
 * Validate Add CarrerLevel
 */
const carrerLevelSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate CarrerLevel Id
 */

const carrerLevelIdSchema = Joi.object({
	id: Joi.number(),
});


const patchCarrerLevelSchema
= Joi.object({
	name: Joi.string().max(255),
	isActive: Joi.boolean(),
});
/**
 * Add a CarrerLevel
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCarrerLevelMiddleware = async (req, res, next) => {
	try {
		const result = await carrerLevelSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedCarrerLevel = await addCarrerLevel(result);
		req.data = addedCarrerLevel;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `CarrerLevel`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCarrerLevelMiddleware = async (req, res, next) => {
	try {
		const result = await patchCarrerLevelSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const carrerLevel = await carrerLevelIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCarrerLevel = await editCarrerLevel(Number(carrerLevel.id), result);

		if (editedCarrerLevel.count) {
			req.data = await findCarrerLevelById(Number(carrerLevel.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `CarrerLevel`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCarrerLevelByIdMiddleware = async (req, res, next) => {
	try {
		const result = await carrerLevelIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const carrerLevel = await findCarrerLevelById(id);
		if (!carrerLevel) throw createError.NotFound("No data found!");
		req.data = carrerLevel;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `CarrerLevel`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCarrerLevelsMiddleware = async (req, res, next) => {
	try {
		const allCarrerLevels = await findAllCarrerLevels();
		if (allCarrerLevels.length === 0) return res.sendStatus(204).end();
		else if (!allCarrerLevels) throw createError.NotFound("No data found!");
		req.data = allCarrerLevels;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `CarrerLevel`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCarrerLevelMiddleware = async (req, res, next) => {
	try {
		const result = await carrerLevelIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const carrerLevel = await deleteCarrerLevel(id);
		if (!carrerLevel.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `CarrerLevel`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCarrerLevelFn = async (result) => {
	try {
		const addedCarrerLevel = await addCarrerLevel({});

		return addedCarrerLevel;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCarrerLevelMiddleware,
	patchCarrerLevelMiddleware,
	findCarrerLevelByIdMiddleware,
	findAllCarrerLevelsMiddleware,
	deleteCarrerLevelMiddleware,
	createCarrerLevelFn,
};
