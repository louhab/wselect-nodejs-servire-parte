"use strict";
import Joi from "joi";
import createError from "http-errors";
import bcrypt from "bcrypt";
import { sendEmailValidation } from "../helpers/sendEmail.hepler.js";
import { signAccessToken } from "../helpers/jwt.helper.js";	
import { addEmployerContract } from "../Models/employerContract.js";
import {
	findEmployerById,
	findAllEmployers,
	addEmployer,
	editEmployer,
	deleteEmployer,
	countEmployer,
	findEmployerByUuid,
} from "../Models/employer.model.js";
import { findUserById } from "../Models/user.model.js";
import fs from "fs";
import path from "path";
const __dirname = path.resolve();

/**
 * Validate Add Employer Contract 
 */
const employerContractSchema = Joi.object({
	name: Joi.string().max(255),
	path: Joi.string().max(255),
	employerId  : Joi.number().required()
});

/**
 * Validate Add Employer
 */
const employerSchema = Joi.object({
	companyName: Joi.string().max(255),
	contactName: Joi.string().max(255),
	contactPhone: Joi.string().max(255),
	address: Joi.string().max(255).allow(null, ""),
	email: Joi.string().max(255),
	remarks: Joi.string(),
	website: Joi.string().max(255),
	// potentialId: Joi.number().allow(null),
});

/**
 * Validate patch Employer
 */
const patchEmployerSchema = Joi.object({
	companyName: Joi.string().max(255).allow(null, ""),
	contactName: Joi.string().max(255).allow(null, ""),
	contactPhone: Joi.string().max(255).allow(null, ""),
	address: Joi.string().max(255).allow(null, ""),
	email: Joi.string().max(255).allow(null, ""),
	remarks: Joi.string().allow(null, ""),
	companieId: Joi.number().allow(null, ""),
	potentialId: Joi.number().allow(null, ""),
	isActive: Joi.boolean().allow(null, ""),
	website: Joi.string().max(255),
	status:Joi.any()
		.valid(
			"Nd",
			"Pas_Interesse",
			"Contract",
			"Devis",
			"Injoignable",
			"Client"
		)
		.allow(null, ""),
	password : Joi.string().max(555),	
});

const updateEmployerPasswordSchema = Joi.object({
	newPassword: Joi.string().required(),
	currentPassword: Joi.string().required(),
});
/**
 * Validate the uuid of the employeer 
 */
const employerPatchUuid = Joi.object({
	uuid : Joi.string().required(),
});

/**
 * Validate Sign in
 */
const signInEmployerSchema = Joi.object({
	email: Joi.string().max(255).required(),
	password: Joi.string().required(),
});

/**
 * Validate Employer Id
 */
const employerIdSchema = Joi.object({
	id: Joi.number(),
});


/**
 * Validate update contract
 */

const updateEmployerContractSchema = Joi.object({
	contract: Joi.any(),
});

/*
* Get A Employer By Uuid
*/
const findEmployerByUuidMiddleware  = async (req, res, next) => {
	try {
		const result = await employerPatchUuid.validateAsync(req.params, {
			abortEarly: false,
		});
		const employer = await findEmployerByUuid(result.uuid);
		if (!employer) throw createError.NotFound("No data found!");
		req.data = employer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
}

/**
 * Add a Employer
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createEmployerMiddleware = async (req, res, next) => {
	try {
		const result = await employerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const ref = req.query?.ref ? Number(req.query?.ref) : null;
		let userId = null;

		if (ref) userId = await findUserById(ref);

		const employerRef = userId?.id;
		const isExists = await countEmployer(result.email);

		if (isExists) throw createError.Conflict("Email deja utilisé");
		const randomPass = Math.random().toString(36).slice(-8);
		const salt = bcrypt.genSaltSync();
		const hashedPass = bcrypt.hashSync(randomPass, salt);

		const addedEmployer = await addEmployer({
			...result,
			password: hashedPass,
			employerRefId: employerRef,
		});

		sendEmailValidation(result?.email, result?.contactName, randomPass);

		req.data = addedEmployer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a Employer
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const signInEmployerMiddleware = async (req, res, next) => {
	try {
		const result = await signInEmployerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const isExists = await countEmployer(result.email);

		if (!isExists)
			throw createError.Unauthorized(
				"Les informations d'identification invalides"
			);

		const employer = await findEmployerById("email", result.email);

		const isCorrect = await bcrypt.compare(
			result.password,
			employer.password
		);

		if (!isCorrect)
			throw createError.Unauthorized(
				"Les informations d'identification invalides"
			);

		const accessToken = await signAccessToken(employer.id, "e");

		delete employer.password;

		req.data = {
			accessToken,
			user: employer,
		};
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const patchEmployerContractMiddleware = async (req, res, next) => {
	try {
		const result = await updateEmployerContractSchema.validateAsync(
			req.body,
			{
				abortEarly: false,
			}
		);

		const employer = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const { files } = req;
		const ext = "." + files["contract"].originalFilename.split(".").pop();
		const pathOnly = path.join(
			__dirname,
			`/public/uploads/contract/${employer.id}`
		);
		const fileName = "employer" + ext;

		if (!fs.existsSync(pathOnly)) {
			fs.mkdirSync(pathOnly);
		}

		const newPath = path.join(pathOnly, fileName);
		console.log(newPath);

		fs.rename(files["contract"].filepath, newPath, (err) => {
			if (err) console.error(err);
		});
		const editedEmployer = await editEmployer(Number(employer.id), {
			contract: `uploads/contract/${employer.id}/${fileName}`,
		});
		if (editedEmployer.count) {
			req.data = await findEmployerById("id", Number(employer.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchEmployerMiddleware = async (req, res, next) => {
	try {
		
		const result = await patchEmployerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const employer = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});
		const editedEmployer = await editEmployer(Number(employer.id), result);
		if (editedEmployer.count) {
			req.data = await findEmployerById("id", Number(employer.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchEmployerPasswordMiddleware = async (req, res, next) => {
	try {
		const result = await updateEmployerPasswordSchema.validateAsync(
			req.body,
			{ abortEarly: false }
		);

		const param = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const employer = await findEmployerById("id", param.id);

		if (!employer) throw createError.BadRequest("invalid user");

		const isCorrect = await bcrypt.compare(
			result.currentPassword,
			employer.password
		);

		if (!isCorrect) throw createError.BadRequest("password incorrect");

		const salt = await bcrypt.genSalt(10);
		const hashedPassword = await bcrypt.hash(result.newPassword, salt);

		const record = await editEmployer(employer.id, {
			password: hashedPassword,
		});

		if (record.count) {
			req.data = await findEmployerById("id", Number(employer.id));
		} else throw createError.NotFound("Record has not been updated");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findEmployerByIdMiddleware = async (req, res, next) => {
	try {
		const result = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const employer = await findEmployerById("id", id);
		if (!employer) throw createError.NotFound("No data found!");
		req.data = employer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllEmployersMiddleware = async (req, res, next) => {
	try {
		
		const allEmployers = await findAllEmployers(Number(req.query.skip),Number(req.query.take));
		if (allEmployers.length === 0) return res.sendStatus(204).end();
		else if (!allEmployers) throw createError.NotFound("No data found!");
		req.data = allEmployers;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteEmployerMiddleware = async (req, res, next) => {
	try {
		const result = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const employer = await deleteEmployer(id);
		if (!employer.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createEmployerFn = async (result) => {
	try {
		const addedEmployer = await addEmployer({
			companyName: result.companyName,
			contactName: result.contactName,
			contactPhone: result.contactPhone,
			address: result.address,
			email: result.email,
			remarks: result.remarks,
			potentialId: result.potentialId,
		});

		return addedEmployer;
	} catch (error) {
		console.log(error);
	}
};
const createEmployerContractMiddleware = async (req, res, next) => {
	try {
		const result = await employerContractSchema.validateAsync(req.body, {
			abortEarly: false,
		});
			// handle files,
			let file = req.files?.contract ;
			let name = file.name ;
			file.mv('public/cotrats/'+name, (err)=>{
				if(err) throw err 
			} )
			let dataToAdd = {
				name: name,
				path : `public/cotrats/${name}`,
				employerId : result.employerId,
			}	
			const addedEmployerContract = await addEmployerContract(dataToAdd);
			req.data = addedEmployerContract;
			next();	
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

export {
	createEmployerMiddleware,
	patchEmployerMiddleware,
	findEmployerByIdMiddleware,
	findAllEmployersMiddleware,
	deleteEmployerMiddleware,
	createEmployerFn,
	signInEmployerMiddleware,
	patchEmployerPasswordMiddleware,
	patchEmployerContractMiddleware,
	createEmployerContractMiddleware,
	findEmployerByUuidMiddleware,
};
