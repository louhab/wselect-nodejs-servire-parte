"use strict";
import Joi from "joi";
import createError from "http-errors";
import { sendFormURL } from "../helpers/sendEmail.hepler.js";

import fs from "fs";
import path from "path";

const __dirname = path.resolve();

import {
	addEmployerForm,
	editEmployerForm,
	findAllEmployerForms,
	findEmployerFormById,
	deleteEmployerForm,
	countEmployerForm,
} from "../Models/employer-form.model.js";

function isEmpty(obj) {
	return Object.keys(obj).length === 0;
}

/**
 * Validate Update Employer
 */
const updateEmployerSchema = Joi.object({
	accountNumber: Joi.string().allow(null, ""),
	companyName: Joi.string().allow(null, ""),
	companyAddress: Joi.string().allow(null, ""),
	companyCity: Joi.string().allow(null, ""),
	companyProvince: Joi.string().allow(null, ""),
	companyCountry: Joi.string().allow(null, ""),
	companyZip: Joi.string().allow(null, ""),

	companyAddressBis: Joi.string().allow(null, ""),
	companyCityBis: Joi.string().allow(null, ""),
	companyProvinceBis: Joi.string().allow(null, ""),
	companyCountryBis: Joi.string().allow(null, ""),
	companyZipBis: Joi.string().allow(null, ""),

	website: Joi.string().allow(null, ""),
	startDate: Joi.date().allow(null, ""),

	isIndividual: Joi.boolean().allow(null, ""),
	isPartnership: Joi.boolean().allow(null, ""),
	isCompany: Joi.boolean().allow(null, ""),
	isCooperative: Joi.boolean().allow(null, ""),
	isNonprofit: Joi.boolean().allow(null, ""),
	isRegisteredCharity: Joi.boolean().allow(null, ""),

	//   Second Section
	firstName: Joi.string().allow(null, ""),
	secondFirstName: Joi.string().allow(null, ""),
	lastName: Joi.string().allow(null, ""),

	positionTitle: Joi.string().allow(null, ""),
	firstPhone: Joi.string().allow(null, ""),
	firstPosition: Joi.string().allow(null, ""),

	secondPhone: Joi.string().allow(null, ""),
	secondPosition: Joi.string().allow(null, ""),

	faxNumber: Joi.string().allow(null, ""),
	// email: Joi.string().allow(null, ""),

	address: Joi.string().allow(null, ""),
	city: Joi.string().allow(null, ""),
	province: Joi.string().allow(null, ""),
	country: Joi.string().allow(null, ""),
	zip: Joi.string().allow(null, ""),

	firstNameBis: Joi.string().allow(null, ""),
	secondFirstNameBis: Joi.string().allow(null, ""),
	lastNameBis: Joi.string().allow(null, ""),

	positionTitleBis: Joi.string().allow(null, ""),
	firstPhoneBis: Joi.string().allow(null, ""),
	firstPositionBis: Joi.string().allow(null, ""),

	secondPhoneBis: Joi.string().allow(null, ""),
	secondPositionBis: Joi.string().allow(null, ""),

	faxNumberBis: Joi.string().allow(null, ""),
	emailBis: Joi.string().allow(null, ""),

	addressBis: Joi.string().allow(null, ""),
	cityBis: Joi.string().allow(null, ""),
	provinceBis: Joi.string().allow(null, ""),
	countryBis: Joi.string().allow(null, ""),
	zipBis: Joi.string().allow(null, ""),

	//   Section 3

	secThree1: Joi.string().allow(null, ""),
	secThree2: Joi.boolean().allow(null, ""),
	secThree3: Joi.boolean().allow(null, ""),
	secThree4: Joi.string().allow(null, ""),
	secThree5: Joi.string().allow(null, ""),
	secThree6: Joi.string().allow(null, ""),

	secThree7: Joi.boolean().allow(null, ""),
	secThree8: Joi.string().allow(null, ""),

	secThree9: Joi.boolean().allow(null, ""),
	secThree10: Joi.string().allow(null, ""),

	tradeName: Joi.string().allow(null, ""),
	workspaceDescription: Joi.string().allow(null, ""),
	workspaceAddress: Joi.string().allow(null, ""),
	workspaceCity: Joi.string().allow(null, ""),
	workspaceProvince: Joi.string().allow(null, ""),
	workspaceZip: Joi.string().allow(null, ""),
	secFive1: Joi.string().allow(null, ""),
	secFive2: Joi.string().allow(null, ""),
	secFive3: Joi.boolean().allow(null, ""),
	secFive4: Joi.string().allow(null, ""),
	secFive5: Joi.string().allow(null, ""),
	secFive6: Joi.boolean().allow(null, ""),
	secFive7: Joi.string().allow(null, ""),
	secFive8: Joi.string().allow(null, ""),
	secFive9: Joi.boolean().allow(null, ""),
	secFive10: Joi.string().allow(null, ""),
	secFive11: Joi.boolean().allow(null, ""),
	secFive12: Joi.any().allow(null, ""),
	secFive13: Joi.boolean().allow(null, ""),
	secFive14: Joi.boolean().allow(null, ""),
	secFive15: Joi.boolean().allow(null, ""),
	secFive16: Joi.boolean().allow(null, ""),
	secFive17: Joi.boolean().allow(null, ""),
	secFive18: Joi.string().allow(null, ""),
	secFive19: Joi.string().allow(null, ""),
});

/**
 * Validate Add Employer
 */
const employerSchema = Joi.object({
	email: Joi.string().required(),
});

/**
 * Validate Employer Id
 */

const employerUUIDSchema = Joi.object({
	uuid: Joi.string(),
});

/**
 * Validate Employer Id
 */

const employerIdSchema = Joi.object({
	id: Joi.string(),
});

/**
 * Add a Employer
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createEmployerFormMiddleware = async (req, res, next) => {
	try {
		const result = await employerSchema.validateAsync(req.body, {
			abortEarly: false,
		});
		const isExists = await countEmployerForm(result.email);
		if (isExists) throw createError.Conflict("Email deja utilisé");
		const addedEmployerForms = await addEmployerForm(result);
		sendFormURL(result?.email, addedEmployerForms.uuid);
		req.data = addedEmployerForms;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchEmployerFormByIdMiddleware = async (req, res, next) => {
	try {
		const result = await updateEmployerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const { id } = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		let fileName;

		if (false) {
			const ext =
				"." + files["secFive12"]?.originalFilename.split(".").pop();
			const pathOnly = path.join(
				__dirname,
				`/public/uploads/${employer.uuid}`
			);
			fileName = employer.uuid + ext;
			if (!fs.existsSync(pathOnly)) {
				console.log("not exist");
				fs.mkdirSync(pathOnly);
			}
			const newPath = path.join(pathOnly, fileName);
			fs.rename(files["secFive12"].filepath, newPath, (err) => {
				if (err) console.error(err);
			});
		}

		const editedEmployer = await editEmployerForm(Number(id), {
			...result,
			// secFive12: !isEmpty(files)
			// 	? `uploads/${employer.uuid}/${fileName}`
			// 	: null,
		});

		if (editedEmployer.count) {
			req.data = await findEmployerFormById(Number(id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchEmployerFormMiddleware = async (req, res, next) => {
	try {
		const result = await updateEmployerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		// const { files } = req;
		// console.log(files);
		let fileName;

		if (false) {
			const ext =
				"." + files["secFive12"]?.originalFilename.split(".").pop();
			const pathOnly = path.join(
				__dirname,
				`/public/uploads/${employer.uuid}`
			);

			fileName = employer.uuid + ext;

			if (!fs.existsSync(pathOnly)) {
				console.log("not exist");
				fs.mkdirSync(pathOnly);
			}

			const newPath = path.join(pathOnly, fileName);

			fs.rename(files["secFive12"].filepath, newPath, (err) => {
				if (err) console.error(err);
			});
		}

		const editedEmployer = await editEmployerForm(req?.payload?.id, {
			...result,
			// secFive12: !isEmpty(files)
			// 	? `uploads/${employer.uuid}/${fileName}`
			// 	: null,
		});

		if (editedEmployer.count) {
			req.data = await findEmployerFormById(req?.payload?.id);
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findEmployerFormByEmployerIdMiddleware = async (req, res, next) => {
	try {
		const { id } = await employerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});
		const employer = await findEmployerFormById(Number(id));
		if (!employer) throw createError.NotFound("No data found!");
		req.data = employer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findEmployerFormByIdMiddleware = async (req, res, next) => {
	try {
		const employer = await findEmployerFormById(req?.payload?.id);
		if (!employer) throw createError.NotFound("No data found!");
		req.data = employer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllEmployerFormsMiddleware = async (req, res, next) => {
	try {
		const allEmployers = await findAllEmployerForms();
		if (allEmployers.length === 0) return res.sendStatus(204).end();
		else if (!allEmployers) throw createError.NotFound("No data found!");
		req.data = allEmployers;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Employer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteEmployerFormMiddleware = async (req, res, next) => {
	try {
		const result = await employerUUIDSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const employer = await deleteEmployerForm(id);
		if (!employer.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

export {
	createEmployerFormMiddleware,
	patchEmployerFormMiddleware,
	findEmployerFormByIdMiddleware,
	findAllEmployerFormsMiddleware,
	deleteEmployerFormMiddleware,
	findEmployerFormByEmployerIdMiddleware,
	patchEmployerFormByIdMiddleware,
};
