"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findCommentById,
	findAllComments,
	addComment,
	editComment,
	deleteComment,
} from "../Models/comment.model.js";

/**
 * Validate Add Comment
 */
const commentSchema = Joi.object({
	comment: Joi.string(),
	employerId: Joi.number(),
});

/**
 * Validate Comment Id
 */

const commentIdSchema = Joi.object({
	id: Joi.number(),
});

const patchCommentSchema = Joi.object({
	comment: Joi.string(),
});
/**
 * Add a Comment
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCommentMiddleware = async (req, res, next) => {
	try {
		const result = await commentSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		console.log();

		const addedComment = await addComment({
			...result,
			userId: req.payload.id,
		});

		req.data = addedComment;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Comment`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCommentMiddleware = async (req, res, next) => {
	try {
		const result = await patchCommentSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const comment = await commentIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedComment = await editComment(Number(comment.id), result);

		if (editedComment.count) {
			req.data = await findCommentById(Number(comment.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Comment`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCommentByIdMiddleware = async (req, res, next) => {
	try {
		const result = await commentIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const comment = await findCommentById(id);
		if (!comment) throw createError.NotFound("No data found!");
		req.data = comment;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Comment`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCommentsMiddleware = async (req, res, next) => {
	try {
		const allComments = await findAllComments();
		if (allComments.length === 0) return res.sendStatus(204).end();
		else if (!allComments) throw createError.NotFound("No data found!");
		req.data = allComments;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Comment`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCommentMiddleware = async (req, res, next) => {
	try {
		const result = await commentIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const comment = await deleteComment(id);
		if (!comment.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Comment`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCommentFn = async (result) => {
	try {
		const addedComment = await addComment({});

		return addedComment;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCommentMiddleware,
	patchCommentMiddleware,
	findCommentByIdMiddleware,
	findAllCommentsMiddleware,
	deleteCommentMiddleware,
	createCommentFn,
};
