import formidable from "formidable";
import path from "path";

const __dirname = path.resolve();

const formMiddleWare = (req, res, next) => {
	const form = formidable({
		uploadDir: path.join(__dirname, `/public/tmp`),
		maxFieldsSize: 2 * 1024 * 1024,
	});

	// Throw an error
	form.on("error", (err) => console.error(err));

	form.parse(req, (err, fields, files) => {
		if (err) {
			next(err);
			return;
		}
		req.fields = fields;
		req.files = files;
		next();
	});
};

export default formMiddleWare;
