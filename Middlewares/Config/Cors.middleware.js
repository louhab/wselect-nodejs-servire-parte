"use strict";

import cors from "cors";

var corsOptions = {
	origin: [
		"http://localhost:3000",
		"http://localhost:6001",
		"http://localhost:6002",
		"http://64.226.123.201",
		"http://64.226.123.201:6002",
		"http://64.226.123.201:3000",
		"http://64.226.123.201:6001",
		"http://preprod.wselect.ca",
		"http://legacy.wselect.ca",
		"https://legacy.wselect.ca",
		"http://wselect:3000",
		"https://wselect.ca/",
		"https://wselect.ca",
		"*",
	],
	optionsSuccessStatus: 200,
};

export default cors(corsOptions);
