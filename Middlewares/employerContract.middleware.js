"use strict";
import Joi from "joi";
import createError from "http-errors";
import { addEmployerContract } from "../Models/employerContract";
/**
 * Validate Add Employer Contract 
 */
const employerContractSchema = Joi.object({
	name: Joi.string().max(255),
	path: Joi.string().max(255),
});

const createEmployerContractMiddleware = async (req, res, next) => {
	try {
		const result = await employerContractSchema.validateAsync(req.body, {
			abortEarly: false,
		});
		const addedEmployerContract = await addEmployerContract(result);
		req.data = addedEmployerContract;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};
export {
    createEmployerContractMiddleware,
}
