"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findCountryById,
	findAllCountrys,
	addCountry,
	editCountry,
	deleteCountry,
} from "../Models/country.model.js";

/**
 * Validate Add Country
 */
const countrySchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate Country Id
 */

const countryIdSchema = Joi.object({
	id: Joi.number(),
});


const patchCountrySchema
= Joi.object({
	name: Joi.string().max(255),
	isActive: Joi.boolean(),
});
/**
 * Add a Country
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCountryMiddleware = async (req, res, next) => {
	try {
		const result = await countrySchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedCountry = await addCountry(result);
		req.data = addedCountry;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Country`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCountryMiddleware = async (req, res, next) => {
	try {
		const result = await patchCountrySchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const country = await countryIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCountry = await editCountry(Number(country.id), result);

		if (editedCountry.count) {
			req.data = await findCountryById(Number(country.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Country`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCountryByIdMiddleware = async (req, res, next) => {
	try {
		const result = await countryIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const country = await findCountryById(id);
		if (!country) throw createError.NotFound("No data found!");
		req.data = country;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Country`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCountrysMiddleware = async (req, res, next) => {
	try {
		const allCountrys = await findAllCountrys();
		if (allCountrys.length === 0) return res.sendStatus(204).end();
		else if (!allCountrys) throw createError.NotFound("No data found!");
		req.data = allCountrys;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Country`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCountryMiddleware = async (req, res, next) => {
	try {
		const result = await countryIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const country = await deleteCountry(id);
		if (!country.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Country`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCountryFn = async (result) => {
	try {
		const addedCountry = await addCountry({});

		return addedCountry;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCountryMiddleware,
	patchCountryMiddleware,
	findCountryByIdMiddleware,
	findAllCountrysMiddleware,
	deleteCountryMiddleware,
	createCountryFn,
};
