"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findAbonnementById,
	findAllAbonnements,
	addAbonnement,
	editAbonnement,
	deleteAbonnement,
} from "../Models/abonnement.model.js";

/**
 * Validate Add Abonnement
 */
const abonnementSchema = Joi.object({
	title: Joi.string().max(255),
	pricePerMonth: Joi.number(),
	currency: Joi.string().max(255),
	countryId: Joi.number(),
  
	// countryAndCity: Joi.string().max(255),
	countryAndCityId: Joi.number(),
	details: Joi.any()
});

/**
 * Validate patch Abonnement
 */
const patchAbonnementSchema = Joi.object({
	title: Joi.string().max(255),
	pricePerMonth: Joi.number(),
	currency: Joi.string().max(255),
  
	countryAndCity: Joi.string().max(255),
	countryAndCityId: Joi.number(),
	details: Joi.any(),
});


/**
 * Validate Abonnement Id
 */

const abonnementIdSchema = Joi.object({
	id: Joi.number(),
});

/**
 * Add a Abonnement
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createAbonnementMiddleware = async (req, res, next) => {
	try {

		const result = await abonnementSchema.validateAsync(req.body, {
			abortEarly: false,
		});
	console.log("err", result)
		const addedAbonnement = await addAbonnement({
			...result,
			details: JSON.stringify(result?.details)
		});

			req.data = addedAbonnement;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Abonnement`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchAbonnementMiddleware = async (req, res, next) => {
	try {
		const result = await patchAbonnementSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const abonnement = await abonnementIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedAbonnement = await editAbonnement(Number(abonnement.id), result);

		if (editedAbonnement.count) {
			req.data = await findAbonnementById("id", Number(abonnement.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Abonnement`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findAbonnementByIdMiddleware = async (req, res, next) => {
	try {
		const result = await abonnementIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const abonnement = await findAbonnementById("id", id);
		if (!abonnement) throw createError.NotFound("No data found!");
		req.data = abonnement;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Abonnement`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllAbonnementsMiddleware = async (req, res, next) => {
	try {
		const allAbonnements = await findAllAbonnements(Number(req.query.skip),Number(req.query.take));
		if (allAbonnements.length === 0) return res.sendStatus(204).end();
		else if (!allAbonnements) throw createError.NotFound("No data found!");
		req.data = allAbonnements;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Abonnement`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteAbonnementMiddleware = async (req, res, next) => {
	try {
		const result = await abonnementIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const abonnement = await deleteAbonnement(id);
		if (!abonnement.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Abonnement`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createAbonnementFn = async (result) => {
	try {
		const addedAbonnement = await addAbonnement({
			result
		});

		return addedAbonnement;
	} catch (error) {
		console.log(error);
	}
};

export {
	createAbonnementMiddleware,
	patchAbonnementMiddleware,
	findAbonnementByIdMiddleware,
	findAllAbonnementsMiddleware,
	deleteAbonnementMiddleware,
	createAbonnementFn,
};
