"use strict";
import Joi from "joi";
import createError from "http-errors";
// import { sendEmailValidation } from "../helpers/sendEmail.hepler.js";

import {
	findOfferById,
	findAllOffers,
	editOffer,
	deleteOffer,
	addOffer,
	findOnByUuid
} from "../Models/offer.model.js";

/**
 * Validate Add Offer
 */
const offerSchema = Joi.object({
	title: Joi.string().max(255).required(),
	description: Joi.string().max(1000).required(),
	employerId: Joi.number().required(),
	diplomasName: Joi.string().max(255).required(),
	isDiplomasRequired: Joi.boolean().required(),
	minExpYears: Joi.any()
		.valid(
			"one",
			"two",
			"three",
			"four",
			"five",
			"six",
			"seven",
			"eight",
			"nine",
			"ten"
		)
		.required(),
	minLanguageLevel: Joi.any()
		.valid(
			"one",
			"two",
			"three",
			"four",
			"five",
			"six",
			"seven",
			"eight",
			"nine",
			"ten"
		)
		.required(),
});

/**
 * Validate Edit Offer
 */
const patchOfferSchema = Joi.object({
	title: Joi.string().max(255),
	description: Joi.string().max(1000),
	employerId :Joi.number().required(),
	diplomasName: Joi.string().max(255),
	isDiplomasRequired: Joi.boolean(),
	minExpYears: Joi.any().valid(
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten"
	),
	minLanguageLevel: Joi.any().valid(
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten"
	),
	status: Joi.boolean(),
});
/**
* Validate Offer Id
*/
const offerIdSchema = Joi.object({
	id: Joi.number(),
});
/**
* Validate Offer Uuid
*/
const offerUuidSchma = Joi.object({
	uuid: Joi.string(),
});
const offerParamsSchema = Joi.object({
	take : Joi.number(),
	skip : Joi.number(),
})

/**
 * Add a Offer
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createOfferMiddleware = async (req, res, next) => {
	
	try {
		const result = await offerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedOffer = await addOffer(result);

		req.data = addedOffer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Offer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchOfferMiddleware = async (req, res, next) => {
	try {
		const result = await patchOfferSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const offer = await offerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedOffer = await editOffer(Number(offer.id), result);

		if (editedOffer.count) {
			req.data = await findOfferById(Number(offer.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Offer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findOfferByUuidMiddleware = async (req, res, next) =>
{
	try {
		const result = await offerUuidSchma.validateAsync(req.params, {
			abortEarly: false,
		});
		const offer = await findOnByUuid(result.uuid);
		if (!offer) throw createError.NotFound("No data found!");
		req.data = offer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Offer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findOfferByIdMiddleware = async (req, res, next) =>
{
	try {
		const result = await offerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const offer = await findOfferById(id);
		if (!offer) throw createError.NotFound("No data found!");
		req.data = offer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Offer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllOffersMiddleware = async (req, res, next) => {
	try {

		const allOffers = await findAllOffers(Number(req.query.skip),Number(req.query.take));
		if (allOffers.length === 0) return res.sendStatus(204).end();
		else if (!allOffers) throw createError.NotFound("No data found!");
		req.data = allOffers;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Offer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteOfferMiddleware = async (req, res, next) => {
	try {
		const result = await offerIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const offer = await deleteOffer(id);
		if (!offer.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Offer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createOfferFn = async (result) => {
	try {
		const addedOffer = await addOffer({});

		return addedOffer;
	} catch (error) {
		console.log(error);
	}
};

export {
	findOfferByUuidMiddleware,
	createOfferMiddleware,
	patchOfferMiddleware,
	findOfferByIdMiddleware,
	findAllOffersMiddleware,
	deleteOfferMiddleware,
	createOfferFn,
};
