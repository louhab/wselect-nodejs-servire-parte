"use strict";
import createError from "http-errors";
import {
	findDocumentById,
	findAllDocuments,
	editDocument,
	addDocument,
	deleteDocument,
} from "../Models/document.model.js";
import Joi from "joi";



/**
 * Add a addedDocument
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Validate Add Diplome
 */
const documentSchema = Joi.object({
	name: Joi.string().max(255),
	path : Joi.string().max(255),
	file: Joi.object(),

});
const DocumentIdSchema = Joi.object({
	id: Joi.number(),
})
const createDocumentMiddleware = async (req, res, next) => {
	try {
	
		const data = {
			name: req.body.name,
            path: req.body.path,
		}
		const addedDocument = await addDocument(data, req.body.file);
		req.data = addedDocument;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Document`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchDocumentMiddleware = async (req, res, next) => {
	try {
		const { id } = await DocumentIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});
		const data = {
			name: req.body.name,
            path: req.body.path,
		}
		const editedDOment = await editDocument(Number(id),data);

		if (editedDOment.count) {
			req.data = await findDocumentById(Number(id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};
/**
 * Get one `Document` 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findDocumentByIdMiddleware = async (req, res, next) => {
	try {
		const result = await DocumentIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const document = await findDocumentById(id);
		if (!document) throw createError.NotFound("No data found!");
		req.data = document;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Documents` 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllDocumentsMiddleware = async (req, res, next) => {
	try {
		const allDocuments = await findAllDocuments();
		if (allDocuments.length === 0) return res.sendStatus(204).end();
		else if (!allDocuments) throw createError.NotFound("No data found!");
		req.data = allDocuments;
		next();
	} catch (error) {
		next(error);
	}
};


/**
 * Delete a `Document` 
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteDocumentMiddleware = async (req, res, next) => {
	try {
		const result = await DocumentIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const document = await deleteDocument(id);
		if (!document.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

export {
	createDocumentMiddleware,
	patchDocumentMiddleware,
	findDocumentByIdMiddleware,
	findAllDocumentsMiddleware,
	deleteDocumentMiddleware,
};