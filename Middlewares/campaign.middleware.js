"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findCampaignById,
	findAllCampaigns,
	addCampaign,
	editCampaign,
	deleteCampaign,
	findCampaignByRef,
} from "../Models/campaign.model.js";

/**
 * Validate Add Campaign
 */
const campaignSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate Campaign Id
 */

const campaignIdSchema = Joi.object({
	id: Joi.number(),
});

/**
 * Validate Campaign Id
 */

const refUidSchema = Joi.object({
	uid: Joi.string().regex(
		/^[0-9a-fA-F]{8}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{4}\b-[0-9a-fA-F]{12}$/
	),
});

/**
 * Add a Campaign
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCampaignMiddleware = async (req, res, next) => {
	try {
		const result = await campaignSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedCampaign = await addCampaign(result);
		req.data = addedCampaign;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Campaign`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCampaignMiddleware = async (req, res, next) => {
	try {
		const result = await patchCampaignSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const campaign = await campaignIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCampaign = await editCampaign(Number(campaign.id), result);

		if (editedCampaign.count) {
			req.data = await findCampaignById(Number(campaign.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Campaign`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCampaignByIdMiddleware = async (req, res, next) => {
	try {
		const result = await campaignIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const campaign = await findCampaignById(id);
		if (!campaign) throw createError.NotFound("No data found!");
		req.data = campaign;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCampaignByUidMiddleware = async (req, res, next) => {
	try {
		const { uid } = await refUidSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const offersOnCampaigns = await findCampaignByRef(uid);
		if (!offersOnCampaigns) throw createError.NotFound("No data found!");
		req.data = offersOnCampaigns;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Campaign`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCampaignsMiddleware = async (req, res, next) => {
	try {
		const allCampaigns = await findAllCampaigns();
		if (allCampaigns.length === 0) return res.sendStatus(204).end();
		else if (!allCampaigns) throw createError.NotFound("No data found!");
		req.data = allCampaigns;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Campaign`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCampaignMiddleware = async (req, res, next) => {
	try {
		const result = await campaignIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const campaign = await deleteCampaign(id);
		if (!campaign.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Campaign`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCampaignFn = async (result) => {
	try {
		const addedCampaign = await addCampaign({});

		return addedCampaign;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCampaignMiddleware,
	patchCampaignMiddleware,
	findCampaignByIdMiddleware,
	findAllCampaignsMiddleware,
	deleteCampaignMiddleware,
	createCampaignFn,
	findCampaignByUidMiddleware,
};
