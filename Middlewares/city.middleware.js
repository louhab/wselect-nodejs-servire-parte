"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findCityById,
	findAllCitys,
	addCity,
	editCity,
	deleteCity,
} from "../Models/city.model.js";

/**
 * Validate Add City
 */
const citySchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate City Id
 */

const cityIdSchema = Joi.object({
	id: Joi.number(),
});


const patchCitySchema
= Joi.object({
	name: Joi.string().max(255),
	isActive: Joi.boolean(),
});
/**
 * Add a City
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCityMiddleware = async (req, res, next) => {
	try {
		const result = await citySchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedCity = await addCity(result);
		req.data = addedCity;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `City`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCityMiddleware = async (req, res, next) => {
	try {
		const result = await patchCitySchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const city = await cityIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCity = await editCity(Number(city.id), result);

		if (editedCity.count) {
			req.data = await findCityById(Number(city.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `City`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCityByIdMiddleware = async (req, res, next) => {
	try {
		const result = await cityIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const city = await findCityById(id);
		if (!city) throw createError.NotFound("No data found!");
		req.data = city;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `City`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCitysMiddleware = async (req, res, next) => {
	try {
		const allCitys = await findAllCitys();
		if (allCitys.length === 0) return res.sendStatus(204).end();
		else if (!allCitys) throw createError.NotFound("No data found!");
		req.data = allCitys;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `City`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCityMiddleware = async (req, res, next) => {
	try {
		const result = await cityIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const city = await deleteCity(id);
		if (!city.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `City`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCityFn = async (result) => {
	try {
		const addedCity = await addCity({});

		return addedCity;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCityMiddleware,
	patchCityMiddleware,
	findCityByIdMiddleware,
	findAllCitysMiddleware,
	deleteCityMiddleware,
	createCityFn,
};
