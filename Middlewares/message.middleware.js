"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findMessageById,
	findAllMessages,
	addMessage,
	editMessage,
	deleteMessage,
} from "../Models/Message.model.js";

/**
 * Validate Add Message
 */
const messageSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate Message Id
 */

const messageIdSchema = Joi.object({
	id: Joi.number(),
});


/**
 * Add a Message
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createMessageMiddleware = async (req, res, next) => {
	try {
		const result = await messageSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedMessage = await addMessage(result);
		req.data = addedMessage;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Message`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findMessageByIdMiddleware = async (req, res, next) => {
	try {
		const result = await messageIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const Message = await findMessageById(id);
		if (!Message) throw createError.NotFound("No data found!");
		req.data = Message;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Message`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllMessagesMiddleware = async (req, res, next) => {
	try {
		const allMessages = await findAllMessages();
		if (allMessages.length === 0) return res.sendStatus(204).end();
		else if (!allMessages) throw createError.NotFound("No data found!");
		req.data = allMessages;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Message`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteMessageMiddleware = async (req, res, next) => {
	try {
		const result = await messageIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const Message = await deleteMessage(id);
		if (!Message.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Message`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createMessageFn = async (result) => {
	try {
		const addedMessage = await addMessage({});

		return addedMessage;
	} catch (error) {
		console.log(error);
	}
};

export {
	createMessageMiddleware,
	findMessageByIdMiddleware,
	findAllMessagesMiddleware,
	deleteMessageMiddleware,
	createMessageFn,
};
