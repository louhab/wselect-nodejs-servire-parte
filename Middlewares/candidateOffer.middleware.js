"use strict";
import Joi from "joi";
import createError from "http-errors";
import bcrypt from "bcrypt";
// import { sendEmailValidation } from "../helpers/sendEmail.hepler.js";

import {
	findCandidateOfferById,
	findAllCandidateOffers,
	editCandidateOffer,
	addCandidateOffer,
	deleteCandidateOffer,
} from "../Models/candidateOffer.model.js";

/**
 * Validate Add CandidateOffer
 */
const candidateOfferSchema = Joi.object({
	offerId: Joi.number().required(),
	candidateId: Joi.number().required(),

	
});

/**
 * Validate Edit CandidateOffer
 */
 const patchCandidateOfferSchema = Joi.object({
	offerId: Joi.number(),
	candidateId: Joi.number(),
});


/**
 * Validate CandidateOffer Id
 */

const candidateOfferIdSchema = Joi.object({
	id: Joi.number(),
});

/**
 * Add a CandidateOffer
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCandidateOfferMiddleware = async (req, res, next) => {
	try {
		const result = await candidateOfferSchema.validateAsync(req.body, {
			abortEarly: false,
		});
		
		const addedCandidateOffer = await addCandidateOffer(result);

		req.data = addedCandidateOffer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `CandidateOffer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCandidateOfferMiddleware = async (req, res, next) => {
	try {
		const result = await patchCandidateOfferSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const candidateOffer = await candidateOfferIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCandidateOffer = await editCandidateOffer(Number(candidateOffer.id), result);

		if (editedCandidateOffer.count) {
			req.data = await findCandidateOfferById(Number(candidateOffer.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `CandidateOffer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCandidateOfferByIdMiddleware = async (req, res, next) => {
	try {
		const result = await candidateOfferIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const candidateOffer = await findCandidateOfferById(id);
		if (!candidateOffer) throw createError.NotFound("No data found!");
		req.data = candidateOffer;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `CandidateOffer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCandidateOffersMiddleware = async (req, res, next) => {
	try {
		const allCandidateOffers = await findAllCandidateOffers();
		if (allCandidateOffers.length === 0) return res.sendStatus(204).end();
		else if (!allCandidateOffers) throw createError.NotFound("No data found!");
		req.data = allCandidateOffers;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `CandidateOffer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCandidateOfferMiddleware = async (req, res, next) => {
	try {
		// const result = await candidateOfferIdSchema.validateAsync(req.params, {
		// 	abortEarly: false,
		// });

		// const id = Number(result.id);
		const candidateOffer = await deleteCandidateOffer();
		if (!candidateOffer.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `CandidateOffer`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCandidateOfferFn = async (result) => {
	try {
		const addedCandidateOffer = await addCandidateOffer({});

		return addedCandidateOffer;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCandidateOfferMiddleware,
	patchCandidateOfferMiddleware,
	findCandidateOfferByIdMiddleware,
	findAllCandidateOffersMiddleware,
	deleteCandidateOfferMiddleware,
	createCandidateOfferFn,
};
