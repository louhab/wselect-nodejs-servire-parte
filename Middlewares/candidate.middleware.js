"use strict";
import Joi from "joi";
import createError from "http-errors";
import bcrypt from "bcrypt";
import { signAccessToken } from "../helpers/jwt.helper.js";
// import { sendEmailValidation } from "../helpers/sendEmail.hepler");
import fs from "fs";
import path from "path";

const __dirname = path.resolve();

import {
	findCandidateById,
	findAllCandidates,
	addCandidate,
	editCandidate,
	deleteCandidate,
	assignToOffers,
	countCandidate,
	countCandidatePhone,
} from "../Models/candidate.model.js";
import { countPreCandidate } from "../Models/precandidature.model.js";
/**
 * Validate Add Candidate
 */
const candidateSchema = Joi.object({
	firstName: Joi.string().max(255),
	lastName: Joi.string().max(255),
	phone: Joi.string().max(255).allow(null),
	diplomasName: Joi.string().max(255).allow(null, ""),
	email: Joi.string().max(255),
	offerId: Joi.number(),
	hasDiplomas: Joi.boolean(),
	yearsExp: Joi.any()
		.valid(
			"one",
			"two",
			"three",
			"four",
			"five",
			"six",
			"seven",
			"eight",
			"nine",
			"ten"
		)
		.allow(null, ""),
	languageLevel: Joi.any()
		.valid("one", "four", "five", "six", "seven", "eight", "nine", "ten")
		.allow(null, ""),
	cv_file: Joi.any(),
	gender: Joi.any().valid("man", "woman"),
	diplomeId: Joi.number(),
	domaineDactiviteId: Joi.number(),
});

/**
 * Validate update cv Candidate
 */

const updateCandidateCvSchema = Joi.object({
	cv_file: Joi.any(),
});

/**
 * Validate update Candidate
 */

const updateCandidateSchema = Joi.object({
	firstName: Joi.string().max(255),
	lastName: Joi.string().max(255),
	password: Joi.string().max(255),
	address: Joi.string().max(255).allow(null),
	phone: Joi.string().max(255).allow(null),
	diplomasName: Joi.string().max(255).allow(null, ""),
	email: Joi.string().max(255),
	offerId: Joi.number(),
	hasDiplomas: Joi.boolean(),
	yearsExp: Joi.any().valid(
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten"
	),
	languageLevel: Joi.any().valid(
		"one",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten"
	),
	cv_file: Joi.any(),
	status: Joi.any().valid("fail", "pass", "good", "verygood"),
	gender: Joi.any().valid("man", "woman"),
	diplomeId: Joi.number(),
	domaineDactiviteId: Joi.number(),
});

/**
 * Validate Candidate Id
 */

const candidateIdSchema = Joi.object({
	id: Joi.number(),
});

/**
 * Validate Sign Up
 */
const signUpCandidateSchema = Joi.object({
	firstName: Joi.string().max(255).required(),
	lastName: Joi.string().max(255).required(),
	phone: Joi.string().max(255).required(),
	email: Joi.string().max(255).required(),
	password: Joi.string().required(),
	status: Joi.any(),
});

/**
 * Validate update Candidate Password
 */
const signInCandidateSchema = Joi.object({
	email: Joi.string().max(255).required(),
	password: Joi.string().required(),
});

/**
 * Validate Sign Up
 */
const updateCandidatePasswordSchema = Joi.object({
	newPassword: Joi.string().required(),
	currentPassword: Joi.string().required(),
});

/**
 * Add a Candidate
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCandidateMiddleware = async (req, res, next) => {
	try {
		const fields = {
			...req.fields,
			hasDiplomas: req.fields?.hasDiplomas === "0" ? false : true,
		};

		const result = await candidateSchema.validateAsync(fields, {
			abortEarly: false,
		});

		const isExists = await countCandidate(result.email);
		const isExistsPhone = await countCandidatePhone(result.phone);

		if (isExists) throw createError.Conflict("Email deja utilisé");
		if (isExistsPhone) throw createError.Conflict("Phone deja utilisé");
		const { files } = req;

		let data;

		if (files?.cv_file) {
			const ext =
				"." + files["cv_file"].originalFilename.split(".").pop();
			const pathOnly = path.join(
				__dirname,
				`/public/uploads/${result.offerId}`
			);
			const fileName =
				result.phone +
				"-" +
				result.firstName +
				"-" +
				result.lastName +
				ext;

			if (!fs.existsSync(pathOnly)) {
				fs.mkdirSync(pathOnly);
			}

			const newPath = path.join(pathOnly, fileName);

			fs.rename(files["cv_file"].filepath, newPath, (err) => {
				if (err) console.error(err);
			});
			data = await assignToOffers({
				...result,
				cvPath: `uploads/${result.offerId}/${fileName}`,
				diplomeId: Number(result?.diplomeId),
				domaineDactiviteId: Number(result?.domaineDactiviteId),
			});
		} else {
			data = await assignToOffers({
				firstName: result?.firstName,
				lastName: result?.lastName,
				phone: result?.phone,
				diplomasName: result?.diplomasName,
				hasDiplomas: result?.hasDiplomas,
				yearsExp: result?.yearsExp,
				languageLevel: result?.languageLevel,
				email: result?.email,
				offerId: result?.offerId || null,
				gender: result?.gender,
				diplomeId: Number(result?.diplomeId),
				domaineDactiviteId: Number(result?.domaineDactiviteId),
			});
		}

		sendEmailValidation(result?.email, result?.contactName, randomPass);
		req.data = data;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a Candidate
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const signUpCandidateMiddleware = async (req, res, next) => {
	try {
		const result = await signUpCandidateSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const isExists = await countCandidate(result.email);
		const isExistsPhone = await countCandidatePhone(result.phone);

		if (isExists) throw createError.Conflict("Email deja utilisé");
		if (isExistsPhone)
			throw createError.Conflict("Numéro de téléphone deja utilisé");

		const salt = await bcrypt.genSalt(10);
		const hashedPassword = await bcrypt.hash(result.password, salt);

		const record = await addCandidate({
			...result,
			password: hashedPassword,
		});

		req.data = record;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a Candidate
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const signInCandidateMiddleware = async (req, res, next) => {
	try {
		const result = await signInCandidateSchema.validateAsync(req.body, {
			abortEarly: false,
		});
		const isExists = await countPreCandidate(result.email)
		if (!isExists.existe)
			throw createError.Unauthorized(
				"Les informations d'identification invalides"
			);
		const candidate = isExists.precandidate;

		// compare passwords
		const isCorrect = await bcrypt.compare(
			result.password,
			candidate.password
		);

		if (!isCorrect)
			throw createError.Unauthorized(
				"Les informations d'identification invalides"
			);

		const accessToken = await signAccessToken(candidate.id, "c");

		req.data = {
			accessToken,
			user: candidate,
		};
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

const patchCandidateCvMiddleware = async (req, res, next) => {
	try {
		const result = await updateCandidateCvSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const candidate = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const { files } = req;
		const ext = "." + files["cv_file"].originalFilename.split(".").pop();
		const pathOnly = path.join(
			__dirname,
			`/public/uploads/${candidate.id}`
		);
		const fileName = "candidate" + ext;

		if (!fs.existsSync(pathOnly)) {
			fs.mkdirSync(pathOnly);
		}

		const newPath = path.join(pathOnly, fileName);

		fs.rename(files["cv_file"].filepath, newPath, (err) => {
			if (err) console.error(err);
		});
		const editedCandidate = await editCandidate(Number(candidate.id), {
			cvPath: `uploads/${candidate.id}/${fileName}`,
		});
		if (editedCandidate.count) {
			req.data = await findCandidateById("id", Number(candidate.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Candidate cv`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCandidateMiddleware = async (req, res, next) => {
	try {
		const result = await updateCandidateSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const candidate = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedCandidate = await editCandidate(
			Number(candidate.id),
			result
		);

		if (editedCandidate.count) {
			req.data = await findCandidateById("id", Number(candidate.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchCandidatePasswordMiddleware = async (req, res, next) => {
	try {
		const result = await updateCandidatePasswordSchema.validateAsync(
			req.body,
			{ abortEarly: false }
		);

		const param = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const candidate = await findCandidateById("id", param.id);

		if (!candidate) throw createError.BadRequest("invalid user");

		const isCorrect = await bcrypt.compare(
			result.currentPassword,
			candidate.password
		);

		if (!isCorrect) throw createError.BadRequest("password incorrect");

		const salt = await bcrypt.genSalt(10);
		const hashedPassword = await bcrypt.hash(result.newPassword, salt);

		const record = await editCandidate(candidate.id, {
			password: hashedPassword,
		});

		if (record.count) {
			req.data = await findCandidateById("id", Number(candidate.id));
		} else throw createError.NotFound("Record has not been updated");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findCandidateByIdMiddleware = async (req, res, next) => {
	try {
		const result = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const candidate = await findCandidateById("id", id);
		if (!candidate) throw createError.NotFound("No data found!");
		req.data = candidate;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get All `Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllCandidatesMiddleware = async (req, res, next) => {
	try {
		const allCandidates = await findAllCandidates(Number(req.query.skip),Number(req.query.take));
		if (allCandidates.length === 0) return res.sendStatus(204).end();
		else if (!allCandidates) throw createError.NotFound("No data found!");
		req.data = allCandidates;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteCandidateMiddleware = async (req, res, next) => {
	try {
		const result = await candidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const candidate = await deleteCandidate(id);
		if (!candidate.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createCandidateFn = async (req, res, next) => {
	try {

		const addedCandidate = await addCandidate(req.body);

		return addedCandidate;
	} catch (error) {
		console.log(error);
	}
};

export {
	createCandidateMiddleware,
	patchCandidateMiddleware,
	findCandidateByIdMiddleware,
	findAllCandidatesMiddleware,
	deleteCandidateMiddleware,
	signUpCandidateMiddleware,
	createCandidateFn,
	signInCandidateMiddleware,
	patchCandidatePasswordMiddleware,
	patchCandidateCvMiddleware,
};
