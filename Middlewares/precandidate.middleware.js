"use strict";
import Joi from "joi";
import createError from "http-errors";
import {
	countCandidate,
	countCandidatePhone,
} from "../Models/candidate.model.js";
import {
	addPreCandidate,
	findAllPreCandidate,
} from '../Models/precandidature.model.js';

import { sendEmailValidation } from "../helpers/sendEmail.hepler.js";
import bcrypt from "bcrypt";

/**p
 * Validate Add Pre Canidates
 */
const precandidateSchema = Joi.object({
	firstName: Joi.string().max(255).required(),
    lastName: Joi.string().max(255).required(),
    gender : Joi.string().max(255).required(),
    phone : Joi.string().max(255).required(),
    email : Joi.string().max(255).required(),
    diplomeId : Joi.string().required(),
	offerId : Joi.number().required(),
	domaineDactiviteId : Joi.string().required(),
    hasDiplomas : Joi.boolean().required(),
	diplomasName:Joi.string().required(),
    employerId : Joi.number().required(),
    yearsExp : Joi.string().required(),
    languageLevel: Joi.string().required(),
	password : Joi.string(),
});

/**
 * Validate Precandidate Id
 */

const  PreCandidateIdSchema = Joi.object({
	id: Joi.number(),
});


const patchPreCandidateSchema
= Joi.object({
    firstName: Joi.string().max(255).required(),
    lastName: Joi.string().max(255).required(),
    gender : Joi.string().max(255).required(),
    phone : Joi.string().max(255).required(),
	offerId : Joi.number().allow(null, ''),
    email : Joi.string().max(255).required(),
 	diplomeId : Joi.number().allow(null, ''),
    hasDiplomas : Joi.boolean().required(),
	diplomasName:Joi.string().allow(null, ''),
	domaineDactiviteId : Joi.string().allow(null, ''),
    employerId : Joi.number().allow(null, ''),
    yearsExp : Joi.string().required(),
    languageLevel: Joi.string().required(),
	password : Joi.string(),

});
/**
 * Add a PreCandidate
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const creatPreCandidateMiddleware = async (req, res, next) => {
	try {
		const fields = {
			...req.body,
			hasDiplomas: req.fields?.hasDiplomas === "0" ? false : true,
		};
		const result = await precandidateSchema.validateAsync(fields, {
			abortEarly: false,
		});
		const isExists = await countCandidate(result.email);
		const isExistsPhone = await countCandidatePhone(result.phone);

		if (isExists) throw createError.Conflict("Email deja utilisé");
		if (isExistsPhone) throw createError.Conflict("Phone deja utilisé");
		
		if(req.files){
			let file = req.files?.cv_file
			let name = file.name 
			file.mv('public/cv_candidates/'+name, (err)=>{
				if(err) throw err 
			} )
			const randomPass = Math.random().toString(36).slice(-8);
			const salt = bcrypt.genSaltSync();
		    const hashedPass = bcrypt.hashSync(randomPass, salt);
			let dataToAdd = {
				...result,
				cvPath: `'public/cv_candidates/${name}`,
				password:hashedPass
			}
			const addedPreCandiate = await addPreCandidate(dataToAdd);
			sendEmailValidation(result?.email, result?.firstName + result?.lastName, randomPass);

			req.data = addedPreCandiate;
			next();
		}
		else {
			const randomPass = Math.random().toString(36).slice(-8);
			
			const salt = bcrypt.genSaltSync();
		    const hashedPass = bcrypt.hashSync(randomPass, salt);
			let dataToAdd = {
				...result,
				password:hashedPass
			}
			const addedPreCandiate = await addPreCandidate(dataToAdd);
			sendEmailValidation(result?.email, result?.firstName + result?.lastName, randomPass);
			req.data = addedPreCandiate;
			next();
		}
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Pre Candidate`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchPreCandidateMiddleware = async (req, res, next) => {
	try {
		const result = await patchPreCandidateSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const precandidate = await PreCandidateIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editPreCandidate = await editPreCandidate(Number(precandidate.id), result);

		if (editPreCandidate.count) {
			req.data = await findPreCandidateById(Number(precandidate.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};


/**
 * Get All `PreCandidates`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllPreCandidates = async (req, res, next) => {
	try {
		const allOreCandidate = await findAllPreCandidate();
		if (allOreCandidate.length === 0) return res.sendStatus(204).end();
		else if (!allOreCandidate) throw createError.NotFound("No data found!");
		req.data = allOreCandidate;
		next();
	} catch (error) {
		next(error);
	}
};



export {
    findAllPreCandidates,
    patchPreCandidateMiddleware,
    creatPreCandidateMiddleware,
};
