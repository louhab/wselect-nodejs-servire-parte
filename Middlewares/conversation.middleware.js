"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findConversationById,
	findAllConversations,
	addConversation,
	deleteConversation,
	addMessage,
} from "../Models/conversation.model.js";

/**
 * Validate Add Conversation
 */
const conversationSchema = Joi.object({
	rommName: Joi.string(),
	candidateId: Joi.number(),
	employerId: Joi.number(),
	candidate: Joi.string(),
	employer: Joi.string(),
});

/**
 * Validate Conversation Id
 */

const conversationIdSchema = Joi.object({
	id: Joi.number(),
	type: Joi.string(),
});

const messageSchema = Joi.object({
	text: Joi.string(),
	owner: Joi.number(),
	conversationId: Joi.number(),
	candidateId: Joi.number(),
	employerId: Joi.number(),
});



/**
 * Add a Conversation
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createConversationMiddleware = async (req, res, next) => {
	try {
		const result = await conversationSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedConversation = await addConversation(result);
		req.data = addedConversation;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a Message
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createMessageMiddleware = async (req, res, next) => {
	try {
		const result = await messageSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedMessage = await addMessage(result);
		req.data = addedMessage;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Conversation`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findConversationByIdMiddleware = async (req, res, next) => {
	try {
		const result = await conversationIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const type = result.type;
		const conversation = await findConversationById(type, id);
		if (!conversation) throw createError.NotFound("No data found!");

		const sortedDesc = conversation.sort(
			(a, b) => Number(b?.messages[b?.messages?.length - 1]?.createdAt) - Number(a?.messages[a?.messages?.length - 1]?.createdAt),
		  );
		req.data = sortedDesc;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Conversation`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllConversationsMiddleware = async (req, res, next) => {
	try {
		const allConversations = await findAllConversations();
		if (allConversations.length === 0) return res.sendStatus(204).end();
		else if (!allConversations) throw createError.NotFound("No data found!");
		req.data = allConversations;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Conversation`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteConversationMiddleware = async (req, res, next) => {
	try {
		const result = await conversationIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const conversation = await deleteConversation(id);
		if (!conversation.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Conversation`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createConversationFn = async (result) => {
	try {
		const addedConversation = await addConversation({});

		return addedConversation;
	} catch (error) {
		console.log(error);
	}
};

export {
	createConversationMiddleware,
	findConversationByIdMiddleware,
	findAllConversationsMiddleware,
	deleteConversationMiddleware,
	createConversationFn,
	createMessageMiddleware,
};
