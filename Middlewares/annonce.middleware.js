"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findAnnonceById,
	findAnnonceViewById,
	findAllAnnonces,
	addAnnonce,
	editAnnonce,
	deleteAnnonce,
} from "../Models/annonce.model.js";

/**
 * Validate Add Annonce
 */
const annonceSchema = Joi.object({
	name: Joi.string().max(255),
	description: Joi.string(),

	carrerLevelId: Joi.number(),
	employmentTypeId: Joi.number(),
	salaryRangeId: Joi.number(),

	yearsExp: Joi.any().valid(
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten"
	),

	jobFieldId: Joi.number(),
	employerId: Joi.number(),
	countryId: Joi.number(),
	cityId: Joi.number(),
});

/**
 * Validate Annonce Id
 */

const annonceIdSchema = Joi.object({
	id: Joi.number(),
});

const annonceViewIdSchema = Joi.object({
	id: Joi.number(),
});

const patchAnnonceSchema = Joi.object({
	name: Joi.string().max(255),
	description: Joi.string(),

	carrerLevelId: Joi.number(),
	employmentTypeId: Joi.number(),
	salaryRangeId: Joi.number(),

	yearsExp: Joi.any().valid(
		"one",
		"two",
		"three",
		"four",
		"five",
		"six",
		"seven",
		"eight",
		"nine",
		"ten"
	),

	jobFieldId: Joi.number(),
	companieId: Joi.number(),
	employerId: Joi.number(),
	countryId: Joi.number(),
	cityId: Joi.number(),
	isActive: Joi.boolean(),
});
/**
 * Add a Annonce
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createAnnonceMiddleware = async (req, res, next) => {
	try {
		const result = await annonceSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedAnnonce = await addAnnonce(result);
		req.data = addedAnnonce;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `Annonce`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchAnnonceMiddleware = async (req, res, next) => {
	try {
		const result = await patchAnnonceSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const annonce = await annonceIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedAnnonce = await editAnnonce(Number(annonce.id), result);

		if (editedAnnonce.count) {
			req.data = await findAnnonceById(Number(annonce.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Annonce`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findAnnonceByIdMiddleware = async (req, res, next) => {
	try {
		const result = await annonceIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const annonce = await findAnnonceById(id);
		if (!annonce) throw createError.NotFound("No data found!");
		req.data = annonce;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `Annonce`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findAnnonceViewByIdMiddleware = async (req, res, next) => {
	try {
		const result = await annonceViewIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const annonce = await findAnnonceViewById(id);
		if (!annonce) throw createError.NotFound("No data found!");
		req.data = annonce;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `Annonce`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllAnnoncesMiddleware = async (req, res, next) => {
	try {
		const allAnnonces = await findAllAnnonces(req?.query);
		if (allAnnonces.length === 0) return res.sendStatus(204).end();
		else if (!allAnnonces) throw createError.NotFound("No data found!");
		req.data = allAnnonces;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `Annonce`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteAnnonceMiddleware = async (req, res, next) => {
	try {
		const result = await annonceIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const annonce = await deleteAnnonce(id);
		if (!annonce.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `Annonce`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createAnnonceFn = async (result) => {
	try {
		const addedAnnonce = await addAnnonce({});

		return addedAnnonce;
	} catch (error) {
		console.log(error);
	}
};

export {
	createAnnonceMiddleware,
	patchAnnonceMiddleware,
	findAnnonceByIdMiddleware,
	findAllAnnoncesMiddleware,
	deleteAnnonceMiddleware,
	createAnnonceFn,
	findAnnonceViewByIdMiddleware,
};
