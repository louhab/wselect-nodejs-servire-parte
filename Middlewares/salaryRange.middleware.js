"use strict";
import Joi from "joi";
import createError from "http-errors";

import {
	findSalaryRangeById,
	findAllSalaryRanges,
	addSalaryRange,
	editSalaryRange,
	deleteSalaryRange,
} from "../Models/salaryRange.model.js";

/**
 * Validate Add SalaryRange
 */
const salaryRangeSchema = Joi.object({
	name: Joi.string().max(255),
});

/**
 * Validate SalaryRange Id
 */

const salaryRangeIdSchema = Joi.object({
	id: Joi.number(),
});


const patchSalaryRangeSchema
= Joi.object({
	name: Joi.string().max(255),
	isActive: Joi.boolean(),
});
/**
 * Add a SalaryRange
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createSalaryRangeMiddleware = async (req, res, next) => {
	try {
		const result = await salaryRangeSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const addedSalaryRange = await addSalaryRange(result);
		req.data = addedSalaryRange;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Edit a `SalaryRange`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const patchSalaryRangeMiddleware = async (req, res, next) => {
	try {
		const result = await patchSalaryRangeSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		const salaryRange = await salaryRangeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const editedSalaryRange = await editSalaryRange(Number(salaryRange.id), result);

		if (editedSalaryRange.count) {
			req.data = await findSalaryRangeById(Number(salaryRange.id));
		} else throw createError.NotFound("Record to update not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one `SalaryRange`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const findSalaryRangeByIdMiddleware = async (req, res, next) => {
	try {
		const result = await salaryRangeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const salaryRange = await findSalaryRangeById(id);
		if (!salaryRange) throw createError.NotFound("No data found!");
		req.data = salaryRange;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Get one record
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */

/**
 * Get All `SalaryRange`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 * @returns
 */
const findAllSalaryRangesMiddleware = async (req, res, next) => {
	try {
		const allSalaryRanges = await findAllSalaryRanges();
		if (allSalaryRanges.length === 0) return res.sendStatus(204).end();
		else if (!allSalaryRanges) throw createError.NotFound("No data found!");
		req.data = allSalaryRanges;
		next();
	} catch (error) {
		next(error);
	}
};

/**
 * Delete a `SalaryRange`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const deleteSalaryRangeMiddleware = async (req, res, next) => {
	try {
		const result = await salaryRangeIdSchema.validateAsync(req.params, {
			abortEarly: false,
		});

		const id = Number(result.id);
		const salaryRange = await deleteSalaryRange(id);
		if (!salaryRange.count)
			throw createError.NotFound("Record to delete not found!");
		next();
	} catch (error) {
		if (error.isJoi) next(createError.BadRequest(error.details));
		next(error);
	}
};

/**
 * Add a `SalaryRange`
 * @param {*} req
 * @param {*} res
 * @param {*} next
 */
const createSalaryRangeFn = async (result) => {
	try {
		const addedSalaryRange = await addSalaryRange({});

		return addedSalaryRange;
	} catch (error) {
		console.log(error);
	}
};

export {
	createSalaryRangeMiddleware,
	patchSalaryRangeMiddleware,
	findSalaryRangeByIdMiddleware,
	findAllSalaryRangesMiddleware,
	deleteSalaryRangeMiddleware,
	createSalaryRangeFn,
};
