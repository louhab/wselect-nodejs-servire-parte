"use strict";
import Joi from "joi";
import createError from "http-errors";
import bcrypt from "bcrypt";
import { findCandidateById } from "../Models/candidate.model.js";
import { findEmployerById } from "../Models/employer.model.js";
import express from "express";
const app = express();

import { signAccessToken } from "../helpers/jwt.helper.js";
import { findUser, IsUnique } from "../Models/auth.model.js";
import { addUser } from "../Models/user.model.js";



const validateTokenMiddleware = async (req, res, next) => {
	try {
		const { id, type } = req?.payload;
		let data;
		if (type === "c") data = await findCandidateById("id", id);
		else data = await findEmployerById("id", id);

		delete data?.password;

		req.data = data;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.UnprocessableEntity(error.details));
		next(error);
	}
};

const loginSchema = Joi.object({
	email: Joi.string().required(),
	password: Joi.string().required(),
});

const registerSchema = Joi.object({
	fullname: Joi.string(),
	email: Joi.string().max(255),
	password: Joi.string(),
	phone: Joi.string().max(255),
	role: Joi.any().valid("Admin", "Commercial", "Entreprise"),
});

const loginMiddleware = async (req, res, next) => {
	try {
		const result = await loginSchema.validateAsync(req.body, {
			abortEarly: false,
		});
		const user = await findUser(result.email);
		if (!user)
			throw createError.Unauthorized("Invalid Username or Password!");
		const isCorrect = await bcrypt.compare(result.password, user.password);
		if (!isCorrect)
			throw createError.Unauthorized("Invalid Username or Password!");
		const newResult = {
			id: user.id,
			email: user.email,
			fullname: user.fullname,
		};
		const accessToken = await signAccessToken(user.id);
		req.accessToken = accessToken;
		req.user = newResult;
		next();
	} catch (error) {
		if (error.isJoi)
			return next(createError.Unauthorized("Invalid Email or Password!"));
		next(error);
	}
};

const registerMiddleware = async (req, res, next) => {
	try {
		// validate Data
		const result = await registerSchema.validateAsync(req.body, {
			abortEarly: false,
		});

		console.log("validation pass !", result);

		// Check Email is used or not
		const user = await IsUnique(result.email);
		if (user) throw createError.Conflict("Email is already used!");

		const salt = await bcrypt.genSalt(10);
		const hashedPassword = await bcrypt.hash(result.password, salt);

		const addedUser = await addUser({
			fullname: result.fullname,
			email: result.email,
			phone: result.phone,
			password: hashedPassword,
			role: result.role,
		});

		const accessToken = await signAccessToken(addedUser.id);

		req.accessToken = accessToken;

		req.user = addedUser;
		next();
	} catch (error) {
		if (error.isJoi) next(createError.UnprocessableEntity(error.details));
		next(error);
	}
};

export { validateTokenMiddleware, loginMiddleware, registerMiddleware };
